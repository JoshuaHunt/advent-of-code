(import (chicken io)
	(chicken keyword))
(load "/home/joshua/code/util.scm")

(define (extract-id game-id)
  (string->number (cadr (string-split game-id '(" ")))))

(define (parse-draw draw)
  (define (parse-colour colour)
    (let* ((tokens (split colour '(" ")))
	   (count (string->number (car tokens))))
      (list (cadr tokens) count)))
  (->> (split draw '(", "))
       (map parse-colour)
       (apply list)))

(define (parse-game line)
  (let* ((chunks (split line '(": " "; ")))
	 (id (extract-id (car chunks)))
	 (draws (map parse-draw (cdr chunks))))
    `((#:id ,id) (#:draws ,draws))))

(define (get-or alist key default)
  (cadr (or (assoc key alist) `("default" ,default))))


(define (is-draw-possible? draw bound)  
  (all (map (lambda (c) (<= (get-or draw c 0)
			    (get-or bound c 0)))
	    '("red" "blue" "green"))))

(define (is-game-possible? game bound)
  (all (map (lambda (d) (is-draw-possible? d bound))
	    (cadr (assoc #:draws game)))))

(call-with-input-file "input1.txt"
  (lambda (port)
    (->> (read-lines port)
	 (map parse-game)
	 (filter (lambda (g)
		   (is-game-possible? g '(("red" 12) ("green" 13) ("blue" 14)))))
	 (map (lambda (g) (cadr (assoc #:id g))))
	 (apply +)
	 (print))))

(define (min-bound game)
  (define (colour-bound c)
    (apply max (append '(0)
		       (map (lambda (draw) (get-or draw c 0)) (cadr (assoc #:draws game))))))
  (list (colour-bound "red")
	(colour-bound "blue")	
	(colour-bound "green")))


(call-with-input-file "input1.txt"
  (lambda (port)
    (->> (read-lines port)
	 (map parse-game)
	 (map min-bound)
	 (map (lambda (x) (apply * x)))
	 (apply +)
	 (print))))
