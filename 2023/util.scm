(import (chicken condition))

(define-syntax ->>
  (ir-macro-transformer
   (lambda (expr inject compare)
     (letrec ((x (cadr expr))
	      (fs (cddr expr))
	      (ravel (lambda (x fs)
		       (if (null? fs)
			   x
			   (ravel (append (car fs) (list x))
				  (cdr fs))))))
       (ravel x fs)))))

(define (filter pred xs)
  (cond ((null? xs) '())
	((pred (car xs))
	 (cons (car xs)
	       (filter pred (cdr xs))))
	(else
	 (filter pred (cdr xs)))))

(define (any xs)
  (if (null? xs)
      #f
      (or (car xs) (any (cdr xs)))))

(define (all xs)
  (if (null? xs)
      #t
      (and (car xs) (all (cdr xs)))))

(define (string-starts-with? x y)
  (cond ((> (string-length x) (string-length y)) #f)
	((equal? x (substring y 0 (string-length x))) x)
	(else #f)))

(define (grid-ref grid pos)
  ;; Looks up pos in grid, returning #f if out of bounds.
  ;;
  ;; :param grid: a list of strings or list of lists
  ;; :param pos: a dotted pair (x . y)
  (let ((x (car pos))
  	(y (cdr pos)))
    (cond ((not (< -1 y (length grid)))
	   #f)
	  ((string? (list-ref grid y))
	   (if (< -1 x (string-length (list-ref grid y)))
	       (string-ref (list-ref grid y) x)
	       #f))
	  ((list? (list-ref grid y))
	   (if (< -1 x (length (list-ref grid y)))
	       (list-ref (list-ref grid y) x)
	       #f))
	  (else (abort (condition '(type message "grid must be a list of strings or list of lists")))))))
	       
  
