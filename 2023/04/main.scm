(import (chicken io)
	(chicken keyword))
(load "/home/joshua/code/util.scm")

(define (to-numbers xs)
  (map string->number
       (filter (lambda (x) (not (equal? "" x)))
	       xs)))

(define (drop-while pred xs)
  (cond ((null? xs)
	 '())
	((pred (car xs))
	 (drop-while pred (cdr xs)))
	(else xs)))

(define (strip-whitespace xs)
  (list->string (drop-while char-whitespace? (string->list xs))))

(define (parse-card line)
  (let* ((sections (string-split line '("|" ":")))
	 (id (string->number (car (drop-while (lambda (x) (equal? x "")) (cdr (string-split line '(" " ":")))))))
	 (winning (string-split (list-ref sections 1) '(" ")))
	 (mine (string-split (list-ref sections 2) '(" "))))
    (list #:id id
	  #:winning (to-numbers winning)
	  #:mine (to-numbers mine))))

(define (get-winning-numbers card)
  (filter (lambda (x) (member x (get-keyword #:winning card)))
	  (get-keyword #:mine card)))

(define (get-score xs)
  (cond ((null? xs)
	 0)
	((null? (cdr xs))
	 1)
	(else
	 (* 2 (get-score (cdr xs))))))

(call-with-input-file "input1.txt"
  (lambda (port)
    (->> (read-lines port)
	 (map parse-card)
	 (map get-winning-numbers)
	 (map get-score)
	 (apply +)
	 (print))))

(define (drop k xs) (list-tail xs k))
(define (take k xs)
  (if (or (null? xs) (<= k 0))
      '()
      (cons (car xs) (take (- k 1) (cdr xs)))))

(define (sublist xs start end)
  (drop start (take end xs)))

(define (get-new-cards all-cards current)
  (let ((count (length (get-winning-numbers current))))
    (if (< 0 count)
	(sublist all-cards
		 (get-keyword #:id current) ; 1-indexed
		 (+ count (get-keyword #:id current)))
	'())))


(define (win-more all-cards remaining)
  (if (null? remaining)
      '()
      (let ((card (cadr remaining))
	    (rest (cddr remaining))
	    (extras (get-new-cards all-cards card))
	    (new-remaining (insert-extras remaining extras)))
	(cons (car remaining)
	      (win-more all-cards (append extras (cdr remaining)))))))
	

(call-with-input-file "input1.txt"
  (lambda (port)
    (let* ((lines (read-lines port))
	   (cards (map parse-card lines)))
      (print (length (win-more cards
			       (map (lambda (c) (#:card c #:count 1)) cards)))))))
	 
