#include <iostream>

int main() {
  int x;
  std::cin >> x;
  std::cout << "I got " << x << std::endl;
  return 0;
}
