(import (chicken io))
(load "/home/joshua/code/util.scm")

(define (count-calories xs)
  (define (count-calories-internal acc xs)
    (cond ((null? xs)
	   (list acc))
	  ((equal? "" (car xs))
	   (cons acc (count-calories-internal 0 (cdr xs))))
	  (else
	   (count-calories-internal
	    (+ acc (string->number (car xs)))
	    (cdr xs)))))
  (count-calories-internal 0 xs))

(call-with-input-file "input0.txt"
  (lambda (port)
    (->> (read-lines port)
	 (count-calories)
	 (apply max)
	 (print))))

	 
