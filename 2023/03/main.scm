(import (chicken io))
(load "/home/joshua/code/util.scm")


(define (find-numbers lines)
  (define (get pos)
    (let ((x (car pos))
	  (y (cdr pos)))
      (if (and (< -1 y (length lines))
	       (< -1 x (string-length (car lines))))
	  (string-ref (list-ref lines y) x)
	  #f)))

  (define (is-empty-string str)
    (equal? "" str))

  (define (wrap pos)
    (let ((x (car pos))
	  (y (cdr pos)))
      (if (<= (string-length (car lines)) x)
	  (cons 0 (+ 1 y))
	  pos)))

  (define (translate p0 p1)
    (let ((x0 (car p0))
	  (y0 (cdr p0))
	  (x1 (car p1))
	  (y1 (cdr p1)))
      (cons (+ x0 x1) (+ y0 y1))))

  (define (x-translate pos dx)
    (let ((x (car pos))
	  (y (cdr pos)))
      (cons (+ x dx) y)))
      
  (define (has-symbol pos)
    (let ((current (get pos)))
      (and current
	   (not (char-numeric? current))
	   (not (equal? #\. current)))))

  ; deltas in clockwise order
  (define deltas '((-1 . -1) (-1 . 0) (-1 . 1) (0 . 1) (1 . 1) (1 . 0) (1 . -1) (0 . -1)))
  
  (define (has-adjacent-symbol pos)
    (any (map (lambda (delta) (has-symbol (translate pos delta)))	      
	      deltas)))
  
  (define (has-part-number? pos)
    (let ((current (get pos)))
      (if (and current (char-numeric? current))
	  (or (has-adjacent-symbol pos)
	      (has-part-number? (x-translate pos 1)))
	  #f)))

  (define (get-candidate-part-number pos)
    (let ((current (get pos)))
      (if
       (and current (char-numeric? current))       
       (string-append (string current)
		      (get-candidate-part-number (x-translate pos 1)))
       "")))				
  
  (define (find-part-numbers pos)
    (let ((candidate (get-candidate-part-number pos)))
      (cond ((> (cdr pos) (length lines))
	     '())
	    ((and (not (is-empty-string candidate))		  
	          (has-part-number? pos))
	     (cons (string->number candidate)
		   (find-part-numbers
		    (wrap (x-translate pos (+ 1 (string-length candidate)))))))
	    ((not (is-empty-string candidate))
	     (find-part-numbers (wrap (x-translate pos (+ 1 (string-length candidate))))))
	    (else
	     (find-part-numbers (wrap (x-translate pos 1)))))))

  (define (find-number pos)
    (define (search-left pos found)
      (let* ((c (get pos))
	     (has-number (and c (char-numeric? c))))
	(cond (has-number
	       (search-left (x-translate pos -1) #t))
	      (found
	       (get-candidate-part-number (x-translate pos 1)))
	      (else #f))))
    (search-left pos #f))

  (define (ring-merge xs)
    (define (helper first xs)
      (cond ((null? (cdr xs))
	     (if (equal? first (car xs))
		 '()
		 xs))
	    ((equal? (car xs) (cadr xs))
	     (helper first (cdr xs)))
	    (else
	     (cons (car xs)
		   (helper first (cdr xs))))))
    (if (null? xs)
	'()
	(helper (car xs) xs)))		

  (define (adjacent-numbers pos)
    (->> deltas
	 (map (lambda (delta) (find-number (translate pos delta))))
	 (ring-merge)
	 (filter (lambda (x) x))
	 (map string->number)))
 
  (define (find-gears pos)
    (let ((current (get pos))
	  (adjacents (adjacent-numbers pos)))
      (cond ((> (cdr pos) (length lines))
	     '())
	    ((and (equal? #\* current)
		  (equal? (length adjacents) 2))	     
	     (cons (apply * adjacents)
		   (find-gears (wrap (x-translate pos 1)))))
	    (else
	     (find-gears (wrap (x-translate pos 1)))))))

  (list (apply + (find-part-numbers '(0 . 0)))
	(apply + (find-gears '(0 . 0)))))
	    

(call-with-input-file "input1.txt"
  (lambda (port)
    (->> (read-lines port)
	 (find-numbers)
	 (print))))
