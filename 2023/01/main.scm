(import (chicken io))
(load "/home/joshua/code/util.scm")

(define (get-last xs)
  (if (null? (cdr xs))
      (car xs)
      (get-last (cdr xs))))

(define (get-first-and-last xs)
  (list (car xs) (get-last xs)))

(define (extract-number x)
  (->> (string->list x)
       (filter char-numeric?)
       (get-first-and-last)
       (list->string)
         (string->number)))

(call-with-input-file "input1.txt"
  (lambda (port)
    (->> (read-lines port)
	 (map extract-number)
	 (apply +)
	 (print))))

(define (extract-digit digits x)
  (let* ((names (map car digits))
	 (name-at-start
	  (map (lambda (name) (string-starts-with? name x))
	       names))
	 (any-match (any name-at-start)))
    (if any-match
	(cadr (assoc any-match digits))
	(extract-digit digits (substring x 1 (string-length x))))))
	

(define (extract-last-digit digits x)
  (let* ((reverse-string (compose list->string reverse string->list))
	 (reversed-digits
	  (map
	   (lambda (y) (list
			(reverse-string (car y))
			(cadr y)))	
	   digits))
	 (reversed-x (reverse-string x)))
    (extract-digit reversed-digits reversed-x)))

(define (extract-text-number x)
  (let* ((digits '(("0" 0) ("1" 1) ("2" 2) ("3" 3) ("4" 4) ("5" 5) ("6" 6) ("7" 7) ("8" 8) ("9" 9)
		   ("zero" 0) ("one" 1) ("two" 2) ("three" 3) ("four" 4) ("five" 5) ("six" 6) ("seven" 7) ("eight" 8) ("nine" 9)))
	 (tens (extract-digit digits x))
	 (ones (extract-last-digit digits x)))
    (+ (* 10 tens) ones)))

(call-with-input-file "input1.txt"
  (lambda (port)
    (->> (read-lines port)
	 (map extract-text-number)
	 (apply +)
	 (print))))
