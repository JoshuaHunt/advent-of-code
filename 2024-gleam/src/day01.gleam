import gleam/int
import gleam/io
import gleam/list
import gleam/regexp
import gleam/result
import gleam/string
import simplifile

type Error {
  RegexError(regexp.CompileError)
  IoError(simplifile.FileError)
  ParseError(String)
}

fn parse(filename: String) -> Result(List(List(Int)), Error) {
  use re <- result.try(
    result.map_error(regexp.from_string("\\s+"), RegexError(_)),
  )
  use text <- result.try(
    result.map_error(simplifile.read(from: filename), IoError(_)),
  )

  text
  |> string.split("\n")
  |> list.map(regexp.split(with: re, content: _))
  |> list.try_map(with: fn(list) {
    list.try_map(over: list, with: fn(x) {
      result.map_error(int.parse(x), fn(_) {
        ParseError("Unable to parse integer")
      })
    })
  })
}

pub fn main() {
  parse("../2024/input01.txt")
  |> io.println
}
