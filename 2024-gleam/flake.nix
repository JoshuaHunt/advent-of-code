{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/b134951a4c9f3c995fd7be05f3243f8ecd65d798";
  };

  outputs = { self, nixpkgs, ...}: let
    system = "x86_64-linux";
  in {
    devShells."${system}".default =
      with import nixpkgs { inherit system; };
      mkShell {
        buildInputs = [];
        packages = [
          gleam
	        erlang
        ];
        shellHook = ''
          exec fish
        '';
      };
  };
}
      
