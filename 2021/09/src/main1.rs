fn main() {
    let dirs: Vec<(i32, i32)> = vec![(0, 1), (1, 0), (0, -1), (-1, 0)];
    let heights: Vec<Vec<i32>> = 
        include_str!("input.txt")
            .lines()
            .map(|l|
                l.chars()
                    .map(|x| x.to_digit(10).unwrap() as i32)
                    .collect())
            .collect();

    let h = heights.len();
    let w = heights[0].len();
    let mut risk = 0;
    for y in 0..h {
        for x in 0..w {
            let is_low = 
                dirs.iter()
                    .map(|(dx, dy)| (x as i32 + dx, y as i32 + dy))
                    .filter(|(x1, y1)| 0 <= *x1 && 0 <= *y1)
                    .map(|(x1, y1)| (x1 as usize, y1 as usize))
                    .filter(|(x1, y1)| *x1 < w && *y1 < h)
                    .all(|(x1, y1)| heights[y1][x1] > heights[y][x]);
            if is_low {
                risk += 1 + heights[y][x];
                println!("({}, {})", x, y);
            }
        }
    }
    println!("{}", risk);
}
