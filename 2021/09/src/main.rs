use std::collections::HashSet;
use std::collections::BinaryHeap;

const DIRS: [(i32, i32); 4] = [(0, 1), (1, 0), (0, -1), (-1, 0)];

fn main() {
    let heights: Vec<Vec<u32>> = 
        include_str!("input.txt")
            .lines()
            .map(|l|
                l.chars()
                    .map(|x| x.to_digit(10).unwrap())
                    .collect())
            .collect();
    
    let mut visited: HashSet<(usize, usize)> = HashSet::new();
    let mut sizes = BinaryHeap::new();

    let h = heights.len();
    let w = heights[0].len();

    for y in 0..h {
        for x in 0..w {
            let size = flood((x, y), &heights, &mut visited);
            if size > 0 {
                sizes.push(size);
            }
        }
    }
    println!("{}", sizes.pop().unwrap() * sizes.pop().unwrap() * sizes.pop().unwrap());
}

fn flood((x,y): (usize, usize), heights: &Vec<Vec<u32>>, visited: &mut HashSet<(usize, usize)>) -> i32 {
    if visited.contains(&(x, y)) {
        return 0;
    }
    if heights[y][x] == 9 {
        return 0;
    }
    visited.insert((x, y));
    let mut size = 1;

    let h = heights.len() as i32;
    let w = heights[0].len() as i32;

    for (dx, dy) in DIRS {
        let x1 = (x as i32) + dx;
        let y1 = (y as i32) + dy;
        if x1 < 0 || x1 >= w || y1 < 0 || y1 >= h {
            continue;
        }
        size += flood((x1 as usize, y1 as usize), heights, visited);
    }
    return size;
}
            