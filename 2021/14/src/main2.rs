use std::collections::HashMap;

fn main() {
    let mut lines = include_str!("input.txt").lines();
    let polymer: &str = lines.next().unwrap();
    let mut pairs: HashMap<(char, char), i64> = {
        let raw_pairs: Vec<(char, char)> = polymer.chars().zip(polymer.chars().skip(1)).collect();
        let mut pairs = HashMap::new();
        for pair in raw_pairs {
            pairs.insert(pair, pairs.get(&pair).unwrap_or(&0) + 1);
        }
        pairs
    };
    println!("{:?}", pairs);

    lines.next(); // skip blank
    let rules: HashMap<(char, char), char> = 
        lines.map(|l| ((l.chars().nth(0).unwrap(), l.chars().nth(1).unwrap()), l.chars().last().unwrap()))
            .collect();
    
    for _i in 0..40 {
        let mut new_pairs = HashMap::new();
        for ((c0, c2),v) in pairs {
            let c1 = *rules.get(&(c0, c2)).unwrap();
            new_pairs.insert((c0, c1), new_pairs.get(&(c0, c1)).unwrap_or(&0) + v);
            new_pairs.insert((c1, c2), new_pairs.get(&(c1, c2)).unwrap_or(&0) + v);
        }
        pairs = new_pairs;
    } 
    let chars: Vec<(char, i64)> = pairs.into_iter().flat_map(|((c0, c1), v)| vec![(c0, v), (c1, v)]).collect();
    let mut counts: HashMap<char, i64> = HashMap::new();
    for (c, v) in chars {
        counts.insert(c, counts.get(&c).unwrap_or(&0) + v);
    }
    let max = (counts.values().max().unwrap()+1) / 2; // +1 for the first char, which isn't counted twice
    let min = (counts.values().min().unwrap()+1) / 2;
    println!("{}", max - min);
}
