use std::collections::HashMap;

fn main() {
    let mut lines = include_str!("input.txt").lines();
    let mut polymer: Vec<char> = lines.next().unwrap().chars().collect();
    lines.next(); // skip blank
    let rules: HashMap<(char, char), char> = 
        lines.map(|l| ((l.chars().nth(0).unwrap(), l.chars().nth(1).unwrap()), l.chars().last().unwrap()))
            .collect();
    
    for _i in 0..40 {
        for i in 0..(polymer.len()-1) {
            polymer.insert(2*i+1, *rules.get(&(polymer[2*i], polymer[2*i+1])).unwrap())
        }
    } 
    let mut counts: HashMap<char, i32> = HashMap::new();
    for c in polymer {
        counts.insert(c, counts.get(&c).unwrap_or(&0) + 1);
    }
    let max = counts.values().max().unwrap();
    let min = counts.values().min().unwrap();
    println!("{}", max - min);
}
