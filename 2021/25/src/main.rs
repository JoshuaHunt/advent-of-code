fn main() {
    let mut board = parse_board(include_str!("input.txt"));
    board.print();
    for i in 1.. {
        let mut no_movers = true;
        let east_movers = board.movers('>', (1, 0));
        no_movers &= east_movers.is_empty();
        board.move_bugs(east_movers, (1, 0));
        
        let south_movers = board.movers('v', (0, 1));
        no_movers &= south_movers.is_empty();
        board.move_bugs(south_movers, (0, 1));
        
        if no_movers {
            println!("Stationary after {} moves", i);
            return
        }
    }
}

fn parse_board(descr: &str) -> Board {
    let mut cells = Vec::new();
    for (i, line) in descr.lines().enumerate() {
        cells.push(Vec::new());
        for char in line.chars() {
            cells[i].push(char);
        }
    }
    return Board {
        width: cells[0].len(),
        height: cells.len(),
        cells,
    };
}

struct Board {
    cells: Vec<Vec<char>>,
    width: usize,
    height: usize,
}

impl Board {
    fn movers(&self, bug: char, (dx, dy): (usize, usize)) -> Vec<(usize, usize)> {
        let mut movers = Vec::new();
        for y in 0..self.height {
            for x in 0..self.width {
                let (newx, newy) = self.translate((x, y), (dx, dy));
                if self.cells[y][x] == bug && self.cells[newy][newx] == '.' {
                    movers.push((x, y));
                }
            }
        }
        return movers;
    }

    fn translate(&self, (x, y): (usize, usize), (dx, dy): (usize, usize)) -> (usize, usize) {
        let newx = (x + dx) % self.width;
        let newy = (y + dy) % self.height;
        (newx, newy)
    }

    fn move_bugs(&mut self, positions: Vec<(usize, usize)>, (dx, dy): (usize, usize)) {
        for (x, y) in positions {
            let (newx, newy) = self.translate((x, y), (dx, dy));
            self.cells[newy][newx] = self.cells[y][x];
            self.cells[y][x] = '.';
        }
    }

    fn print(&self) {
        for row in self.cells.iter() {
            for c in row {
                print!("{}", c);
            }
            println!("")
        }
        println!("");
    }
}