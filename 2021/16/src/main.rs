use to_binary::BinaryString;

fn main() {
    // let input = include_str!("input.txt");
    let input = "C200B40A82";
    let binary = BinaryString::from_hex(input).unwrap().to_string();
    let (_length, packet) = parse_packet(&binary);
    println!("{:?}", evaluate(&packet));
}

fn evaluate(packet: &Packet) -> usize {
    match packet {
        Packet::LITERAL{value, ..} => *value,
        Packet::OPERATOR{type_id, subpackets, ..} if *type_id == 0 =>
            subpackets.iter().map(|s| evaluate(s)).sum::<usize>(),
        Packet::OPERATOR{type_id, subpackets, ..} if *type_id == 1 =>
            subpackets.iter().map(|s| evaluate(s)).product::<usize>(),
        Packet::OPERATOR{type_id, subpackets, ..} if *type_id == 2 =>
            subpackets.iter().map(|s| evaluate(s)).min().unwrap(),
        Packet::OPERATOR{type_id, subpackets, ..} if *type_id == 3 =>
            subpackets.iter().map(|s| evaluate(s)).max().unwrap(),
        Packet::OPERATOR{type_id, subpackets, ..} if *type_id == 5 =>
            if evaluate(&subpackets[0]) > evaluate(&subpackets[1]) { 1 } else { 0 },
        Packet::OPERATOR{type_id, subpackets, ..} if *type_id == 6 =>
            if evaluate(&subpackets[0]) < evaluate(&subpackets[1]) { 1 } else { 0 },
        Packet::OPERATOR{type_id, subpackets, ..} if *type_id == 7 =>
            if evaluate(&subpackets[0]) == evaluate(&subpackets[1]) { 1 } else { 0 },
        _ => panic!("Oh no!")
    }
}

fn sum_versions(packet: &Packet) -> usize {
    match packet {
        Packet::LITERAL{version, ..} => *version,
        Packet::OPERATOR{version, subpackets, ..} => *version + subpackets.iter().map(|s| sum_versions(s)).sum::<usize>(),
    }
}

fn parse_packet(input: &str) -> (usize, Packet) {
    println!("Parse packet {}", input);
    let version = usize::from_str_radix(&input[0..3], 2).unwrap();
    let type_id = usize::from_str_radix(&input[3..6], 2).unwrap();
    println!("    Version {}, type {}", version, type_id);
    println!("    Content {}", &input[6..]);
    if type_id == 4 {
        let (length, value) = parse_literal_value(&input[6..]);
        return (length + 6, Packet::LITERAL{version, type_id, value});
    }

    println!("    Total length bit: {}", input.chars().nth(6).unwrap());
    let is_total_length_packet = input.chars().nth(6).unwrap() == '0';
    let (length, subpackets) = if is_total_length_packet {
        parse_total_length_subpackets(&input[7..])
    } else {
        parse_counted_subpackets(&input[7..])
    };
    return (length + 7, Packet::OPERATOR{version, type_id, subpackets});
}

fn parse_literal_value(input: &str) -> (usize, usize) {
    println!("Parse literal {}", input);
    let mut i: usize = 0;
    let mut value = 0;
    let mut finished = false;
    while !finished {
        finished = input.chars().nth(i).unwrap() == '0';
        value = value * 16 + usize::from_str_radix(&input[(i+1)..(i+5)], 2).unwrap();
        i += 5;
    }
    return (i, value);
}

fn parse_total_length_subpackets(input: &str) -> (usize, Vec<Packet>) {
    println!("Parse by length: {}", input);
    let length = usize::from_str_radix(&input[0..15], 2).unwrap();
    println!("    Length: {}", length);
    println!("    Content: {}", &input[15..]);
    let mut parsed_bits = 0;
    let mut packets = Vec::new();
    while parsed_bits < length {
        let (new_parsed, new_packet) = parse_packet(&input[15+parsed_bits..]);
        parsed_bits += new_parsed;
        packets.push(new_packet);
    }
    return (15 + length, packets);
}

fn parse_counted_subpackets(input: &str) -> (usize, Vec<Packet>) {
    println!("Parse by count: {}", input);
    let count = usize::from_str_radix(&input[0..11], 2).unwrap();
    println!("    Count: {}", count);
    let mut packets = Vec::new();
    let mut length = 11;
    for _i in 0..count {
        let (new_length, new_packet) = parse_packet(&input[length..]);
        length += new_length;
        packets.push(new_packet);
    }
    return (length, packets);
}

#[derive(Debug)]
enum Packet {
    LITERAL{version: usize, type_id: usize, value: usize},
    OPERATOR{version: usize, type_id: usize, subpackets: Vec<Packet>}
}