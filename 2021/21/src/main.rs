use itertools::cons_tuples;
use std::collections::HashMap;

fn main() {
    main1();
    main2();
}

fn main2() {
    let mut boards = HashMap::new();
    let mut wins = [0, 0];
    let rolls: HashMap<isize, usize> = {
        let mut r = HashMap::new();
        for r0 in 1..=3 {
            for r1 in 1..=3 {
                for r2 in 1..=3 {
                    let roll = r0 + r1 + r2;
                    r.insert(roll, r.get(&roll).unwrap_or(&0) + 1);
                }
            }
        }
        r
    };
    boards.insert(
        Board { 
            players: [Player::new(7), Player::new(3)],
            current_player: 0
        },
        1
    );
    while !boards.is_empty() {
        let mut new_boards = HashMap::new();
        boards.into_iter()
            .flat_map(|(b, bf)| rolls.iter().map(|(r, rf)| (b.step(*r), bf * rf)).collect::<HashMap<Board, usize>>())
            .for_each(|(b, f)| {
                let new_f = new_boards.get(&b).unwrap_or(&0) + f;
                new_boards.insert(b, new_f);
            });
        let (finished, unfinished) = new_boards.into_iter().partition(|(b, _f)| b.winner().is_some());
        boards = unfinished;
        for (b, f) in finished {
            let winner = b.winner().unwrap();
            wins[winner] += f;
        }
    }
    
    println!("{}\n{}", wins[0], wins[1]);
}

#[derive(Eq, PartialEq, Hash)]
struct Board {
    players: [Player; 2],
    current_player: usize,
}

impl Board {
    fn step(&self, roll: isize) -> Board {
        let mut new_players = self.players.clone();
        new_players[self.current_player].roll(roll);
        Board {
            players: new_players,
            current_player: (self.current_player + 1) % 2,
        }
    }

    fn winner(&self) -> Option<usize> {
        for i in 0..2 {
            if self.players[i].score >= 21 {
                return Some(i);
            }
        }
        return None
    }
}

fn play(boards: &mut Vec<Board>) -> [usize; 2] {
    let mut wins = [0; 2];
    for r0 in 1..=3 {
        for r1 in 1..=3 {
            for r2 in 1..=3 {
                let next_board = boards[boards.len() - 1].step(r0 + r1 + r2);
                if let Some(winner) = next_board.winner() {
                    wins[winner] += 1;
                } else {
                    boards.push(next_board);
                    let [p0win, p1win] = play(boards);
                    boards.pop();
                    wins[0] += p0win;
                    wins[1] += p1win;
                }
            }
        }
    }
    return wins;
}

fn main1() {
    let mut players = vec![Player::new(7), Player::new(3)];

    let die0 = (1..=100).cycle().step_by(3);
    let die1 = (1..=100).cycle().skip(1).step_by(3);
    let die2 = (1..=100).cycle().skip(2).step_by(3);
    let rolls = cons_tuples(die0.zip(die1).zip(die2));
    let mut round = 0;
    for (r1, r2, r3) in rolls {
        let player = &mut players[round % 2];
        let roll = r1 + r2 + r3;
        round += 1;        
        player.roll(roll);
        if player.score >= 1000 {
            println!("{} * {} = {}", players[round % 2].score, round * 3, players[round % 2].score * round as isize * 3);
            break
        }
    }
}

#[derive(Eq, PartialEq, Clone, Hash)]
struct Player {
    pos: isize,
    score: isize,
}

impl Player {
    fn new(pos: isize) -> Player {
        Player {score: 0, pos}
    }

    fn roll(&mut self, roll: isize) {
        self.pos = (self.pos + roll - 1) % 10 + 1;
        self.score += self.pos;
    }
}
