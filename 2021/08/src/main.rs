use std::collections::HashSet;
use std::collections::HashMap;

fn main() {
    let count = include_str!("input.txt").lines()
        .flat_map(|v| v.split(" ").skip(11))
        .filter(|x| x.len() == 2 || x.len() == 3 || x.len() == 4 || x.len() == 7)
        .count();
    println!("{}", count);

    let sum: i32 = include_str!("input.txt").lines()
        .map(|l| decode(l))
        .sum();
    println!("{}", sum);
}

fn decode(line: &str) -> i32 {
    let inputs = parse_inputs(line);
    let output = parse_output(line);

    let mut wiring = Wiring::new();
    wiring.put(find_a(&inputs), 'a');
    wiring.put(find_segment_with_count(6, &inputs), 'b');
    wiring.put(find_segment_with_count(4, &inputs), 'e');
    wiring.put(find_segment_with_count(9, &inputs), 'f');
    wiring.put(find_c(&inputs, &wiring), 'c');
    wiring.put(find_d(&inputs, &wiring), 'd');
    wiring.put(find_g(&wiring), 'g');

    return decode_output(output, &wiring);
}

fn parse_inputs(line: &str) -> Vec<Digit> {
    line.split(" ").take(10).map(Digit::new).collect()
}

fn parse_output(line: &str) -> Vec<Digit> {
    line.split(" ").skip(11).map(Digit::new).collect()
}

fn find_a(inputs: &Vec<Digit>) -> char {
    let seven = find_digit_with_len(3, inputs);
    let one = find_digit_with_len(2, inputs);
    seven.without(one)
}

fn find_digit_with_len(len: usize, digits: &Vec<Digit>) -> &Digit {
    digits.iter().filter(|d| d.len() == len).next().unwrap()
}

fn find_segment_with_count(count: i8, inputs: &Vec<Digit>) -> char {
    *find_segments_with_count(count, inputs).iter().next().unwrap()
}

fn find_segments_with_count(count: i8, inputs: &Vec<Digit>) -> Vec<char> {
    let mut counts: HashMap<char, i8> = "abcdefg".chars().map(|c| (c, 0)).collect();
    for digit in inputs {
        for seg in &digit.segs {
            counts.insert(*seg, counts.get(seg).unwrap_or(&0) + 1);
        }
    }
    counts.iter().filter(|(k, v)| **v == count).map(|(k, v)| *k).collect()
}

fn find_c(inputs: &Vec<Digit>, wiring: &Wiring) -> char {
    let c_or_a = find_segments_with_count(8, inputs);
    if wiring.map.contains_key(&c_or_a[0]) {
        c_or_a[1]
    } else {
        c_or_a[0]
    }
}

fn find_d(inputs: &Vec<Digit>, wiring: &Wiring) -> char {
    let four = find_digit_with_len(4, inputs);
    *four.segs.iter().filter(|c| !wiring.map.contains_key(c)).next().unwrap()
}

fn find_g(wiring: &Wiring) -> char {
    "abcdefg".chars().filter(|c| !wiring.map.contains_key(c)).next().unwrap()
}

fn decode_output(output: Vec<Digit>, wiring: &Wiring) -> i32 {
    return output[0].decode(wiring) * 1000 
        + output[1].decode(wiring) * 100
        + output[2].decode(wiring) * 10
        + output[3].decode(wiring);
}

struct Wiring {
    map: HashMap<char, char>,
}

impl Wiring {
    fn new() -> Wiring {
        Wiring {
            map: HashMap::new()
        }
    }

    fn put(&mut self, k: char, v: char) {
        self.map.insert(k, v);
    }

    fn get(&self, k: &char) -> &char {
        self.map.get(k).unwrap()
    }
}

struct Digit {
    segs: HashSet<char>,
}

impl Digit {
    fn new(desc: &str) -> Digit {
        Digit {
            segs: desc.chars().collect()
        }
    }

    fn len(&self) -> usize {
        self.segs.len()
    }

    fn decode(&self, wiring: &Wiring) -> i32 {
        Self::to_int(self.segs.iter().map(|s| wiring.get(s)).collect())
    }

    fn to_int(segs: HashSet<&char>) -> i32 {
        if segs.len() == 2 {
            1
        } else if segs.len() == 3 {
            7
        } else if segs.len() == 4 {
            4
        } else if segs.len() == 7 {
            8
        } else if segs.len() == 6 && !segs.contains(&'d') {
            0
        } else if segs.len() == 6 && !segs.contains(&'c') {
            6
        } else if segs.len() == 6 && !segs.contains(&'e') {
            9
        } else if segs.contains(&'b') {
            5
        } else if segs.contains(&'e') {
            2
        } else {
            3
        }
    }

    fn without(&self, other: &Digit) -> char {
        *self.segs.difference(&other.segs).next().unwrap()
    }
}


