use std::collections::HashSet;
use std::collections::HashMap;


const DIRS: [(isize, isize); 9] =
    [(-1, -1), (0, -1), (1, -1), (-1, 0), (0, 0), (1, 0), (-1, 1), (0, 1), (1, 1)];

fn main() {
    let mut input = include_str!("input.txt").lines();
    let translator = build_translator(input.next().unwrap());
    let mut image = read_image(input.skip(1));

    for i in 0..50 {
        // print_img(&image);
        let background = if translator.contains(&0) {
            i % 2 == 1
        } else {
            false
        };
        image = enhance_image(&mut image, &translator, background);
    }
    // print_img(&image);
    let lit_count = image.iter().flat_map(|row| row).filter(|lit| **lit).count();
    println!("{} lit pixels", lit_count);
}

fn build_translator(line: &str) -> HashSet<usize> {
    line.chars().enumerate().filter(|(_i, c)| *c == '#').map(|(i, _c)| i).collect()
}

fn read_image<'a, I>(lines: I) -> Vec<Vec<bool>> where I: Iterator<Item = &'a str>  {
    let mut result = Vec::new();
    for (y, line) in lines.enumerate() {
        result.push(Vec::new());
        for c in line.chars() {
            result[y].push(c == '#')
        }
    }
    return result;
}

fn enhance_image(image: &mut Vec<Vec<bool>>, translator: &HashSet<usize>, background: bool) -> Vec<Vec<bool>> {
    let size = image.len();
    for row in image.iter_mut() {
        row.insert(0, background);
        row.push(background);
    }
    image.insert(0, vec![background; size + 2]);
    image.push(vec![background; size + 2]);

    let get = |x: isize, y: isize| -> bool {
        if x < 0 || y < 0 || x as usize >= size + 2 || y as usize >= size + 2 {
            background
        } else {
            image[y as usize][x as usize]
        }
    };

    let mut new_image = Vec::new();
    for y in 0..(size+2) {
        new_image.push(Vec::new());
        for x in 0..(size+2) {
            let pixels: Vec<bool> = 
                DIRS.iter().map(|(dx, dy)| (x as isize + dx, y as isize + dy))
                    .map(|(x, y)| get(x, y))
                    .collect();
            let lit = translator.contains(&pixels_to_index(&pixels));
            new_image[y].push(lit);
        }
    }

    return new_image
}

fn pixels_to_index(pixels: &Vec<bool>) -> usize {
    let mut val = 0;
    for p in pixels.iter() {
        val = 2 * val + (*p as usize);
    }
    return val;
}

fn print_img(image: &Vec<Vec<bool>>) {
    println!("({}, {})", image.len(), image.len());
    for row in image {
        for x in row {
            if *x {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!("")
    }
    println!("")
}