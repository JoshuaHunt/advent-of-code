use std::fmt::Display;
use std::fmt::Formatter;

fn main() {
    let mut board = read_board();
    let mut i = 0;
    loop {
        i += 1;
        board.charge_all();
        if board.flash() == 100 {
            break
        }        
        println!("{}", board);
    }
    println!("{}", board);
    println!("{}", i);
}

fn read_board() -> Board {
    Board {
        cells: 
            include_str!("input.txt").lines()
                .map(|l| 
                    l.chars()
                        .map(|c| c.to_digit(10).unwrap() as i32)
                        .collect())
                .collect(),
        charged: Vec::new(),
    }
}

struct Board {
    cells: Vec<Vec<i32>>,
    charged: Vec<(usize, usize)>
}

const DIRS: [(i32, i32); 8] = [(1,-1),(1,0),(1,1),(0,-1),(0,1),(-1,-1),(-1,0),(-1,1)];

impl Board {
    fn charge_all(&mut self) {
        for y in 0..10 {
            for x in 0..10 {
                self.charge(x, y);
            }
        }
    }

    fn flash(&mut self) -> i32 {
        let mut flashed: Vec<(usize, usize)> = Vec::new();
        while let Some((x, y)) = self.charged.pop() {
            for (dx, dy) in DIRS {
                self.charge((x as i32) + dx, (y as i32) + dy);
            }
            flashed.push((x, y));
        };
        for (x, y) in &flashed {
            self.cells[*y][*x] = 0;
        }
        return flashed.len() as i32
    }

    fn charge(&mut self, x: i32, y: i32) {
        if x < 0 || 10 <= x || y < 0 || 10 <= y {
            return
        }
        let x0 = x as usize;
        let y0 = y as usize;
        if self.cells[y0][x0] > 9 {
            // already flashed
            return
        }
        self.cells[y0][x0] += 1;
        if self.cells[y0][x0] > 9 {
            self.charged.push((x0, y0));
        }
    }
}

impl Display for Board {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        for y in 0..10 {
            for x in 0..10 {
                write!(f, "{}", self.cells[y][x])?;
            }
            write!(f, "\n")?;
        }
        Ok(())
    }
}
