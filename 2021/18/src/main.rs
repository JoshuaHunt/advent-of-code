use std::fmt;

fn main() {
    let mut arena = Arena::new();
    let numbers: Vec<NodeId> = include_str!("input.txt").lines().map(|l| parse_number(&mut l.chars(), &mut arena)).collect();

    let mut magnitudes: Vec<u32> = vec![];
    for n0 in &numbers {
        for n1 in &numbers {
            magnitudes.push(magnitude(&add(&arena.clone(n0), &arena.clone(n1), &mut arena), &arena));
        }
    }
    let max = magnitudes.iter().max().unwrap();
    println!("Max: {}", max);
}

fn magnitude(node: &NodeId, arena: &Arena) -> u32 {
    if let Some(x) = node.try_get_value(arena) {
        return x;
    }
    let children = node.children(arena).unwrap();
    return 3 * magnitude(&children[0], arena) + 2 * magnitude(&children[1], arena);
}

fn print_value_tree(node: &NodeId, arena: &Arena) -> String {
    if let Some([l, r]) = node.children(arena) {
        format!("[{},{}]", print_value_tree(&l, arena), print_value_tree(&r, arena))
    } else {
        format!("{:?}", node.get_value(arena))
    }
}

fn print_tree(node: &NodeId, arena: &Arena) -> String {
    if let Some([l, r]) = node.children(arena) {
        format!("[{},{}]@{}", print_tree(&l, arena), print_tree(&r, arena), node)
    } else {
        format!("{:?}@{}", node.get_value(arena), node)
    }
}

fn parse_number<'a, T>(line: &mut T, arena: &mut Arena) -> NodeId 
where T: Iterator<Item=char> {
    let first = line.next().unwrap();
    if let Some(number) = first.to_digit(10) {
        let new_node = arena.new_node(Some(number));
        return new_node;
    }
    if first != '[' {
        panic!("Expected [, got {}", first);
    }
    let left = parse_number(line, arena);
    if line.next().unwrap() != ',' {
        panic!("Expected ,");
    }
    let right = parse_number(line, arena);
    if line.next().unwrap() != ']' {
        panic!("Expected ,");
    }
    let parent = arena.new_node(None);
    parent.set_children(&left, &right, arena);
    return parent;
}

fn add(lhs: &NodeId, rhs: &NodeId, arena: &mut Arena) -> NodeId {
    let root = arena.new_node(None);
    root.set_children(&lhs, &rhs, arena);
    reduce(&root, arena);
    return root;
}

fn reduce(node: &NodeId, arena: &mut Arena) {
    loop {
        if let Some(exploder) = find_exploder(node, arena, 0) {
            explode(exploder, arena);
            continue;
        }
        if let Some(splitter) = find_splitter(node, arena) {
            split(&splitter, arena);
            continue;
        }
        break;
    }
}

fn find_exploder(node: &NodeId, arena: &Arena, depth: u32) -> Option<NodeId> {
    if depth >= 4 && is_simple(node, arena) {
        return Some(node.clone());
    }
    for child in node.children(arena).iter().flat_map(|c| c) {
        if let Some(exploded) = find_exploder(child, arena, depth + 1) {
            return Some(exploded);
        }
    }
    return None;
}

fn is_simple(node: &NodeId, arena: &Arena) -> bool {
    node.children(arena)
        .map(|children| children.iter().all(|c| c.children(arena).is_none()))
        .unwrap_or(false)
}

fn explode(node: NodeId, arena: &mut Arena) {
    let children = node.children(arena).unwrap();
    if let Some(left_id) = arena.prev_regular(&node) {
        arena.get_mut(&left_id).x = Some(left_id.get_value(arena) + children[0].get_value(arena));
    }
    if let Some(right_id) = arena.next_regular(&node) {
        arena.get_mut(&right_id).x = Some(right_id.get_value(arena) + children[1].get_value(arena));
    }
    arena.replace_with_zero(&node);
}

fn find_splitter(node: &NodeId, arena: &Arena) -> Option<NodeId> {
    if let Some(value) = node.try_get_value(arena) {
        if value >= 10 {
            return Some(*node);
        } 
    }
    for child in node.children(arena).iter().flat_map(|c| c) {
        let splitter = find_splitter(child, arena);
        if splitter.is_some() {
            return splitter;
        }
    }
    return None;
}

fn split(node: &NodeId, arena: &mut Arena) {
    let x = node.get_value(arena);
    arena.replace_with_pair(node, x/2, (x+1)/2);
}

struct Arena {
    nodes: Vec<Node>,
    children: Vec<Option<[NodeId; 2]>>,
    parents: Vec<Option<NodeId>>,
}

impl Arena {
    fn new() -> Arena {
        Arena {
            nodes: vec![],
            children: vec![],
            parents: vec![],
        }
    }

    fn new_node(&mut self, x: Option<u32>) -> NodeId {
        let next_id = self.nodes.len();
        self.nodes.push(Node {x});
        self.children.push(None);
        self.parents.push(None);
        return NodeId {id: next_id}
    }

    fn get_mut<'a>(&'a mut self, id: &NodeId) -> &'a mut Node {
        &mut self.nodes[id.id]
    }

    fn set_children(&mut self, parent: &NodeId, (l, r): (&NodeId, &NodeId)) {
        self.parents[l.id] = Some(*parent);
        self.parents[r.id] = Some(*parent);
        self.children[parent.id] = Some([l.clone(), r.clone()]);
    }

    fn get_children(&self, parent: &NodeId) -> Option<[NodeId; 2]> {
        self.children[parent.id]
    }

    fn next_regular(&self, node: &NodeId) -> Option<NodeId> {
        let mut current = self.next(node).unwrap(); // Don't want the child
        while let Some(next) = self.next(&current) {
            current = next;
            if self.children[current.id].is_none() {
                return Some(current);
            }
        }
        return None; 
    }

    fn prev_regular(&self, node: &NodeId) -> Option<NodeId> {
        let mut current = self.previous(node).unwrap(); // Don't want the child
        while let Some(prev) = self.previous(&current) {
            current = prev;
            if self.children[current.id].is_none() {
                return Some(current);
            }
        }
        return None; 
    }

    fn next(&self, node: &NodeId) -> Option<NodeId> {
        let mut current = *node;
        if self.children[current.id].is_some() {
            current = self.children[current.id].unwrap()[1];
            while let Some([l, _r]) = self.children[current.id] {
                current = l;
            }
            return Some(current.clone());
        } else {
            let mut prev: NodeId;
            loop {
                if self.parents[current.id].is_none() {
                    return None;
                }
                prev = current;
                current = self.parents[current.id].unwrap();
                if current.children(self).unwrap()[0] == prev {
                    return Some(current);
                }
            }
        }
    }

    fn previous(&self, node: &NodeId) -> Option<NodeId> {
        if self.children[node.id].is_some() {
            let mut current = self.children[node.id].unwrap()[0];
            while let Some([_l, r]) = self.children[current.id] {
                current = r;
            }
            return Some(current);
        } else {
            let mut current = *node;
            let mut prev: NodeId;
            loop {
                if self.parents[current.id].is_none() {
                    return None;
                }
                prev = current;
                current = self.parents[current.id].unwrap();
                if current.children(self).unwrap()[1] == prev {
                    return Some(current);
                }
            }
        }
    }

    fn replace_with_zero(&mut self, node: &NodeId) {
        let children = self.children[node.id].unwrap();
        self.parents[children[0].id] = None;
        self.parents[children[1].id] = None;
        self.children[node.id] = None;
        self.nodes[node.id].x = Some(0);
        // TODO: remove the child nodes themselver?
    }

    fn replace_with_pair(&mut self, node: &NodeId, l: u32, r: u32) {
        let left = self.new_node(Some(l));
        let right = self.new_node(Some(r));
        self.set_children(node, (&left, &right));
        self.nodes[node.id].x = None;
    }

    fn clone(&mut self, node: &NodeId) -> NodeId {
        let copy = self.new_node(self.nodes[node.id].x);
        if let Some([l, r]) = self.children[node.id] {
            let left = self.clone(&l);
            let right = self.clone(&r);
            copy.set_children(&left, &right, self);
        }
        copy
    }
}

struct Node {
    x: Option<u32>,
}

#[derive(Copy, Clone, Eq, PartialEq)]
struct NodeId {
    id: usize,
}

impl NodeId {
    fn children(&self, arena: &Arena) -> Option<[NodeId; 2]> {
        arena.get_children(self)
    }

    fn get_value(&self, arena: &Arena) -> u32 {
        arena.nodes[self.id].x.unwrap()
    }

    fn try_get_value(&self, arena: &Arena) -> Option<u32> {
        arena.nodes[self.id].x
    }

    fn set_children(&self, l: &NodeId, r: &NodeId, arena: &mut Arena) {
        arena.set_children(self, (l, r));
    }
}

impl fmt::Display for NodeId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.id)
    }
}