use std::collections::HashSet;
use priority_queue::PriorityQueue;

fn main() {
    let risks: Vec<Vec<i32>> = 
        include_str!("input.txt")
            .lines().map(|l| l.chars().map(|c| c.to_digit(10).unwrap() as i32).collect())
            .collect();
    let mut to_visit: PriorityQueue<(usize, usize), i32> = PriorityQueue::new(); // max heap
    let h = risks.len() * 5;
    let w = risks[0].len() * 5;
    to_visit.push((0, 0), 0);
    let mut visited: HashSet<(usize, usize)> = HashSet::new();
    while let Some(((x, y), cost)) = to_visit.pop() {
        for neighbour in get_neighbours((x, y), w, h) {
            if !visited.contains(&neighbour) {
                to_visit.push_increase(neighbour, cost - get_cost(neighbour, &risks));
            }
        }
        visited.insert((x, y));
        if (x, y) == (w-1, h-1) {
            println!("Goal: {}", cost);
            break;
        }
    }
}

const DIRS: [(i32, i32); 4] = [(0, 1), (1, 0), (-1, 0), (0, -1)];
fn get_neighbours((x, y): (usize, usize), w: usize, h: usize) -> Vec<(usize, usize)> {
    DIRS.iter()
        .map(|(dx, dy)| (x as i32 + dx, y as i32 + dy))
        .filter(|(x, y)| 0 <= *x && 0 <= *y)
        .map(|(x, y)| (x as usize, y as usize))
        .filter(|(x, y)| *x < w && *y < h)
        .collect()
}

fn get_cost((x, y): (usize, usize), risks: &Vec<Vec<i32>>) -> i32 {
    let h = risks.len();
    let w = risks[0].len();
    if x >= w {
        wrap(1 + get_cost((x - w, y), risks))
    } else if y >= h {
        wrap(1 + get_cost((x, y - h), risks))
    } else {
        risks[y][x]
    }
}

fn wrap(x: i32) -> i32 {
    let mut y = x;
    while y > 9 {
        y -= 9
    }
    y
}