use std::collections::HashSet;
use std::collections::HashMap;
use priority_queue::PriorityQueue;

fn main() {
    let risks: Vec<Vec<i32>> = 
        include_str!("input.txt")
            .lines().map(|l| l.chars().map(|c| c.to_digit(10).unwrap() as i32).collect())
            .collect();
    
    let mut to_visit: Vec<(usize, usize)> = Vec::new();
    let mut costs: HashMap<(usize, usize), i32> = HashMap::new();
    let mut visited: HashSet<(usize, usize)> = HashSet::new();

    let h = risks.len();
    let w = risks[0].len();
    to_visit.push((0, 0));
    costs.insert((0, 0), 0);
    while let Some((x, y)) = to_visit.pop() {
        for neighbour in get_neighbours((x, y), w, h) {
            if !visited.contains(&neighbour) {
                let old_cost = costs.get(&neighbour);
                let parent_cost = costs.get(&(x, y));
                if old_cost.is_none() || old_cost.unwrap() > cost - risks[y][x] {
                    parents.insert(neighbour, (x, y));
                } else {
                    panic!("Panic!");
                }
            }
        }
        visited.insert((x, y));
        if (x, y) == (w-1, h-1) {
            println!("Goal: {}", cost);
            break;
        }
    }
}

const DIRS: [(i32, i32); 4] = [(0, 1), (1, 0), (-1, 0), (0, -1)];
fn get_neighbours((x, y): (usize, usize), w: usize, h: usize) -> Vec<(usize, usize)> {
    DIRS.iter()
        .map(|(dx, dy)| (x as i32 + dx, y as i32 + dy))
        .filter(|(x, y)| 0 <= *x && 0 <= *y)
        .map(|(x, y)| (x as usize, y as usize))
        .filter(|(x, y)| *x < w && *y < h)
        .collect()
}