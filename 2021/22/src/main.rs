use std::cmp;

fn main() {
    let queue = parse_instructions(include_str!("input.txt").lines());
    let mut instructions: Vec<Instruction> = Vec::new();
    
    for (i, mut instruction) in queue.into_iter().rev().enumerate() {
        println!("Cuboid {}/{}", i, instructions.len());
        for later_instruction in &instructions {
            instruction.remove(&later_instruction.cuboid);
        }
        instructions.push(instruction);    
    }
    let count: usize = instructions.into_iter().map(|c| c.volume()).sum();
    println!("{} on", count);
}

fn parse_instructions<'a, I>(lines: I) -> Vec<Instruction> 
where I : Iterator<Item = &'a str> {
    let mut instructions = Vec::new();
    for line in lines {
        let coord_descrs: Vec<&str> = line.split(&[' ', ','][..]).collect();
        instructions.push(
            Instruction {
                cuboid: Cuboid {
                    x: Range::parse(coord_descrs[1]),
                    y: Range::parse(coord_descrs[2]),
                    z: Range::parse(coord_descrs[3]),
                },
                is_on: coord_descrs[0] == "on",
                cutouts: vec![]
            }
        )
    }
    instructions
}

struct Instruction {
    cuboid: Cuboid,
    cutouts: Vec<Cuboid>,
    is_on: bool,
}

impl Instruction {
    fn remove(&mut self, c: &Cuboid) {
        if let Some(intersection) = self.cuboid.intersect(c) {
            self.cutouts.push(intersection);
        }
    }

    fn volume(&self) -> usize {
        if !self.is_on {
            return 0
        }
        let mut volume = self.cuboid.volume() as isize;
        let mut sign = -1;
        let mut intersections: Vec<(Vec<usize>, Cuboid)> =
            self.cutouts.iter().enumerate().map(|(i, c)| (vec![i], c.clone())).collect();
        while !intersections.is_empty() {
            let dvolume = intersections.iter().map(|(_cs, intr)| intr.volume()).sum::<usize>() as isize;
            volume += sign * dvolume;
            sign *= -1;
            intersections = intersect(&self.cutouts, intersections);
        }
        if volume < 0 || volume > self.cuboid.volume() as isize {
            panic!("oops");
        }
        return volume as usize;
    }
}

fn intersect(cs: &Vec<Cuboid>, old_intersections: Vec<(Vec<usize>, Cuboid)>) -> Vec<(Vec<usize>, Cuboid)> {
    let mut new_intersections = vec![];
    for (old_cubes, intersection) in old_intersections {
        for (i, c) in cs.iter().enumerate() {
            if i <= old_cubes[old_cubes.len() - 1] {
                continue;
            }
            if let Some(new_intersection) = intersection.intersect(c) {
                let mut new_cubes = old_cubes.clone();
                new_cubes.push(i);
                new_intersections.push((new_cubes, new_intersection));
            }
        }
    }
    return new_intersections;
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
struct Cuboid {
    x: Range,
    y: Range,
    z: Range,
}

impl Cuboid {
    fn volume(&self) -> usize {
        self.x.len() * self.y.len() * self.z.len()
    }

    fn intersect(&self, other: &Cuboid) -> Option<Cuboid> {
        let x = self.x.intersect(&other.x);
        let y = self.y.intersect(&other.y);
        let z = self.z.intersect(&other.z);
        if x.is_some() && y.is_some() && z.is_some() {
            return Some(Cuboid {
                x: x.unwrap(),
                y: y.unwrap(),
                z: z.unwrap(),
            })
        }
        return None
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
struct Range {
    min: isize,
    max: isize,
}

impl Range {
    fn parse(descr: &str) -> Range {
        let coords: Vec<&str> = descr.split(&['=', '.'][..]).filter(|c| *c != "").collect();
        let min = coords[1].parse().unwrap();
        let max = coords[2].parse().unwrap();
        Range {min, max}
    }

    fn len(&self) -> usize {
        (self.max + 1 - self.min) as usize
    }

    fn intersect(&self, other: &Range) -> Option<Range> {
        let min = cmp::max(self.min, other.min);
        let max = cmp::min(self.max, other.max);
        if min <= max {
            Some(Range{min,max})
        } else {
            None
        }
    }
}