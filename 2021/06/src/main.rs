fn main() {
    let mut age_distr: [u64; 9] = [0; 9];
    include_str!("input.txt")
        .split(",")
        .map(|x| x.parse().unwrap())
        .for_each(|x: usize| age_distr[x] += 1);
    
    for _i in 0..256 {
        advance(&mut age_distr);
    }
    println!("{}", age_distr.iter().sum::<u64>());
}

fn advance(ages: &mut [u64; 9]) {
    let breeding = ages[0];
    for i in 0..=7 {
        ages[i] = ages[i+1]
    }
    ages[8] = breeding;
    ages[6] += breeding;
}