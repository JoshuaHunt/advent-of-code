use std::collections::BinaryHeap;
use std::cmp::{Ord, Reverse};
use std::{cmp,fmt};

fn main() {
    let start = parse_board(include_str!("input.txt"));
    println!("{}", start);
    let mut queue = BinaryHeap::new();
    queue.push(Reverse(start));
    
    let mut iter = 0;
    while let Some(Reverse(current)) = queue.pop() {
        iter += 1;
        if iter % 1000 == 0 {
            println!("{}\n", current);
            println!("Queue has size {}", queue.len());
        }

        if current.is_finished() {
            println!("Cost {}", current.cost);
            break
        }
        for next in current.next_positions() {
            queue.push(Reverse(next));
        }
    }
}

fn parse_board(input: &str) -> Board {
    let mut board = Board {
        rooms: [['.'; 2]; 9],
        cost: 0
    };
    board.rooms[2][1] = '#';
    board.rooms[4][1] = '#';
    board.rooms[6][1] = '#';

    board.rooms[1][0] = input.chars().nth(31).unwrap();
    board.rooms[3][0] = input.chars().nth(33).unwrap();
    board.rooms[5][0] = input.chars().nth(35).unwrap();
    board.rooms[7][0] = input.chars().nth(37).unwrap();
    board.rooms[1][1] = input.chars().nth(45).unwrap();
    board.rooms[3][1] = input.chars().nth(47).unwrap();
    board.rooms[5][1] = input.chars().nth(49).unwrap();
    board.rooms[7][1] = input.chars().nth(51).unwrap();
    return board;
}

#[derive(PartialEq, Eq, Ord, PartialOrd, Clone, Debug)]
struct Board {
    cost: usize,
/*
#############
#00.2.4.6.88#
###1#3#5#7###
  #1#3#5#7#
  #########
*/
    rooms: [[char; 2]; 9],
}


impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "#############")?;
        writeln!(f, "#{}{}.{}.{}.{}.{}{}#", 
            self.rooms[0][1],
            self.rooms[0][0],
            self.rooms[2][0],
            self.rooms[4][0],
            self.rooms[6][0],
            self.rooms[8][0],
            self.rooms[8][1],
        )?;
        writeln!(f, "###{}#{}#{}#{}###",
            self.rooms[1][0],
            self.rooms[3][0],
            self.rooms[5][0],
            self.rooms[7][0],
        )?;
        writeln!(f, "  #{}#{}#{}#{}#  ",
            self.rooms[1][1],
            self.rooms[3][1],
            self.rooms[5][1],
            self.rooms[7][1],
        )?;
        writeln!(f, "  #########  ")?;
        write!(f, "  => {}", self.cost)?;
        Ok(())
    }
}

const HALLWAYS: [(usize, usize); 7] = [(0, 1), (0, 0), (2, 0), (4, 0), (6, 0), (8, 0), (8, 1)];

impl Board {
    fn new(descr: &str) -> Board {
        Board {
            rooms: [
                [descr.chars().nth(0).unwrap(), descr.chars().nth(1).unwrap()],
                [descr.chars().nth(2).unwrap(), descr.chars().nth(3).unwrap()],
                [descr.chars().nth(4).unwrap(), '#'], 
                [descr.chars().nth(5).unwrap(), descr.chars().nth(6).unwrap()],
                [descr.chars().nth(7).unwrap(), '#'],
                [descr.chars().nth(8).unwrap(), descr.chars().nth(9).unwrap()],
                [descr.chars().nth(10).unwrap(), '#'],
                [descr.chars().nth(11).unwrap(), descr.chars().nth(12).unwrap()],
                [descr.chars().nth(13).unwrap(), descr.chars().nth(14).unwrap()]],
            cost: 0
        }
    }

    fn is_finished(&self) -> bool {
        for bug in "ABCD".chars() {
            let dest_room = get_dest_room(bug);
            for i in 0..2 {
                if self.rooms[dest_room][i] != bug {
                    return false
                }
            }
        }
        return true
    }

    fn is_impossible(&self) -> bool {
        let mut bugs_in_corridor = false;
        for (start_room, room_i) in HALLWAYS {
            let bug = self.rooms[start_room][room_i];
            if bug != '.' {
                bugs_in_corridor = true;
                let dest_room = get_dest_room(bug);
                if self.is_hallway_clear(start_room, dest_room) {
                    return false;
                }
            }
        }
        return bugs_in_corridor
    }

    fn next_positions(&self) -> Vec<Board> {
        if self.is_impossible() {
            return vec![]
        }
        let pos = self.from_hallway_positions();
        if !pos.is_empty() {
            return pos;
        }
        return self.to_hallway_positions();
    }

    fn from_hallway_positions(&self) -> Vec<Board> {
        let mut nexts = Vec::new();
        for (start_room, start_i) in HALLWAYS {
            let bug = self.rooms[start_room][start_i];
            if bug == '.' {
                continue;
            }
            let dest_room = get_dest_room(bug);
            for dest_i in [0, 1] {
                if dest_i == 0 && self.rooms[dest_room][1] == '.' {
                    continue;
                }
                if let Some(steps) = self.steps_needed((start_room, start_i), (dest_room, dest_i)) {
                    if self.has_stranger(bug, dest_room) {
                        continue;
                    }
                    let mut next = self.clone();
                    next.rooms[start_room][start_i] = '.';
                    next.rooms[dest_room][dest_i] = bug;
                    next.cost += get_cost(bug, steps);
                    nexts.push(next);
                }
            }
        }
        return nexts;
    }

    fn to_hallway_positions(&self) -> Vec<Board> {
        let mut nexts = Vec::new();
        for source_room in [1, 3, 5, 7] {
            for source_i in [0, 1] {
                let bug = self.rooms[source_room][source_i];
                if bug == '.' {
                    continue;
                }
                if get_dest_room(bug) == source_room && (source_i == 1 || self.rooms[source_room][1] == bug) {
                    continue;
                }
                for (dest_room, dest_i) in HALLWAYS {
                    if let Some(steps) = self.steps_needed((source_room, source_i), (dest_room, dest_i)) {
                        let mut next = self.clone();
                        next.rooms[source_room][source_i] = '.';
                        next.rooms[dest_room][dest_i] = bug;
                        next.cost += get_cost(bug, steps);
                        nexts.push(next);
                    }
                }
            }
        }
        return nexts;
    }

    fn steps_needed(&self, (start_room, start_i): (usize, usize), (dest_room, dest_i): (usize, usize)) -> Option<usize> {
        // check hallway entrances clear:
        if start_i == 1 && self.rooms[start_room][0] != '.' {
            return None;
        }
        if dest_i == 1 && self.rooms[dest_room][0] != '.' {
            return None;
        }
        // check dest is not full
        if self.rooms[dest_room][dest_i] != '.' {
            return None
        }
        // check hallways between start and dest:
        if !self.is_hallway_clear(start_room, dest_room) {
            return None
        }
        let steps = 1 + (dest_room as isize - start_room as isize).abs() as usize + start_i + dest_i;
        return Some(steps);
    }

    fn has_stranger(&self, bug: char, dest_room: usize) -> bool {
        (self.rooms[dest_room][0] != '.' && self.rooms[dest_room][0] != bug)
            || (self.rooms[dest_room][1] != '.' && self.rooms[dest_room][1] != bug)
    }

    fn is_hallway_clear(&self, start_room: usize, dest_room: usize) -> bool {
        let min = cmp::min(start_room, dest_room);
        let max = cmp::max(start_room, dest_room);
        for room in (min+1)..max {
            if room % 2 == 0 && self.rooms[room][0] != '.' {
                return false
            }
        }
        return true;
    }
}

fn get_dest_room(bug: char) -> usize {
    (bug as usize - 'A' as usize) * 2 + 1
}

fn get_cost(bug: char, steps: usize) -> usize {
    let cost = (10 as usize).pow(bug as u32 - 'A' as u32);
    cost * steps
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_cost_test() {
        assert_eq!(get_cost('B', 2), 20);
    }

    #[test]
    fn next_positions_doesnt_move_out_of_dest() {
        let board = Board::new("..AA.BB.CC.DD..");
        assert_eq!(board.next_positions(), vec![]);
    }

    #[test]
    fn detects_finish() {
        let board = Board::new("..AA.BB.CC.DD..");
        assert_eq!(board.is_finished(), true);
    }

    #[test]
    fn only_moves_into_dest() {
        let board = Board::new("A..A..B.CC.DD.B");
        assert_eq!(
            board.next_positions().into_iter().map(|mut b| {
                b.cost = 0;
                b
            }).collect::<Vec<Board>>(), 
            vec![
                Board::new("..AA..B.CC.DD.B"),
                Board::new("A..A.BB.CC.DD..")
            ]
        )
    }

    #[test]
    fn is_impossible() {
        let board = Board::new("..AA..BDCCB.D..");
        assert_eq!(
            board.is_impossible(),
            true
        );
        let board = Board::new("A..A..B.CC.DD.B");
        assert_eq!(
            board.is_impossible(),
            false,
        );
    }
}