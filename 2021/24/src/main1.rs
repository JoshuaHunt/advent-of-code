use std::collections::{HashMap, HashSet};

fn main() {
    let mut arena = Arena::new();
    let tree = build_tree(include_str!("input.txt").lines().take(14 * 18), &mut arena);
    // let tree = arena.simplify(tree);
    arena.set_inputs(vec![9,9,9,1,9,9,4,7,2,8,2,9,3,1]);
    // arena.print(NodeId{id: 506}, 0);
    // println!("{}",arena.format(NodeId{id: 506}));
    let result = arena.evaluate(tree, 0);
    println!("{}", result);
    // arena.print(tree, 0);
    // println!("{}", arena.active_nodes(tree, &mut HashSet::new()).len());
}

fn build_tree<'a, I>(descr: I, arena: &mut Arena) -> NodeId 
where I : Iterator<Item=&'a str>
{
    let mut trees = [arena.add_node(Node::Constant(0)); 4];

    for line in descr {
        let terms: Vec<&str> = line.split(" ").collect();
        let first_register = get_register_index(terms[1]).unwrap();
        let tree1 = trees[first_register];
        let tree2 = if terms.len() < 3 {
            None
        } else if let Some(i) = get_register_index(terms[2]) {
            Some(trees[i])
        } else {
            Some(arena.add_node(Node::Constant(terms[2].parse().unwrap())))
        };
        trees[first_register] = arena.add_instruction(terms[0], tree1, tree2);
    }
    trees[3]
}

fn get_register_index(reg: &str) -> Option<usize> {
    for (i, r) in ["w", "x", "y", "z"].into_iter().enumerate() {
        if *r == *reg {
            return Some(i)
        }
    }
    return None
}

struct Arena {
    nodes: Vec<Node>,
    input_nodes: Vec<NodeId>,
    input_values: Vec<isize>,
    cache: HashMap<NodeId, isize>,
    simplified_nodes: HashMap<NodeId, NodeId>,
    bounds_cache: HashMap<NodeId, [isize; 2]>,
}

impl Arena {
    fn new() -> Arena {
        Arena {
            nodes: vec![],
            input_nodes: vec![],
            input_values: vec![],
            cache: HashMap::new(),
            simplified_nodes: HashMap::new(),
            bounds_cache: HashMap::new(),
        }
    }

    fn add_node<'a>(&'a mut self, node: Node) -> NodeId {
        self.nodes.push(node);
        NodeId::new(self.nodes.len() - 1)
    }

    fn add_instruction(&mut self, instr: &str, tree0: NodeId, tree1: Option<NodeId>) -> NodeId {
        match instr {
            "inp" => {
                let id = self.add_node(Node::Input);
                self.input_nodes.push(id);
                id
            },
            "add" => {
                self.add_node(Node::Add(tree0, tree1.unwrap()))
            }
            "mod" => {
                self.add_node(Node::Mod(tree0, tree1.unwrap()))
            }
            "div" => {
                self.add_node(Node::Div(tree0, tree1.unwrap()))
            }
            "mul" => {
                self.add_node(Node::Mul(tree0, tree1.unwrap()))
            }
            "eql" => {
                self.add_node(Node::Eql(tree0, tree1.unwrap()))
            }
            _ => panic!("Unimplemented instruction {}", instr)
        }
    }

    fn evaluate(&mut self, id: NodeId, depth: usize) -> isize {
        let deeper = depth + 1;
        if let Some(&value) = self.cache.get(&id) {
            return value;
        }
        let value = match self.get(id).clone() {
            Node::Constant(val) => val,
            Node::Input => self.get_input_for(id),
            Node::Add(lhs, rhs) => self.evaluate(lhs, deeper) + self.evaluate(rhs, deeper),
            Node::Mod(lhs, rhs) => self.evaluate(lhs, deeper) % self.evaluate(rhs, deeper),
            Node::Div(lhs, rhs) => self.evaluate(lhs, deeper) / self.evaluate(rhs, deeper),
            Node::Mul(lhs, rhs) => self.evaluate(lhs, deeper) * self.evaluate(rhs, deeper),
            Node::Eql(lhs, rhs) => (self.evaluate(lhs, deeper) == self.evaluate(rhs, deeper)) as isize,
        };
        self.cache.insert(id, value);
        // println!("{} {}={:?} -> {}", ".".repeat(depth), id.id, self.get(id), value);
        value
    }

    fn try_evaluate(&mut self, id: NodeId) -> Option<isize> {
        match self.get(id).clone() {
            Node::Constant(val) => Some(val),
            Node::Add(lhs, rhs) => 
                self.try_evaluate(lhs).and_then(|l| self.try_evaluate(rhs).map(|r| r + l)),
            Node::Mod(lhs, rhs) => self.try_evaluate(lhs).and_then(|l| self.try_evaluate(rhs).map(|r| l % r)),
            Node::Div(lhs, rhs) => self.try_evaluate(lhs).and_then(|l| self.try_evaluate(rhs).map(|r| l / r)),
            Node::Mul(lhs, rhs) => self.try_evaluate(lhs).and_then(|l| self.try_evaluate(rhs).map(|r| l * r)),
            Node::Eql(lhs, rhs) => self.try_evaluate(lhs).and_then(|l| self.try_evaluate(rhs).map(|r| (r == l) as isize)),
            _ => None
        }
    }

    fn active_nodes<'a>(&mut self, id: NodeId, nodes: &'a mut HashSet<NodeId>) -> &'a HashSet<NodeId> {
        if nodes.contains(&id) {
            return nodes;
        }
        nodes.insert(id);
        match self.get(id).clone() {
            Node::Add(lhs, rhs) => {
                for n in self.active_nodes(lhs, nodes).clone() {
                    nodes.insert(n);
                }
                for n in self.active_nodes(rhs, nodes).clone() {
                    nodes.insert(n);
                }
            },
            Node::Mod(lhs, rhs) => {
                for n in self.active_nodes(lhs, nodes).clone() {
                    nodes.insert(n);
                }
                for n in self.active_nodes(rhs, nodes).clone() {
                    nodes.insert(n);
                }
            },
            Node::Div(lhs, rhs) => {
                for n in self.active_nodes(lhs, nodes).clone() {
                    nodes.insert(n);
                }
                for n in self.active_nodes(rhs, nodes).clone() {
                    nodes.insert(n);
                }
            },
            Node::Mul(lhs, rhs) => {
                for n in self.active_nodes(lhs, nodes).clone() {
                    nodes.insert(n);
                }
                for n in self.active_nodes(rhs, nodes).clone() {
                    nodes.insert(n);
                }
            },
            Node::Eql(lhs, rhs) => {
                for n in self.active_nodes(lhs, nodes).clone() {
                    nodes.insert(n);
                }
                for n in self.active_nodes(rhs, nodes).clone() {
                    nodes.insert(n);
                }
            },
            _ => {}
        }
        return nodes;
    }
    
    fn print(&mut self, id: NodeId, depth: usize) {
        print!("{}", ".".repeat(depth));
        let bounds = self.bounds(id);
        println!("{}={:?} -> {:?} in {:?}", id.id, self.get(id), self.cache.get(&id), bounds);
        let deeper = depth + 1;
        match self.get(id).clone() {
            Node::Add(lhs, rhs) => {
                self.print(lhs, deeper);
                self.print(rhs, deeper);
            },
            Node::Mod(lhs, rhs) => {
                self.print(lhs, deeper);
                self.print(rhs, deeper);
            },
            Node::Div(lhs, rhs) => {
                self.print(lhs, deeper);
                self.print(rhs, deeper);
            },
            Node::Mul(lhs, rhs) => {
                self.print(lhs, deeper);
                self.print(rhs, deeper);
            },
            Node::Eql(lhs, rhs) => {
                self.print(lhs, deeper);
                self.print(rhs, deeper);
            },
            _ => {}
        }
    }

    fn simplify(&mut self, id: NodeId) -> NodeId {
        if let Some(&new_id) = self.simplified_nodes.get(&id) {
            return new_id;
        }
        // println!("Simplifying {} = {:?}", id.id, self.get(id));
        let mut new_id = match self.get(id).clone() {
            Node::Add(lhs, rhs) => {
                let new_lhs = self.simplify(lhs);
                let new_rhs = self.simplify(rhs);
                if lhs != new_lhs || rhs != new_rhs {
                    self.add_node(Node::Add(new_lhs, new_rhs))
                } else {
                    id
                }
            },
            Node::Mod(lhs, rhs) => {
                let new_lhs = self.simplify(lhs);
                let new_rhs = self.simplify(rhs);
                if lhs != new_lhs || rhs != new_rhs {
                    self.add_node(Node::Mod(new_lhs, new_rhs))
                } else {
                    id
                }
            },
            Node::Div(lhs, rhs) => {
                let new_lhs = self.simplify(lhs);
                let new_rhs = self.simplify(rhs);
                if lhs != new_lhs || rhs != new_rhs {
                    self.add_node(Node::Div(new_lhs, new_rhs))
                } else {
                    id
                }
            },
            Node::Mul(lhs, rhs) => {
                let new_lhs = self.simplify(lhs);
                let new_rhs = self.simplify(rhs);
                if lhs != new_lhs || rhs != new_rhs {
                    self.add_node(Node::Mul(new_lhs, new_rhs))
                } else {
                    id
                }
            },
            Node::Eql(lhs, rhs) => {
                let new_lhs = self.simplify(lhs);
                let new_rhs = self.simplify(rhs);
                if lhs != new_lhs || rhs != new_rhs {
                    self.add_node(Node::Eql(new_lhs, new_rhs))
                } else {
                    id
                }
            },
            _ => id
        };

        // 0 operand simplifications
        new_id = match self.get(new_id).clone() {
            Node::Mul(lhs, rhs) =>
                if *self.get(rhs) == Node::Constant(0) {
                    // println!("{} simplifies: mul ? 0 shortcircuit", new_id.id);
                    rhs
                } else if *self.get(lhs) == Node::Constant(0) {
                    // println!("{} simplifies: mul 0 ? shortcircuit", new_id.id);
                    lhs
                } else if *self.get(rhs) == Node::Constant(1) {
                    // println!("{} simplifies: mul ? 1 shortcircuit", new_id.id);
                    lhs
                } else {
                    new_id
                },
            Node::Add(lhs, rhs) => 
                if *self.get(rhs) == Node::Constant(0) {
                    // println!("{} simplifies: add ? 0 shortcircuit", new_id.id);
                    lhs
                } else if *self.get(lhs) == Node::Constant(0) {
                    // println!("{} simplifies: add 0 ? shortcircuit", new_id.id);
                    rhs
                } else {
                    new_id
                },
            Node::Div(lhs, rhs) => 
                if *self.get(lhs) == Node::Constant(0) {
                    // println!("{} simplifies: div 0 ? shortcircuit", new_id.id);
                    lhs
                } else if *self.get(rhs) == Node::Constant(1) {
                    // println!("{} simplifies: div ? 1 shortcircuit", new_id.id);
                    lhs 
                } else {
                    new_id
                },
            Node::Mod(lhs, _rhs) => 
                if *self.get(lhs) == Node::Constant(0) {
                    // println!("{} simplifies: mod 0 ? shortcircuit", new_id.id);
                    lhs
                } else {
                    new_id
                },
            Node::Eql(lhs, rhs) => {
                let lbounds = self.bounds(lhs);
                let rbounds = self.bounds(rhs);
                if lbounds[1] < rbounds[0] || rbounds[1] < lbounds[0] {
                    self.add_node(Node::Constant(0))
                } else {
                    new_id
                }
                
            },
            _ => new_id
        };

        if let Some(val) = self.try_evaluate(new_id) {
            // println!("{} simplifies: constant tree shortcircuit", new_id.id);
            new_id = self.add_node(Node::Constant(val));
        }
        
        self.simplified_nodes.insert(id, new_id);
        // println!("Inserting {} -> {} = {:?}", id.id, new_id.id, self.get(new_id));
        return new_id;
    }

    fn get<'a>(&'a self, id: NodeId) -> &'a Node {
        &self.nodes[id.id]
    }

    fn get_input_for(&self, id: NodeId) -> isize {
        self.input_nodes.iter().position(|&i| i == id).map(|i| self.input_values[i]).unwrap()
    }

    fn set_inputs(&mut self, inputs: Vec<isize>) {
        self.input_values = inputs
    }

    fn bounds(&mut self, id: NodeId) -> [isize; 2] {
        if let Some(&b) = self.bounds_cache.get(&id) {
            return b;
        }
        let b = match self.get(id).clone() {
            Node::Constant(val) => [val, val],
            Node::Input => [0, 9],
            Node::Add(lhs, rhs) => {
                let [lmin, lmax] = self.bounds(lhs);
                let [rmin, rmax] = self.bounds(rhs);
                [lmin + rmin, lmax + rmax]
            }
            Node::Mod(lhs, rhs) => [0, self.bounds(rhs)[1]],
            Node::Div(lhs, rhs) => {
                let [lmin, lmax] = self.bounds(lhs);
                let [rmin, rmax] = self.bounds(rhs);
                if lmin < 0 || rmin < 0 {
                    panic!("No div bounds");
                }
                [lmin / rmax, lmax / rmin]
            },
            Node::Mul(lhs, rhs) => {
                let [lmin, lmax] = self.bounds(lhs);
                let [rmin, rmax] = self.bounds(rhs);
                if lmin < 0 || rmin < 0 {
                    panic!("No mul bounds");
                }
                [lmin * rmin, lmax * rmax]
            },
            Node::Eql(lhs, rhs) => {
                let [lmin, lmax] = self.bounds(lhs);
                let [rmin, rmax] = self.bounds(rhs);
                if rmax < lmin || lmax < rmin {
                    [0, 0]
                } else if lmax == lmin && lmin == rmin && rmin == rmax {
                    [1, 1]
                } else {
                    [0, 1]
                }
            },
        };
        self.bounds_cache.insert(id, b);
        return b;
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
struct NodeId {
    id: usize
}

impl NodeId {
    fn new(id: usize) -> NodeId {
        NodeId {id}
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Node {
    Constant(isize),
    Input,
    Add(NodeId, NodeId),
    Mod(NodeId, NodeId),
    Div(NodeId, NodeId),
    Mul(NodeId, NodeId),
    Eql(NodeId, NodeId),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn inp_works() {
        let mut arena = Arena::new();
        let tree = build_tree(vec!["inp z"].into_iter(), &mut arena);
        arena.set_inputs(vec![7]);
        let result = arena.evaluate(tree, 0);
        assert_eq!(result, 7);
    }

    #[test]
    fn add_works() {
        let mut arena = Arena::new();
        let tree = build_tree(vec![
            "inp x",
            "inp z",
            "add z x"
        ].into_iter(), &mut arena);
        arena.set_inputs(vec![3, 5]);
        let result = arena.evaluate(tree, 0);
        assert_eq!(result, 8);
    }

    #[test]
    fn simplity_mult_by_0() {
        let mut arena = Arena::new();
        let tree = build_tree(vec![
            "inp z",
            "mul z 0",
        ].into_iter(), &mut arena);
        let tree = arena.simplify(tree);
        let result = arena.evaluate(tree, 0);
        assert_eq!(result, 0);
    }

    #[test]
    fn simplify_nested_mult_by_0() {
        let mut arena = Arena::new();
        let tree = build_tree(vec![
            "inp x",
            "mul x 0",
            "inp z",
            "mul z 0",
            "add z x"
        ].into_iter(), &mut arena);
        let tree = arena.simplify(tree);
        let result = arena.evaluate(tree, 0);
        assert_eq!(result, 0);
    }
}