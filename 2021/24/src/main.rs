const CONSTANTS: [(isize, isize, isize); 14] = [
    (11, 1, 1),
    (10, 10, 1),
    (13, 2, 1),
    (-10, 5, 26),
    (11, 6, 1),
    (11, 0, 1),
    (12, 16, 1),

    (-11, 12, 26),
    (-7, 15, 26),
    (13, 7, 1),
    (-13, 6, 26),
    (0, 5, 26),
    (-11, 6, 26),
    (0, 15, 26),
];

fn main() {
    let mut inputs = vec![];
    let answer = search(&mut inputs, 0);
    println!("{:?}", answer);
}

fn search(inputs: &mut Vec<isize>, z: isize) -> Option<Vec<isize>> {
    if inputs.len() == 14 {
        if z == 0 {
            return Some(inputs.clone());
        } else {
            return None
        }
    }

    let i = inputs.len();
    let (n, m, d) = CONSTANTS[i];
    for w in 1..=9 {
        let x = (z % 26) + n;
        if ((i >= 7 && i != 9) || i == 3) && w != x {
            continue;
        }
        
        let mut z = z / d;
        if x != w {
            z = 26 * z + w + m;
        }
        inputs.push(w);
        let answer = search(inputs, z);
        if answer.is_some() {
            return answer;
        }
        inputs.pop();

    }

    return None;
}