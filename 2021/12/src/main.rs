use std::convert::TryFrom;
use std::collections::HashSet;

fn main() {
    let edges: Vec<Edge> = include_str!("input.txt").lines().map(|l| Edge::try_from(l).unwrap()).collect();
    let mut visited: HashSet<&Cave> = HashSet::new();
    let path_count = explore(&edges, &mut visited, /*double_visit=*/None, &Cave::new("start"));
    println!("{}", path_count);
}

fn explore<'a>(edges: &'a Vec<Edge>, visited: &mut HashSet<&'a Cave<'a>>, double_visit: Option<&'a Cave<'a>>, node: &'a Cave<'a>) -> i32 {
    if node.name == "end" {
        return 1
    }

    if node.is_small() {
        visited.insert(node);
    }

    let neighbours: Vec<&'a Cave<'a>> = 
        edges.iter()
            .flat_map(|e| e.opposite(node))
            .filter(|v| !visited.contains(v))
            .collect();

    let mut path_count = 0;
    for neighbour in neighbours {
        path_count += explore(edges, visited, double_visit, neighbour);
    }

    if double_visit.is_none() {
        let visited_neighbours: Vec<&'a Cave<'a>> = 
            edges.iter()
                .flat_map(|e| e.opposite(node))
                .filter(|v| visited.contains(v))
                .filter(|v| v.name != "start")
                .collect();
        for neighbour in visited_neighbours {
            path_count += explore(edges, visited, Some(neighbour), neighbour);
        }
    }

    if node.is_small() && double_visit.map(|v| v != node).unwrap_or(true) {
        visited.remove(node);
    }

    return path_count;
}

#[derive(Debug)]
struct Edge<'a> {
    v0: Cave<'a>,
    v1: Cave<'a>,
}

impl<'a> Edge<'a> {
    fn opposite(&self, node: &Cave) -> Option<&Cave> {
        if *node == self.v0 {
            Some(&self.v1)
        } else if *node == self.v1 {
            Some(&self.v0)
        } else {
            None
        }
    }
}

impl <'a> TryFrom<&'a str> for Edge<'a> {
    type Error = &'static str;

    fn try_from(text: &'a str) -> Result<Self, Self::Error> {
        let vs: Vec<&str> = text.split("-").collect();
        Ok(Edge {
            v0: Cave::new(vs[0]),
            v1: Cave::new(vs[1])
        })
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct Cave<'a> {
    name: &'a str,
}

impl<'a> Cave<'a> {
    fn new(name: &str) -> Cave {
        Cave {
            name
        }
    }

    fn is_small(&self) -> bool {
        self.name == self.name.to_ascii_lowercase()
    }
}