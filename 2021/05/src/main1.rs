use intervaltree::IntervalTree;
use intervaltree::Element;
use std::convert::TryFrom;
use std::cmp::{max, min};
use core::ops::Range;
use std::collections::HashSet;
use std::fmt::Display;
use std::fmt::Formatter;


fn main() {
    let lines: Vec<Line> = 
        include_str!("input.txt").lines()
            .map(Line::try_from)
            .map(Result::unwrap)
            .filter(Line::is_orthogonal)
            .collect();
    let horizontals: IntervalTree<i16, &Line> =
        lines.iter()
            .map(|l| (l.horizontal_span(), l))
            .map(Element::from)
            .collect();
    let verticals: IntervalTree<i16, &Line> =
        lines.iter()
            .map(|l| (l.vertical_span(), l))
            .map(Element::from)
            .collect();           

    let mut danger_count = 0;
    for x in 0..1000 {
        println!("{}", x);
        let h_intersects: HashSet<&Line> = horizontals.query_point(x).map(|e| e.value).collect();
        // println!("x = {} -> {:?}", x, h_intersects);
        for y in 0..1000 {
            let v_intersects: HashSet<&Line> = verticals.query_point(y).map(|e| e.value).collect();
            // println!("y = {} -> {:?}", y, v_intersects);
            if v_intersects.intersection(&h_intersects).nth(1).is_some() {
                danger_count += 1;
                // println!("({}, {}) -> {:?}", x, y, v_intersects.intersection(&h_intersects));
            }
        }
    }
    println!("{:?}", danger_count);
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct Point {
    x: i16,
    y: i16
}

impl Point {
    fn new(x: i16, y: i16) -> Point {
        Point {x, y}
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct Line {
    p1: Point,
    p2: Point
}

impl Line {
    fn new(p1: Point, p2: Point) -> Line {
        Line {p1, p2}
    }

    fn horizontal_span(&self) -> Range<i16> {
        Range {
            start: min(self.p1.x, self.p2.x),
            end: max(self.p1.x, self.p2.x) + 1
        }
    }

    fn vertical_span(&self) -> Range<i16> {
        Range {
            start: min(self.p1.y, self.p2.y),
            end: max(self.p1.y, self.p2.y) + 1
        }
    }

    fn is_orthogonal(&self) -> bool {
        self.p1.x == self.p2.x || self.p1.y == self.p2.y
    }
}

impl TryFrom<&str> for Line {
    type Error = &'static str;

    fn try_from(text: &str) -> Result<Self, Self::Error> {
        let xs: Vec<Result<i16, Self::Error>> = 
            text.split(" -> ")
                .flat_map(|s| s.split(","))
                .map(|x| x.parse().or(Err("Unable to parse line")))
                .collect();
        Ok(Line::new(Point::new(xs[0]?, xs[1]?), Point::new(xs[2]?, xs[3]?)))
    }
}

impl Display for Line {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{} -> {}", self.p1, self.p2)
    }
}