use std::convert::TryFrom;
use std::fmt::Display;
use std::fmt::Formatter;


fn main() {
    let lines: Vec<Line> = 
        include_str!("input.txt").lines()
            .map(Line::try_from)
            .map(Result::unwrap)
            .collect();
    let mut grid: [[i8; 1000]; 1000] = [[0; 1000]; 1000];
    for line in lines {
        for point in line {
            grid[point.x as usize][point.y as usize] += 1;
        }
    }

    let mut danger_count = 0;
    for x in 0..1000 {
        for y in 0..1000 {
            if grid[x][y] > 1 {
                danger_count += 1;
            }
        }    
    }
    println!("{:?}", danger_count);
}

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
struct Point {
    x: i16,
    y: i16
}

impl Point {
    fn new(x: i16, y: i16) -> Point {
        Point {x, y}
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct Line {
    p1: Point,
    p2: Point
}

impl Line {
    fn new(p1: Point, p2: Point) -> Line {
        Line {p1, p2}
    }
}

impl IntoIterator for Line {
    type Item = Point;
    type IntoIter = LineIterator;
    fn into_iter(self) -> Self::IntoIter {
        LineIterator::new(self.p1, self.p2)
    }
}

struct LineIterator {
    dx: i16,
    dy: i16,
    end: Point,
    current: Point,
}

impl LineIterator {
    fn new(start: Point, end: Point) -> LineIterator {
        let dx = sign(end.x - start.x);
        let dy = sign(end.y - start.y);
        LineIterator {
            dx,
            dy,
            end: Point::new(end.x + dx, end.y + dy),
            current: Point::new(start.x - dx, start.y - dy),
        }
    }
}

fn sign(x: i16) -> i16 {
    if x.abs() > 0 {
        x / x.abs()
    } else {
        0
    }
}

impl Iterator for LineIterator {
    type Item = Point;

    fn next(&mut self) -> Option<Self::Item> {
        self.current.x += self.dx;
        self.current.y += self.dy;
        if self.current == self.end {
            None
        } else {
            Some(self.current)
        }
    }
}

impl TryFrom<&str> for Line {
    type Error = &'static str;

    fn try_from(text: &str) -> Result<Self, Self::Error> {
        let xs: Vec<Result<i16, Self::Error>> = 
            text.split(" -> ")
                .flat_map(|s| s.split(","))
                .map(|x| x.parse().or(Err("Unable to parse line")))
                .collect();
        Ok(Line::new(Point::new(xs[0]?, xs[1]?), Point::new(xs[2]?, xs[3]?)))
    }
}

impl Display for Line {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{} -> {}", self.p1, self.p2)
    }
}