use std::collections::HashSet;

#[derive(Debug)]
struct Board {
    cells: [i32; 25]
}

const BINGO_LINES: [[usize; 5]; 10] = 
  [[ 0, 1, 2, 3, 4],
   [ 5, 6, 7, 8, 9],
   [10,11,12,13,14],
   [15,16,17,18,19],
   [20,21,22,23,24],
   
   [ 0, 5,10,15,20],
   [ 1, 6,11,16,21],
   [ 2, 7,12,17,22],
   [ 3, 8,13,18,23],
   [ 4, 9,14,19,24]];

impl Board {
    fn read_from<I>(input: &mut I) -> Board where I: Iterator<Item = i32> {
        let mut cells = [0; 25];
        for i in 0..25 {
            cells[i] = input.next().unwrap();
        }
        return Board {
            cells,
        }
    }

    fn has_bingo(&self, called: &HashSet<i32>) -> bool {
        BINGO_LINES.iter().any(|line| self.has_bingo_on(line, called))
    }

    fn has_bingo_on(&self, line: &[usize; 5], called: &HashSet<i32>) -> bool {
        line.iter().all(|cell| called.contains(&self.cells[*cell]))
    }

    fn score(&self, called: &HashSet<i32>) -> i32 {
        self.cells.iter().filter(|v| !called.contains(v)).sum()
    }
}

fn main() {
    let mut lines = include_str!("input.txt").lines();
    let to_call: Vec<i32> = parse_called_numbers(&mut lines);
    let boards: Vec<Board> = parse_boards(lines);

    let mut called: HashSet<i32> = HashSet::new();
    let mut last_board: Option<&Board> = None;
    for v in to_call {
        called.insert(v);
        let unfinished_count = boards.iter().filter(|b| !b.has_bingo(&called)).count();
        if unfinished_count == 1 {
            last_board = boards.iter().filter(|b| !b.has_bingo(&called)).next();
        } else if unfinished_count == 0 {
            println!("{:?}", called);
            let score = last_board.unwrap().score(&called);
            println!("{} * {} = {}", v, score, score * v);
            break;
        }
    }
}

fn parse_called_numbers<'a, I>(lines: &mut I) -> Vec<i32> 
where I: Iterator<Item = &'a str> {
    lines.next().unwrap().split(",").map(|x| x.parse().unwrap()).collect()
}

fn parse_boards<'a, I>(lines: I) -> Vec<Board>
where I: Iterator<Item = &'a str> {
    let mut num_values = lines.flat_map(str::split_whitespace).map(|x| x.parse::<i32>().unwrap()).peekable();
    let mut boards = Vec::new();
    while num_values.peek().is_some() {
        boards.push(Board::read_from(&mut num_values));
    }
    return boards;
}