use std::collections::HashSet;

fn main() {
    let mut input = include_str!("input.txt").lines();
    let mut dots = parse_dots(&mut input);
    let instructions = parse_instructions(input);
    for instruction in instructions {
        dots = fold(instruction, dots);
    }
    println!("{}", dots.len());
    for y in 0..7 {
        for x in 0..40 {
            if dots.contains(&Dot::new(x, y)) {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!("");
    }
}

fn fold(instruction: Instruction, dots: HashSet<Dot>) -> HashSet<Dot> {
    dots.into_iter()
        .map(|d| instruction.transform(d))
        .collect()
}

fn parse_dots<'a, T: Iterator<Item=&'a str>>(input: &mut T) -> HashSet<Dot> {
    let mut dots = HashSet::new();
    for line in input {
        if line.is_empty() {
            return dots
        }
        dots.insert(Dot::from(line));
    }
    return dots
}

fn parse_instructions<'a, T: Iterator<Item=&'a str>>(input: T) -> Vec<Instruction> {
    input.map(Instruction::from).collect()
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct Dot {
    x: i32,
    y: i32,
}

impl Dot {
    fn new(x: i32, y: i32) -> Dot {
        Dot { x, y }
    }
}

impl From<&str> for Dot {
    fn from(line: &str) -> Self {
        let xs: Vec<&str> = line.split(",").collect();
        Dot {
            x: xs[0].parse().unwrap(),
            y: xs[1].parse().unwrap(),
        }
    }
}

#[derive(Debug)]
enum Instruction {
    // Dots move vertically, fold along y=, line is horizontal
    VERTICAL { y: i32},
    // Dots move horizontally, fold along x=, line is vertical
    HORIZONTAL { x: i32 },
}

impl Instruction {
    fn transform(&self, dot: Dot) -> Dot {
        match self {
            Instruction::VERTICAL{y} if dot.y > *y => 
                Dot::new(dot.x, y - (dot.y - y)),
            Instruction::HORIZONTAL{x} if dot.x > *x => 
                Dot::new(x - (dot.x - x), dot.y),
            _ => dot
        }
    }
}

impl From<&str> for Instruction {
    fn from(line: &str) -> Self {
        let intro_len = "fold along x=".len();
        if line.starts_with("fold along x=") {
            let x = line[intro_len..].parse().unwrap();
            return Instruction::HORIZONTAL{x};
        } else if line.starts_with("fold along y=") {
            let y = line[intro_len..].parse().unwrap();
            return Instruction::VERTICAL{y};
        }
        panic!("Not an instruction!");
    }
}