fn main() {
    let mut crabs: Vec<i32> = include_str!("input.txt").split(",").map(|x| x.parse().unwrap()).collect();
    crabs.sort();
    main1(&crabs);
    main2(&crabs);
}

fn main1(crabs: &Vec<i32>) {
    let median: i32 = crabs[crabs.len() / 2];
    let cost: i32 = crabs.iter().map(|x| (x - median).abs()).sum();
    println!("{}", cost);
}

fn main2(crabs: &Vec<i32>) {
    let mean = crabs.iter().sum::<i32>() / (crabs.len() as i32);
    let cost: i32 =
        crabs.iter()
            .map(|y| (y - mean).abs())
            .map(|d| (d*d + d)/2)
            .sum();
    println!("{} {}", mean, cost);
}