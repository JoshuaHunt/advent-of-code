use strum::IntoEnumIterator;
use strum_macros::EnumIter;
use nalgebra::{Rotation3, Vector3};
use std::ops::{Add, Sub};
use std::collections::HashSet;
use std::marker::Copy;

const DEFAULT_ORIENTATION: Orientation = Orientation::PxPy;

fn main() {
    let mut scanners: Vec<Scanner> = parse_scanners();
    let mut to_fix: Vec<FixedScanner> = vec![FixedScanner::new(scanners.remove(0), AbsPos{x:0, y:0, z: 0}, DEFAULT_ORIENTATION)];
    let mut fixed_scanners: Vec<FixedScanner> = Vec::new();
    
    while scanners.len() > 0 {
        let fs = to_fix[0].clone();
        let mut i = 0;
        loop {
            if let Some((pos, o)) = possible_positions(&fs, &scanners[i]) {
                let scanner = scanners.remove(i);
                println!("Scanner {} matched scanner {}", scanner.id, fs.scanner.id);
                to_fix.push(FixedScanner::new(scanner, pos, o));
            } else {
                i += 1;
            }
            if i >= scanners.len() {
                break;
            }
        }
        fixed_scanners.push(to_fix.remove(0));
        println!("{} scanners unmatched, {} in fixed queue", scanners.len(), to_fix.len());
    }
    fixed_scanners.append(&mut to_fix);

    let beacons: HashSet<AbsPos> = fixed_scanners.iter().flat_map(|fs| fs.beacons.clone()).collect();
    println!("{} beacons", beacons.len());

    let mut distances: Vec<isize> = vec![];
    for s0 in &fixed_scanners {
        for s1 in &fixed_scanners {
            let d = (s0.pos.x - s1.pos.x).abs() + (s0.pos.y - s1.pos.y).abs() + (s0.pos.z - s1.pos.z).abs();
            distances.push(d);
        }
    }
    println!("{} dist", distances.iter().max().unwrap());
}

fn possible_positions(fs0: &FixedScanner, s1: &Scanner) -> Option<(AbsPos, Orientation)> {
    let mut positions = HashSet::new();
    for o1 in Orientation::iter() {
        for b1_rel in &s1.beacons {
            for b0 in &fs0.beacons {
                let s1_abs = implied_position(b0, b1_rel, &o1);
                let s1_abs_beacons: HashSet<AbsPos> = s1.beacons.iter().map(|b1| b1.to_absolute(&s1_abs, &o1)).collect();
                let overlap: HashSet<&AbsPos> = s1_abs_beacons.intersection(&fs0.beacons).collect();
                if overlap.len() >= 12 {
                    positions.insert((s1_abs, o1));
                }
            }    
        }
    }
    if positions.len() > 1 {
        panic!("Got multiple possible positions");
    }
    return positions.into_iter().next();
}

/** 
 * Returns the position that scanner 1 would have to be in to see the absolute position b0 in
 * relative position b1 while having orientation o1. 
 */
fn implied_position(b0: &AbsPos, b1: &RelPos, o1: &Orientation) -> AbsPos {
    // Conversion of b1 to absolute reference frame:
    // (b1 rotated from o1 to default) + scanner1.abs_pos
    b0 - &o1.rotate_to_default_orientation(b1)
}

fn parse_scanners() -> Vec<Scanner> {
    let mut input = include_str!("input.txt").lines().skip(1);
    let mut id = 0;
    let mut scanner = Scanner::new(id);
    let mut scanners = Vec::new();
    while let Some(line) = input.next() {
        if line.starts_with("--- ") {
            scanners.push(scanner);
            id += 1;
            scanner = Scanner::new(id);
        } else if line.len() > 0 {
            let coords: Vec<isize> = line.split(",").map(|x| x.parse().unwrap()).collect();
            scanner.add_beacon(RelPos::new(coords[0], coords[1], coords[2]));
        }
    }       
    scanners.push(scanner);
    return scanners;
}

#[derive(Clone, Debug)]
struct Scanner {
    id: usize,
    beacons: Vec<RelPos>
}

#[derive(Clone)]
struct FixedScanner {
    scanner: Scanner,
    pos: AbsPos,
    orientation: Orientation,
    beacons: HashSet<AbsPos>,
}

impl FixedScanner {
    fn new(scanner: Scanner, pos: AbsPos, orientation: Orientation) -> FixedScanner {
        let beacons = scanner.beacons.iter().map(|b| b.to_absolute(&pos, &orientation)).collect();
        FixedScanner { scanner, pos, orientation, beacons }
    }
}

#[derive(Copy, Clone, Debug, EnumIter, Eq, PartialEq, Hash)]
enum Orientation {
    // Naming convention:
    // <axis you point along><up axis>
    // e.g. PxNY points along X axis and thinks negative Y is up
    PxPy,
    PxNy,
    PxPz,
    PxNz,
    NxPy,
    NxNy,
    NxPz,
    NxNz,

    PyPx,
    PyNx,
    PyPz,
    PyNz,
    NyPx,
    NyNx,
    NyPz,
    NyNz,

    PzPx,
    PzNx,
    PzPy,
    PzNy,
    NzPx,
    NzNx,
    NzPy,
    NzNy,
}

const PX: Vector3<f64> = Vector3::new(1.0, 0.0, 0.0);
const PY: Vector3<f64> = Vector3::new(0.0, 1.0, 0.0);
const PZ: Vector3<f64> = Vector3::new(0.0, 0.0, 1.0);
const NX: Vector3<f64> = Vector3::new(-1.0, 0.0, 0.0);
const NY: Vector3<f64> = Vector3::new(0.0, -1.0, 0.0);
const NZ: Vector3<f64> = Vector3::new(0.0, 0.0, -1.0);

impl Orientation {
    fn matrix(&self) -> Rotation3<f64> {
        match self {
            Orientation::PxPy => Rotation3::face_towards(&PX, &PY),
            Orientation::PxNy => Rotation3::face_towards(&PX, &NY),
            Orientation::PxPz => Rotation3::face_towards(&PX, &PZ),
            Orientation::PxNz => Rotation3::face_towards(&PX, &NZ),
            Orientation::NxPy => Rotation3::face_towards(&NX, &PY),
            Orientation::NxNy => Rotation3::face_towards(&NX, &NY),
            Orientation::NxPz => Rotation3::face_towards(&NX, &PZ),
            Orientation::NxNz => Rotation3::face_towards(&NX, &NZ),

            Orientation::PyPx => Rotation3::face_towards(&PY, &PX),
            Orientation::PyNx => Rotation3::face_towards(&PY, &NX),
            Orientation::PyPz => Rotation3::face_towards(&PY, &PZ),
            Orientation::PyNz => Rotation3::face_towards(&PY, &NZ),
            Orientation::NyPx => Rotation3::face_towards(&NY, &PX),
            Orientation::NyNx => Rotation3::face_towards(&NY, &NX),
            Orientation::NyPz => Rotation3::face_towards(&NY, &PZ),
            Orientation::NyNz => Rotation3::face_towards(&NY, &NZ),

            Orientation::PzPy => Rotation3::face_towards(&PZ, &PY),
            Orientation::PzNy => Rotation3::face_towards(&PZ, &NY),
            Orientation::PzPx => Rotation3::face_towards(&PZ, &PX),
            Orientation::PzNx => Rotation3::face_towards(&PZ, &NX),
            Orientation::NzPy => Rotation3::face_towards(&NZ, &PY),
            Orientation::NzNy => Rotation3::face_towards(&NZ, &NY),
            Orientation::NzPx => Rotation3::face_towards(&NZ, &PX),
            Orientation::NzNx => Rotation3::face_towards(&NZ, &NX),
        }
    }

    fn rotate_to_default_orientation(&self, pos: &RelPos) -> RelPos {
        Orientation::rotate(self, &DEFAULT_ORIENTATION, pos)
    }

    fn rotate(o1: &Orientation, o2: &Orientation, pos: &RelPos) -> RelPos {
        let v = Vector3::new(pos.x as f64, pos.y as f64, pos.z as f64);
        let rotated_v = o2.matrix().transform_vector(&o1.matrix().inverse_transform_vector(&v));
        RelPos { 
            x: rotated_v.x.round() as isize, 
            y: rotated_v.y.round() as isize, 
            z: rotated_v.z.round() as isize
        }
    }
}

impl Scanner {
    fn new(id: usize) -> Scanner {
        Scanner {
            id,
            beacons: Vec::new()
        }
    }

    fn add_beacon(&mut self, pos: RelPos) {
        self.beacons.push(pos);
    }
}

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
struct AbsPos {
    x: isize,
    y: isize,
    z: isize,
}

impl Add<&RelPos> for &AbsPos {
    type Output = AbsPos;

    fn add(self, other: &RelPos) -> Self::Output {
        AbsPos {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl Sub<&RelPos> for &AbsPos {
    type Output = AbsPos;

    fn sub(self, other: &RelPos) -> Self::Output {
        AbsPos {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

#[derive(Clone, Debug)]
struct RelPos {
    x: isize,
    y: isize,
    z: isize,
}

impl RelPos {
    fn new(x: isize, y: isize, z: isize) -> RelPos {
        RelPos { x, y, z}
    }

    fn to_absolute(&self, origin: &AbsPos, orientation: &Orientation) -> AbsPos {
        origin + &orientation.rotate_to_default_orientation(&self)
    }
}