fn main() {
    let input = "target area: x=287..309, y=-76..-48";
    let target_y = (-76, -47);
    let target_x = (287, 310);
    // let target_y = (-10, -4);
    // let target_x = (20, 31);

    let dxs = get_dxs(target_x);
    let mut successes = 0;
    for dx in dxs {
        let dys = get_dys(target_x, target_y, dx);
        successes += dys.len();
        println!("{}: +{}", dx, dys.len());
    }
    println!("{}", successes);
}

fn get_dys(target_x: (isize, isize), target_y: (isize, isize), dx0: isize) -> Vec<isize> {
    (-1000..1000).filter(|dy| lands_in(target_x, target_y, dx0, *dy)).collect()
}

fn lands_in((minx, maxx): (isize, isize), (miny, maxy): (isize, isize), dx0: isize, dy0: isize) -> bool {
    let mut x = 0;
    let mut y = 0;
    let mut dx = dx0;
    let mut dy = dy0;
    while y >= miny {
        x += dx;
        y += dy;

        if dx > 0 { 
            dx -= 1;
        }
        dy -= 1;
        if minx <= x && x < maxx && miny <= y && y < maxy {
            return true;
        }
    }
    return false;
}

fn get_dxs(target_x: (isize, isize)) -> Vec<isize> {
    (1..10000).filter(|dx| x_lands_in(target_x, *dx)).collect()
}

fn x_lands_in((min_x, max_x): (isize, isize), dx0: isize) -> bool {
    let mut dx = dx0;
    let mut x = 0;
    while x < min_x {
        x += dx;
        if dx > 0 {
            dx -= 1;
        }
        if dx == 0 {
            break;
        }
    }
    return min_x <= x && x < max_x;
}