fn main() {
    let lines = include_str!("day2.txt").lines();
    let mut horizontal = 0;
    let mut depth = 0;
    for line in lines {
        if line.starts_with("forward ") {
            let delta: i32 = line.replace("forward ", "").parse().unwrap();
            horizontal += delta;
        } else if line.starts_with("down ") {
            let delta: i32 = line.replace("down ", "").parse().unwrap();
            depth += delta;
        } else if line.starts_with("up ") {
            let delta: i32 = line.replace("up ", "").parse().unwrap();
            depth -= delta;
        }
    }
    println!("{}", horizontal * depth);
}