fn main() {
    let lines = include_str!("day2.txt").lines();
    let mut horizontal = 0;
    let mut depth = 0;
    let mut aim = 0;
    for line in lines {
        if line.starts_with("forward ") {
            let delta: i32 = line.replace("forward ", "").parse().unwrap();
            horizontal += delta;
            depth += aim * delta;
        } else if line.starts_with("down ") {
            let delta: i32 = line.replace("down ", "").parse().unwrap();
            aim += delta;
        } else if line.starts_with("up ") {
            let delta: i32 = line.replace("up ", "").parse().unwrap();
            aim -= delta;
        }
    }
    println!("{}", horizontal * depth);
}