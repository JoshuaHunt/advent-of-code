use std::collections::HashMap;
use std::convert::TryFrom;

#[derive(Debug)]
struct Position {
    x: i32,
    depth: i32
}

impl Position {
    fn origin() -> Position {
        Position::new(0, 0)
    }

    fn new(x: i32, depth: i32) -> Position {
        Position {x, depth}
    }

    fn translate(&self, delta: Movement) -> Position {
        match delta {
            Movement::Forward(dx) => Position::new(self.x + dx, self.depth),
            Movement::Down(dz) => Position::new(self.x, self.depth + dz),
            Movement::Up(dz) => Position::new(self.x, self.depth - dz),
        }
    }
}

enum Movement {
    Forward(i32),
    Down(i32),
    Up(i32),
}

impl TryFrom<&str> for Movement {
    type Error = &'static str;

    fn try_from(text: &str) -> Result<Self, Self::Error> {
        let names = HashMap::from([
            ("forward ", Movement::Forward as fn(i32) -> Movement),
            ("down ", Movement::Down as fn(i32) -> Movement),
            ("up ", Movement::Up as fn(i32) -> Movement),
        ]);
        for key in names.keys() {
            if text.starts_with(key) {
                let delta: i32 = text.replace(key, "").parse().or(Err("Can't parse delta"))?;
                return Ok(names[key](delta));
            }
        }
        Err("Not a valid movement")
    }
}

fn main() {
    let lines = include_str!("day2.txt").lines();
    let mut position = Position::origin();

    for line in lines {
        let movement = Movement::try_from(line).unwrap();
        position = position.translate(movement);
    }
    println!("{}", position.x * position.depth);
}