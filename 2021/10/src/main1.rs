use std::collections::HashMap;

fn main() {
    let braces = HashMap::from([(')', '('), (']', '['), ('}', '{'), ('>', '<')]);
    let brace_points = HashMap::from([(')', 3), (']', 57), ('}', 1197), ('>', 25137)]);
    let mut points = 0;
    let mut stack: Vec<char> = Vec::new();
    
    for line in include_str!("input.txt").lines() {
        for c in line.chars() {
            if braces.contains_key(&c) {
                if let Some(open) = stack.pop() {
                    if open != *braces.get(&c).unwrap() {
                        points += brace_points.get(&c).unwrap();
                    }
                }
            } else {
                stack.push(c);
            }
        }
    }
    println!("{}", points);
}
