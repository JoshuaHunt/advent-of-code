use std::collections::HashMap;

fn main() {
    let braces = HashMap::from([(')', '('), (']', '['), ('}', '{'), ('>', '<')]);
    let brace_points = HashMap::from([('(', 1), ('[', 2), ('{', 3), ('<', 4)]);
    let mut points = Vec::new();
    
    'outer: for line in include_str!("input.txt").lines() {
        let mut stack: Vec<char> = Vec::new();
        for c in line.chars() {
            if braces.contains_key(&c) {
                if let Some(open) = stack.pop() {
                    if open != *braces.get(&c).unwrap() {
                        // mismatched
                        continue 'outer;
                    }
                }
            } else {
                stack.push(c);
            }
        }
        let mut line_score: u64 = 0;
        while let Some(open) = stack.pop() {
            line_score = 5 * line_score + brace_points.get(&open).unwrap();
        }
        points.push(line_score);
    }
    points.sort();
    println!("{}", points[points.len() / 2]);
}
