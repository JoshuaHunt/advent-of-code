use std::fs::File;
use std::io::{BufRead, BufReader};
use std::collections::VecDeque;

fn main() {
    let file = File::open("./day1.txt").unwrap();
    let reader = BufReader::new(file);
    let mut measurements = VecDeque::<i32>::new();
    let mut increases = 0;

    for line in reader.lines() {
        let value: i32 = line.unwrap().parse().unwrap();
        measurements.push_back(value);

        if measurements.len() > 3 {
            let removed_value = measurements.pop_front().unwrap();
            if removed_value < value {
                increases += 1;
            }
        }
    }
    println!("{} increases", increases);
}