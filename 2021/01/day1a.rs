use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("./day1.txt").unwrap();
    let reader = BufReader::new(file);
    let mut old_value: Option<i32> = None;
    let mut increases = 0;

    for line in reader.lines() {
        let value: i32 = line.unwrap().parse().unwrap();
        if let Some(old_v) = old_value {
            if value > old_v {
                increases += 1;
            }
        }
        old_value = Some(value);
    }
    println!("{} increases", increases);
}