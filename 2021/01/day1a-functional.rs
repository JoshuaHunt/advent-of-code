use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let reader = BufReader::new(File::open("./day1.txt").unwrap());
    let xs: Vec<i32> = reader.lines().map(|line| line.unwrap().parse().unwrap()).collect();
    let increases = 
        xs.iter()
            .zip(xs.iter().skip(1))
            .filter(|(x, y): &(&i32, &i32)| x < y)
            .count();
    println!("{} increases", increases);
}