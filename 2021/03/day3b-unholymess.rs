fn main() {
    let lines: Vec<&str> = include_str!("input.txt").lines().collect();

    let gamma_len = lines[0].len();

    let input: Vec<i32> = lines.into_iter()
        .map(|x| i32::from_str_radix(x, 2).unwrap())
        .collect();

    let mut most_common_candidates = input.clone();
    let mut x = 1 << (gamma_len - 1);
    while most_common_candidates.len() > 1 {
        let one_count = most_common_candidates.iter().filter(|y| *y & x > 0).count();
        let filter_digit = if one_count as f32 >= (most_common_candidates.len() as f32) / 2.0 {
            1
        } else {
            0
        };
        most_common_candidates = most_common_candidates.into_iter().filter(|y| *y & x == filter_digit * x).collect();
        x = x >> 1;
    }
    let oxygen = most_common_candidates[0];    

    let mut least_common_candidates = input.clone();
    x = 1 << (gamma_len - 1);
    while least_common_candidates.len() > 1 {
        let one_count = least_common_candidates.iter().filter(|y| *y & x > 0).count();
        let filter_digit = if (one_count as f32) < (least_common_candidates.len() as f32) / 2.0 {
            1
        } else {
            0
        };
        least_common_candidates = least_common_candidates.into_iter().filter(|y| *y & x == filter_digit * x).collect();
        x = x >> 1;
    }
    let co2 = least_common_candidates[0];
    println!("{} * {} = {}", oxygen, co2, oxygen * co2);
}