fn main() {
    let lines: Vec<&str> = include_str!("input.txt").lines().collect();

    let gamma_threshold = lines.len() / 2;
    let gamma_len = lines[0].len();

    let input: Vec<i32> = lines.into_iter()
        .map(|x| i32::from_str_radix(x, 2).unwrap())
        .collect();
    let mut gamma = 0;
    for i in 0..gamma_len {
        let x = 1 << i;
        let one_count = input.iter().filter(|y| *y & x > 0).count();
        if one_count > gamma_threshold {
            gamma += x;
        }
    }
    let epsilon = ((1 << gamma_len) - 1) - gamma;
    println!("{} * {} = {}", gamma, epsilon, gamma * epsilon);
}