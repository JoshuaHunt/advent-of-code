use std::fs::File;
use std::io::{BufRead, BufReader};
use std::collections::HashSet;

fn main() {
    let file = File::open("./day1.txt").unwrap();
    let reader = BufReader::new(file);
    let xs: Vec<i32> = 
        reader.lines()
            .map(|l| l.unwrap().parse().unwrap())
            .collect();
    
    for i in 0..xs.len() {
        for j in (i+1)..xs.len() {
            if (xs[i] + xs[j] >= 2020) {
                continue;
            }
            for k in (j+1)..xs.len() {
                if xs[i] + xs[j] + xs[k] == 2020 {
                    println!("{} * {} * {} = {}", xs[i], xs[j], xs[k], xs[i] * xs[j] * xs[k]);
                    return;
                }
            }
        }
    }
}