use std::fs::File;
use std::io::{BufRead, BufReader};
use std::collections::HashSet;

fn main() {
    let file = File::open("./day1.txt").unwrap();
    let reader = BufReader::new(file);
    let mut complements = HashSet::<i32>::new();

    for line in reader.lines() {
        let value: i32 = line.unwrap().parse().unwrap();
        if complements.contains(&value) {
            println!("{} * {} = {}", value, 2020 - value, (2020 - value) * value);
            break;
        }
        complements.insert(2020 - value);
    }
}