package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func max(x, y int) int {
	if x > y {
		return x
	} else {
		return y
	}
}

func sign(x int) int {
	if x > 0 {
		return 1
	} else if x < 0 {
		return -1
	} else {
		return 0
	}
}

type point struct {
	x, y int
}

func interpolate(a, b point) []point {
	points := []point{a}
	dx := sign(b.x - a.x)
	dy := sign(b.y - a.y)
	for a != b {
		a.x += dx
		a.y += dy
		points = append(points, a)
	}
	return append(points, b)
}

type wall struct {
	corners []point
}

func (this *wall) maxY() int {
	maxY := 0
	for _, p := range this.points() {
		maxY = max(maxY, p.y)
	}
	return maxY
}

func (this *wall) points() []point {
	points := make([]point, 0)
	for i := 1; i < len(this.corners); i++ {
		points = append(points, interpolate(this.corners[i-1], this.corners[i])...)
	}
	return points
}

type world struct {
	walls   []wall
	blocked map[point]bool
	maxY    int
}

func newWorld(walls []wall) world {
	maxY := 0
	blocked := make(map[point]bool)
	for _, wall := range walls {
		maxY = max(maxY, wall.maxY())
		for _, p := range wall.points() {
			blocked[p] = true
		}
	}
	return world{walls, blocked, maxY}
}

func (w *world) isBlocked(p point) bool {
	// Remove for part 1:
	if p.y >= w.maxY+2 {
		return true
	}
	return w.blocked[p]
}

func (w *world) addSand(source point) bool {
	if w.isBlocked(source) {
		return false
	}
	// Part 1:
	// for source.y <= w.maxY {
	for {
		down := point{source.x, source.y + 1}
		if !w.isBlocked(down) {
			source = down
			continue
		}

		downLeft := point{source.x - 1, source.y + 1}
		if !w.isBlocked(downLeft) {
			source = downLeft
			continue
		}

		downRight := point{source.x + 1, source.y + 1}
		if !w.isBlocked(downRight) {
			source = downRight
			continue
		}

		// we came to rest
		w.blocked[source] = true
		return true
	}
	// Part 1: we fell off the world
	// return false
}

func parseWall(line string) wall {
	pairs := strings.Split(line, " -> ")
	corners := make([]point, 0)
	for _, pair := range pairs {
		numbers := strings.Split(pair, ",")
		x, err := strconv.Atoi(numbers[0])
		if err != nil {
			log.Fatal(err)
		}
		y, err := strconv.Atoi(numbers[1])
		corners = append(corners, point{x, y})
	}
	return wall{corners}
}

func parseWalls(filename string) []wall {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	walls := make([]wall, 0)
	for scanner.Scan() {
		walls = append(walls, parseWall(scanner.Text()))
	}
	return walls
}

func main() {
	world := newWorld(parseWalls("input1.txt"))
	source := point{500, 0}
	count := 0
	for {
		ok := world.addSand(source)
		if ok {
			count++
		} else {
			break
		}
	}
	fmt.Println(count)
}
