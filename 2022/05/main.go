package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type stack []byte

func (s stack) Peek() byte {
	return s[len(s)-1]
}

func (s stack) Push(v byte) stack {
	return append(s, v)
}

func (s stack) Pop() (stack, byte, error) {
	l := len(s)
	if l == 0 {
		return s, 0, errors.New("Empty stack")
	}
	return s[:l-1], s[l-1], nil
}

type move struct {
	count  int
	source int
	dest   int
}

func (m move) apply(stacks []stack) ([]stack, error) {
	for i := 0; i < m.count; i++ {
		var crate byte
		var err error
		stacks[m.source], crate, err = stacks[m.source].Pop()
		if err != nil {
			return []stack{}, err
		}
		stacks[m.dest] = stacks[m.dest].Push(crate)
	}
	return stacks, nil
}

func (m move) applySameOrder(stacks []stack) ([]stack, error) {
	sourceStart := len(stacks[m.source]) - m.count

	for i := 0; i < m.count; i++ {
		crate := stacks[m.source][sourceStart+i]
		stacks[m.dest] = stacks[m.dest].Push(crate)
	}
	var err error
	for i := 0; i < m.count; i++ {
		stacks[m.source], _, err = stacks[m.source].Pop()
		if err != nil {
			return stacks, err
		}
	}
	return stacks, nil
}

func getTops(stacks []stack) string {
	tops := make([]byte, len(stacks))
	for i := 0; i < len(stacks); i++ {
		tops[i] = stacks[i].Peek()
	}
	return string(tops)
}

func main() {
	filename := "input1.txt"
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	//skip header
	for i := 0; i < 10; i++ {
		scanner.Scan()
	}

	stacks := buildStacks()

	for scanner.Scan() {
		move, err := parseMove(scanner.Text())
		if err != nil {
			log.Fatal(err)
		}
		stacks, err = move.applySameOrder(stacks)
		if err != nil {
			log.Fatal(err)
		}
	}
	fmt.Println(getTops(stacks))
}

func parseMove(line string) (move, error) {
	tokens := strings.Split(line, " ")
	count, err := strconv.Atoi(tokens[1])
	if err != nil {
		return move{}, err
	}
	source, err := strconv.Atoi(tokens[3])
	if err != nil {
		return move{}, err
	}
	dest, err := strconv.Atoi(tokens[5])
	if err != nil {
		return move{}, err
	}
	return move{
		count, source - 1, dest - 1,
	}, nil
}

func buildStacks() []stack {
	stacks := [9]stack{
		[]byte{'F', 'C', 'P', 'G', 'Q', 'R'},
		[]byte{'W', 'T', 'C', 'P'},
		[]byte{'B', 'H', 'P', 'M', 'C'},
		[]byte{'L', 'T', 'Q', 'S', 'M', 'P', 'R'},
		[]byte{'P', 'H', 'J', 'Z', 'V', 'G', 'N'},
		[]byte{'D', 'P', 'J'},
		[]byte{'L', 'G', 'P', 'Z', 'F', 'J', 'T', 'R'},
		[]byte{'N', 'L', 'H', 'C', 'F', 'P', 'T', 'J'},
		[]byte{'G', 'V', 'Z', 'Q', 'H', 'T', 'C', 'W'}}
	return stacks[:]
}
