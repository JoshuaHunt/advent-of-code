(load "~/scripts/lib.lisp")
(ql:quickload :str)

(defun transpose (rows)
  "Converts a list of strings (representing rows) to a list of strings containing the columns from bottom to top"
  (let* ((width (length (first rows)))
	 (height (length rows)))
    (loop for x from 0 to (1- width)
	  collecting (loop for y from (1- height) downto 0
			   collecting (char (nth y rows) x)))))

(defun take-every (n xs)
  "Returns the 0th, nth, 2nth, ... elements of the list xs as a list."
  (if (null xs)
      nil
      (cons (first xs) (take-every n (nthcdr n xs)))))

(defun partition (split-p xs)
  "Returns a list of lists containing the sublists of xs for which split-p returns false. That is, elements where split-p return true indicate where to break xs into sublists and are not included in the output."
  (let ((output '())
	(current '()))
    (loop for x in xs
	  do (if (funcall split-p x)
		 (progn
		   (push (nreverse current) output)
		   (setq current '()))
		 (push x current)))
    (when (not (null current))
      (push (nreverse current) output))
    (nreverse output)))

(defun take-while (keep-p xs)
  "Returns all elements from xs up to but not including the first element for which keep-p evaluates to nil."
  (loop for x in xs
	while (funcall keep-p x)
	collecting x))
		   
(defun parse-stacks (lines)
  (let* ((cols (transpose lines))
	 (stacks-with-blanks
	   (mapcar #'rest (take-every 4 (rest cols))))
	 (stacks-without-blanks
	   (mapcar #'(lambda (stack)
		       (take-while #'(lambda (x) (not (equal x #\Space)))
				   stack))
		   stacks-with-blanks)))
    ; Ensure that push/pop apply to the top of the stack
    (mapcar #'nreverse stacks-without-blanks)))

(defun parse-instruction (line)
  "Parses an instruction of the form 'move X from Y to Z', returning the list of integers (X Y Z), converted to 0-indexing."
  (let ((words (str:words line)))
    `(,(parse-integer (nth 1 words))
      ,(1- (parse-integer (nth 3 words)))
      ,(1- (parse-integer (nth 5 words))))))

(defun parse-instructions (lines)
  (mapcar #'parse-instruction lines))

(defun apply-instruction-old (instr stacks)
  (destructuring-bind (count src dest) instr
    (loop for i from 1 to count
	  do (let ((b (pop (nth src stacks))))
	       (push b (nth dest stacks))))
    stacks))

(defun apply-instruction-new (instr stacks)
  (destructuring-bind (count src dest) instr
    (let ((crates-to-move (copy-list (subseq (nth src stacks) 0 count))))
      (loop for crate in (nreverse crates-to-move)
	    do (progn
		 (pop (nth src stacks))
		 (push crate (nth dest stacks)))))))

(defun stack-tops (stacks)
  (mapcar #'first stacks))

(let* ((lines (read-lines "input1.txt"))
       (blocks (partition #'(lambda (line) (string= "" line)) lines))
       (stacks (parse-stacks (first blocks)))
       (instructions (parse-instructions (second blocks))))
  (format t "~a~%"
	  (loop
	    with current = (copy-tree stacks)
	    for instruction in instructions
	    do (apply-instruction-old instruction current)
	    finally (return (stack-tops current))))
  (format t "~a~%"
	  (loop
	    with current = (copy-tree stacks)
	    for instruction in instructions
	    do (apply-instruction-new instruction current)
	    finally (return (stack-tops current)))))
