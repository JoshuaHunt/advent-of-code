import Data.Char (isSpace, isNumber)
import Data.List (elemIndex)
import Data.Maybe (isJust, fromJust)
import qualified Data.Map as Map

main = do
    contents <- getContents
    print . (moveCrates moveCratesSerially) . lines $ contents
    print . (moveCrates moveCratesParallel) . lines $ contents

moveCrates :: (Crates -> Move -> Crates) -> [String] -> String
moveCrates move lines = stackTops (foldl move initialCrates moves)
                        where (firstHalf, secondHalf) = splitBlank lines
                              initialCrates = parseCrates firstHalf
                              moves = parseMoves secondHalf

data Crates = Crates (Map.Map Int String)
              deriving (Show)
-- count src dest
data Move = Move Int Int Int

stackTops :: Crates -> String
stackTops (Crates stacks) =
    let heads = Map.map head stacks
        headsList = map (\i -> Map.lookup i heads) [1..]
        finiteHeadsList = takeWhile isJust headsList
    in map fromJust finiteHeadsList

moveCratesSerially :: Crates -> Move -> Crates
moveCratesSerially crates (Move 0 _ _) = crates
moveCratesSerially crates (Move count src dest) =
    moveCratesSerially (moveCrate crates src dest) (Move (count-1) src dest)

moveCratesParallel :: Crates -> Move -> Crates
moveCratesParallel (Crates stacks) (Move count src dest) =
    let Just xs = Map.lookup src stacks
        Just ys = Map.lookup dest stacks
        stacks' = Map.insert src (drop count xs) stacks
        stacks'' = Map.insert dest ((take count xs) ++ ys) stacks'
    in Crates stacks''

moveCrate :: Crates -> Int -> Int -> Crates
moveCrate (Crates stacks) src dest =
    let Just (x:xs) = Map.lookup src stacks
        Just ys = Map.lookup dest stacks
        stacks' = Map.insert src xs stacks
        stacks'' = Map.insert dest (x:ys) stacks'
    in Crates stacks''

splitBlank :: [String] -> ([String], [String])
splitBlank xs = (take n xs, drop (n+1) xs)
                where Just n = elemIndex "" xs

parseCrates :: [String] -> Crates
parseCrates lines =
    let header = head $ reverse lines
        cratesSize = length $ filter isNumber header
        rest = reverse . tail $ reverse lines
        getStack = \i -> dropWhile isSpace . map (!! (4*i-3)) $ rest
        stacks = map (\i -> (i, getStack i)) [1..cratesSize]
    in Crates (Map.fromList stacks)

parseMoves = map parseMove

parseMove :: String -> Move
parseMove line =
    let tokens = words line
        count = read (tokens !! 1)
        src = read (tokens !! 3)
        dest = read (tokens !! 5)
    in Move count src dest