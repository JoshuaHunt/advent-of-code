package main

import "fmt"

type deque struct {
	buffer [40]int
	// Next read position
	head int
	// Next write position
	tail int
}

func newDeque(contents ...int) deque {
	var d deque
	for _, c := range contents {
		d.pushLast(c)
	}
	return d
}

func (d *deque) pushLast(c int) {
	d.buffer[d.tail] = c
	d.tail = (d.tail + 1) % len(d.buffer)
}

func (d *deque) popFirst() int {
	item := d.buffer[d.head]
	d.head = (d.head + 1) % len(d.buffer)
	return item
}

func (d *deque) isEmpty() bool {
	return d.head == d.tail
}

type op func(int) int

type monkey struct {
	items       deque
	op          op
	test        int
	true_dest   int
	false_dest  int
	inspections int
}

func loadTestMonkeys() [4]monkey {
	return [4]monkey{
		{
			items:      newDeque(79, 98),
			op:         func(x int) int { return x * 19 },
			test:       23,
			true_dest:  2,
			false_dest: 3,
		},
		{
			items:      newDeque(54, 65, 75, 74),
			op:         func(x int) int { return x + 6 },
			test:       19,
			true_dest:  2,
			false_dest: 0,
		},
		{
			items:      newDeque(79, 60, 97),
			op:         func(x int) int { return x * x },
			test:       13,
			true_dest:  1,
			false_dest: 3,
		},
		{
			items:      newDeque(74),
			op:         func(x int) int { return x + 3 },
			test:       17,
			true_dest:  0,
			false_dest: 1,
		},
	}
}

func mult(m int) op {
	return func(x int) int { return x * m }
}

func add(a int) op {
	return func(x int) int { return x + a }
}

func square() op {
	return func(x int) int { return x * x }
}

func loadMyMonkeys() [8]monkey {
	return [8]monkey{
		{
			items:      newDeque(91, 58, 52, 69, 95, 54),
			op:         mult(13),
			test:       7,
			true_dest:  1,
			false_dest: 5,
		},
		{
			items:      newDeque(80, 80, 97, 84),
			op:         square(),
			test:       3,
			true_dest:  3,
			false_dest: 5,
		},
		{
			items:      newDeque(86, 92, 71),
			op:         add(7),
			test:       2,
			true_dest:  0,
			false_dest: 4,
		},
		{
			items:      newDeque(96, 90, 99, 76, 79, 85, 98, 61),
			op:         add(4),
			test:       11,
			true_dest:  7,
			false_dest: 6,
		},
		{
			items:      newDeque(60, 83, 68, 64, 73),
			op:         mult(19),
			test:       17,
			true_dest:  1,
			false_dest: 0,
		},
		{
			items:      newDeque(96, 52, 52, 94, 76, 51, 57),
			op:         add(3),
			test:       5,
			true_dest:  7,
			false_dest: 3,
		},
		{
			items:      newDeque(75),
			op:         add(5),
			test:       13,
			true_dest:  4,
			false_dest: 2,
		},
		{
			items:      newDeque(83, 75),
			op:         add(1),
			test:       19,
			true_dest:  2,
			false_dest: 6,
		},
	}
}

func main() {
	monkeys := loadMyMonkeys()
	mod := 1
	for _, m := range monkeys {
		mod *= m.test
	}
	for i := 0; i < 10_000; i++ {
		for j := range monkeys {
			monkey := &monkeys[j]
			for !monkey.items.isEmpty() {
				monkey.inspections += 1
				item := monkey.items.popFirst()
				fmt.Printf("%v -> ", item)
				item = monkey.op(item)
				fmt.Printf("%v -> ", item)
				// item /= 3
				item = item % mod
				fmt.Printf("%v -> ", item)
				if item%monkey.test == 0 {
					fmt.Printf("throw to %v\n", monkey.true_dest)
					monkeys[monkey.true_dest].items.pushLast(item)
				} else {
					fmt.Printf("throw to %v\n", monkey.true_dest)
					monkeys[monkey.false_dest].items.pushLast(item)
				}
			}
		}
		fmt.Println(i + 1)
		for j, monkey := range monkeys {
			fmt.Printf("%v: %v\n", j, monkey.items)
		}
		fmt.Println()
	}
	for _, monkey := range monkeys {
		fmt.Println(monkey.inspections)
	}
}
