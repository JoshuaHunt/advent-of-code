package main

import (
	"testing"
)

func TestChase(t *testing.T) {
	type test struct {
		tail     pos
		head     pos
		wantTail pos
	}

	tests := []test{
		{tail: pos{0, 0}, head: pos{2, 0}, wantTail: pos{1, 0}},
		{tail: pos{0, 0}, head: pos{1, 1}, wantTail: pos{0, 0}},
	}

	for i, tc := range tests {
		tc.tail.chase(tc.head)
		if tc.tail != tc.wantTail {
			t.Fatalf("[%v] expected: %v, got: %v", i, tc.wantTail, tc.tail)
		}
	}
}
