(load "~/scripts/lib.lisp")

(defun parse-moves (lines)
  

(let* ((lines (read-lines "input0.txt"))
       (moves (parse-moves lines))
       (configurations (loop for move in moves
			     for configuration = (apply-move move '((const 0 0) (const 0 0)))			     
			       then (apply-move move configuration)
			     collect configuration)))
  (length (remove-duplicates (mapcar #'last configurations))))
