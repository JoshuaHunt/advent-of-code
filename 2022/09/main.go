package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func sign(x int) int {
	if x < 0 {
		return -1
	} else if x > 0 {
		return 1
	} else {
		return 0
	}
}

type pos struct {
	x int
	y int
}

func (p *pos) move(dx, dy int) {
	p.x += dx
	p.y += dy
}

func (p *pos) chase(o pos) {
	dx := o.x - p.x
	dy := o.y - p.y
	if abs(dx)+abs(dy) <= 1 {
		// orthogonally adjacent
		return
	} else if abs(dx) == 1 && abs(dy) == 1 {
		// diagonally adjacent
		return
	} else if abs(dy) == 0 {
		p.x += sign(dx)
	} else if abs(dx) == 0 {
		p.y += sign(dy)
	} else {
		p.x += sign(dx)
		p.y += sign(dy)
	}
}

func getDir(dir string) (dx, dy int, err error) {
	switch dir {
	case "L":
		dx = -1
	case "R":
		dx = 1
	case "U":
		dy = 1
	case "D":
		dy = -1
	default:
		err = errors.New(dir + " did not match any case")
	}
	return
}

func parseMove(line string) (dx, dy, count int, err error) {
	tokens := strings.Split(line, " ")
	count, err = strconv.Atoi(tokens[1])
	if err != nil {
		return 0, 0, 0, err
	}
	dx, dy, err = getDir(tokens[0])
	return
}

const nodeCount = 10

func printPos(nodes [nodeCount]pos) {
	positions := make(map[pos]int)
	for i, node := range nodes {
		_, ok := positions[node]
		if !ok {
			positions[node] = i
		}
	}
	for y := 4; y >= 0; y-- {
		for x := 0; x < 5; x++ {
			node := pos{x, y}
			i, ok := positions[node]
			if ok {
				fmt.Print(i)
			} else {
				fmt.Print(".")
			}
		}
		fmt.Println()
	}
	fmt.Println()
}

func main() {
	file, err := os.Open("input1.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var nodes [nodeCount]pos
	tailPositions := make(map[pos]bool)
	for scanner.Scan() {
		dx, dy, count, err := parseMove(scanner.Text())
		if err != nil {
			log.Fatal(err)
		}
		for i := 0; i < count; i++ {
			nodes[0].move(dx, dy)
			for j := 1; j < nodeCount; j++ {
				nodes[j].chase(nodes[j-1])
			}
			tailPositions[nodes[nodeCount-1]] = true
			printPos(nodes)
		}
	}
	fmt.Println(len(tailPositions))
}
