package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

const width int = 7

type vector struct {
	x, y int
}
type point struct {
	x, y int
}
type grid struct {
	points  [][width]bool
	yOffset int
	maxY    int
}

func (p point) shift(v vector) point {
	p.x += v.x
	p.y += v.y
	return p
}

type shape struct {
	pixels []vector
}
type rock struct {
	shape *shape
	pos   point
}

func rowsToDelete(grid grid) int {
	for y := 0; y < len(grid.points); y++ {
		for _, blocked := range grid.points[y] {
			if !blocked {
				return y - 1
			}
		}
	}
	return len(grid.points) - 1
}

var shapes []shape = []shape{
	{[]vector{{0, 0}, {1, 0}, {2, 0}, {3, 0}}},
	{[]vector{{1, 0}, {0, 1}, {1, 1}, {2, 1}, {1, 2}}},
	{[]vector{{0, 0}, {1, 0}, {2, 0}, {2, 1}, {2, 2}}},
	{[]vector{{0, 0}, {0, 1}, {0, 2}, {0, 3}}},
	{[]vector{{0, 0}, {0, 1}, {1, 0}, {1, 1}}},
}

func (g *grid) newRock(id int) rock {
	minY := rowsToDelete(*g)
	g.yOffset += minY
	g.maxY -= minY
	g.points = g.points[minY:]
	for len(g.points) < g.maxY+8 {
		var row [width]bool
		g.points = append(g.points, row)
	}
	return rock{&shapes[id%len(shapes)], point{2, g.maxY + 4}}
}

type wind struct {
	pattern string
	i       int
}

func windDir(c byte) int {
	if c == '<' {
		return -1
	} else {
		return 1
	}
}

func (w *wind) blow(rock rock, grid [][width]bool) rock {
	c := w.pattern[w.i]
	rock.pos.x += windDir(c)
	if isBlocked(rock, grid) {
		rock.pos.x -= windDir(c)
	}
	w.i = (w.i + 1) % len(w.pattern)
	return rock
}

func isBlocked(rock rock, grid [][width]bool) bool {
	for _, dp := range rock.shape.pixels {
		p := rock.pos.shift(dp)
		if p.x < 0 || p.x >= width || p.y < 0 {
			return true
		}
		if grid[p.y][p.x] {
			return true
		}
	}
	return false
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func (g *grid) save(rock rock) {
	for _, dp := range rock.shape.pixels {
		p := rock.pos.shift(dp)
		g.points[p.y][p.x] = true
		g.maxY = max(g.maxY, p.y)
	}
}

func fall(rock rock, grid *grid, wind *wind) {
	for {
		rock = wind.blow(rock, grid.points)
		rock.pos.y -= 1
		if isBlocked(rock, grid.points) {
			rock.pos.y += 1
			grid.save(rock)
			return
		}
	}
}

func readWind(filename string) wind {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	scanner.Scan()
	return wind{scanner.Text(), 0}
}

func eq(a, b [7]bool) bool {
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func display(g *grid) {
	l := len(g.points)
	for r := range g.points {
		for _, x := range g.points[l-1-r] {
			if x {
				fmt.Print("#")
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
}

func findPeriod(g *grid) int {
	l := g.maxY
	for period := 20; period < l/2; period++ {
		isPeriodic := true
		for i := 0; i < period; i++ {
			if g.points[l-i] != g.points[l-i-period] {
				isPeriodic = false
				break
			}
		}
		if isPeriodic {
			// display(g)
			return period
		}
	}
	return -1
}

func getHeight(rocks int, fast bool) int {
	points := [][width]bool{{true, true, true, true, true, true, true}}
	grid := grid{points, 0, 0}
	wind := readWind("input0.txt")
	heights := make([]int, 0)
	states := make([]string, 0)
	for i := 0; i < rocks; i++ {
		fmt.Print(i, "\r")
		rock := grid.newRock(i)
		fall(rock, &grid, &wind)
		period := findPeriod(&grid)
		heights = append(heights, grid.maxY+grid.yOffset)
		state := fmt.Sprintf("%v,%v", i%len(shapes), wind.i%len(wind.pattern))
		states = append(states, state)
		if fast && period > 0 && (rocks-i) > period && state == states[i-period] {
			fmt.Println("\nPeriod is", period)
			steps := (rocks - i) / period
			grid.yOffset += steps * (heights[i] - heights[i-period])
			i += steps * period
		}
	}
	return grid.maxY + grid.yOffset
}

func main() {
	rocks := 1_000_000_000_000
	fmt.Println(getHeight(rocks, true))
	// for i := 4999; i < 1_000_000_000_000; i++ {
	// 	slowHeight := getHeight(i, false)
	// 	fastHeight := getHeight(i, true)
	// 	if slowHeight != fastHeight {
	// 		fmt.Println("Breaks at", i, "rocks")
	// 		return
	// 	}
	// }
}

//1514285714288
