(load "~/scripts/lib.lisp")

(defun shape-score (shape)
  (case shape
    (:R 1)
    (:P 2)
    (:S 3)))

(defun i-win (them me)
    (member `(,them ,me) '((:R :P) (:P :S) (:S :R)) :test #'equal))

(defun match-score (them me)
  (cond
    ((equal them me) 3)
    ((i-win them me) 6)
    (t 0)))

(defun to-shape (char)
  (case char
    (#\A :R)
    (#\B :P)
    (#\C :S)
    (#\X :R)
    (#\Y :P)
    (#\Z :S)))

(defun to-shapes (match)
  `(,(to-shape (char match 0)) ,(to-shape (char match 2))))

(defun get-score (moves)
  (destructuring-bind (them me) moves
    (+ (shape-score me) (match-score them me))))

(defun to-result (char)
  (case char
    (#\X :lose)
    (#\Y :draw)
    (#\Z :win)))

(defun from-result (result them)
  (case result
    (:draw them)
    (:win (find-if #'(lambda (me) (i-win them me)) '(:R :P :S)))
    (:lose (find-if #'(lambda (me) (i-win me them)) '(:R :P :S)))))


(defun to-shapes-2 (match)
  (let* ((them (to-shape (char match 0)))
	 (result (to-result (char match 2)))
	 (me (from-result result them)))
    `(,them ,me)))
     
(format t "~a~%" (reduce #'+ (map 'list #'get-score (map 'list #'to-shapes (read-lines "input1.txt")))))
(fn-stream (read-lines "input1.txt")
	   (mapcar #'to-shapes-2)
	   (mapcar #'get-score)
	   (reduce #'+)
	   (format t "~a~%"))
