package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func getMoves(scanner *bufio.Scanner) (bool, byte, byte) {

	fileContinues := scanner.Scan()
	if !fileContinues {
		return true, 0, 0
	}
	theirMove := scanner.Text()[0]
	scanner.Scan()
	myMove := scanner.Text()[0]
	return false, theirMove, myMove
}

func getMovePoints(myMove byte) int {
	return 1 + (int)(myMove-'A')
}

func getWinPoints(theirMove, myMove byte) int {
	if myMove == theirMove {
		return 3
	}
	if myMove == 'A' && theirMove == 'C' {
		return 6
	} else if theirMove == 'A' && myMove == 'C' {
		return 0
	} else if myMove > theirMove {
		return 6
	}
	return 0
}

func getPoints(theirMove, myMove byte) int {
	return getMovePoints(myMove) + getWinPoints(theirMove, myMove)
}

func getMyMove(theirMove, targetResult byte) byte {
	myMove := theirMove + (targetResult - 'Y')
	if myMove > 'C' {
		return myMove - 3
	} else if myMove < 'A' {
		return myMove + 3
	}
	return myMove
}

func main() {
	filename := "input1.txt"
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)

	points := 0
	for {
		fileEnded, theirMove, targetResult := getMoves(scanner)
		if fileEnded {
			break
		}
		myMove := getMyMove(theirMove, targetResult)
		points += getPoints(theirMove, myMove)
	}
	fmt.Println(points)
}
