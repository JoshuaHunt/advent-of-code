package main

import (
	"testing"
)

func TestSplit(t *testing.T) {
	type test struct {
		theirMove byte
		myMove    byte
		wantScore int
	}

	tests := []test{
		{theirMove: 'A', myMove: 'A', wantScore: 3},
		{theirMove: 'A', myMove: 'B', wantScore: 6},
		{theirMove: 'A', myMove: 'C', wantScore: 0},
		{theirMove: 'B', myMove: 'A', wantScore: 0},
		{theirMove: 'B', myMove: 'C', wantScore: 6},
	}

	for i, tc := range tests {
		gotScore := getWinPoints(tc.theirMove, tc.myMove)
		if tc.wantScore != gotScore {
			t.Fatalf("[%v] expected: %v, got: %v, test: %v", i, tc.wantScore, gotScore, tc)
		}
	}
}
