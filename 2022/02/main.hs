main = do
    contents <- getContents
    print . solvePart1 . lines $ contents
    print . solvePart2 . lines $ contents

data Move = Rock | Paper | Scissors
            deriving (Eq)

solvePart1 :: [String] -> Int
solvePart1 = sum . map score . map parseMoves1

parseMoves1 :: String -> (Move, Move)
parseMoves1 descr = (moves !! 0, moves !! 1)
                   where moves = map toMove . words $ descr


toMove :: String -> Move
toMove "A" = Rock
toMove "B" = Paper
toMove "C" = Scissors
toMove "X" = Rock
toMove "Y" = Paper
toMove "Z" = Scissors

solvePart2 :: [String] -> Int
solvePart2 = sum . map score . map parseMoves2

parseMoves2 :: String -> (Move, Move)
parseMoves2 descr = let tokens = words descr
                        elfMove = toMove . head $ tokens
                        myMove = moveForResult (toResult (tokens !! 1)) elfMove
                    in (elfMove, myMove)

data Result = Win | Lose | Draw
              deriving (Eq)

toResult :: String -> Result
toResult x 
         | x == "X" = Lose
         | x == "Y" = Draw
         | x == "Z" = Win

moveForResult :: Result -> Move -> Move
moveForResult result elfMove = head [myMove | myMove <- [Rock, Paper, Scissors], matchResult elfMove myMove == result]

matchResult :: Move -> Move -> Result
matchResult elf me
            | elf == Paper && me == Rock = Lose
            | elf == Rock && me == Scissors = Lose
            | elf == Scissors && me == Paper = Lose
            | elf == me = Draw
            | otherwise = Win

matchScore :: Result -> Int
matchScore result
           | result == Lose = 0
           | result == Draw = 3
           | result == Win = 6

score :: (Move, Move) -> Int
score (elf, me) = shapeScore me + matchScore (matchResult elf me)
    where shapeScore me
                     | me == Rock = 1
                     | me == Paper = 2
                     | me == Scissors = 3
