from math import ceil

def get_max_geodes(blueprint, t_left, resources, robots):
	print(t_left, resources, robots)
	machine_built = False
	max_geodes = -1
	for resource in range(3, -1, -1):
		cost = get_cost(blueprint, resource)
		dt = time_to_get(cost, robots, resources)
		if dt is None or dt > t_left:
			continue
		dt += 1 # for building the machine
		machine_built = True
		resources = add_resources(resources, cost, -1)
		resources = add_resources(resources, robots, dt)
		robots[resource] += 1
		max_geodes = max(max_geodes, get_max_geodes(blueprint, t_left - dt, resources, robots))
		robots[resource] -= 1
		resources = add_resources(resources, robots, -dt)
		resources = add_resources(resources, cost, 1)

	if machine_built:
		return max_geodes
	else:
		return resources[3] + t_left * robots[3]

def get_cost(blueprint, resource):
	if resource == 0: # ore
		return [blueprint[1], 0, 0, 0]
	elif resource == 1: # clay
		return [blueprint[2], 0, 0, 0]
	elif resource == 2: # obsidian
		return [blueprint[3], blueprint[4], 0, 0]
	elif resource == 3: # geode
		return [blueprint[5], 0, blueprint[6], 0]

def time_to_get(cost, robots, starting_resources):
	impossible = [i for i in range(4) if cost[i] > starting_resources[i] and robots[i] == 0]
	if len(impossible) > 0:
		return None
	dt = [ceil((c - r) / dr) for c, r, dr in zip(cost, starting_resources, robots) if c > r]
	if not dt:
		return 0
	return max(dt)

def add_resources(resources, robots, dt):
	return [(r+dr*dt) for r, dr in zip(resources, robots)]

blueprints = []
with open("input1.txt") as f:
	for line in f:
		token_positions = [1,6,12,18,21,27,30]
		blueprints.append([int(line.split()[i].strip(":.")) for i in token_positions])
print(get_max_geodes(blueprints[0], 24, [0, 0, 0, 0], [1, 0, 0, 0]))
