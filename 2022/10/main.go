package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type instr struct {
	v int
	// Executes after the t^th cycle
	t int
}

type instrQueue struct {
	buffer [2]instr
	// Index of next read location (front of queue)
	head int
	// Index of next write location (one after end of queue)
	tail int
}

func (this *instrQueue) Dequeue() instr {
	result := this.buffer[this.head]
	this.head = (this.head + 1) % len(this.buffer)
	return result
}

func (this *instrQueue) Enqueue(v instr) {
	this.buffer[this.tail] = v
	this.tail = (this.tail + 1) % len(this.buffer)
}

func (this *instrQueue) Peek() instr {
	return this.buffer[this.head]
}

func (this *instrQueue) IsEmpty() bool {
	return this.head == this.tail
}

func parseInstr(line string, t int) instr {
	tokens := strings.Split(line, " ")
	if tokens[0] == "noop" {
		return instr{0, t}
	} else {
		v, err := strconv.Atoi(tokens[1])
		if err != nil {
			log.Fatal(err)
		}
		return instr{v, t + 1}
	}
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func main() {
	file, err := os.Open("input1.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	var queue instrQueue

	x := 1
	s := 0
	var buffer [240]byte
	var instr instr
	for t := 1; t <= 240; t++ {
		if queue.IsEmpty() {
			if !scanner.Scan() {
				log.Fatal("Out of lines")
			}
			instr = parseInstr(scanner.Text(), t)
			queue.Enqueue(instr)
		}
		if abs((t-1)%40-(x%40)) <= 1 {
			buffer[t-1] = '#'
		} else {
			buffer[t-1] = '.'
		}
		// fmt.Printf("Cycle %v, CRT at %v, sprite at %v\n", t, t-1, x)
		// fmt.Println(string(buffer[0:40]))
		// fmt.Println(string(buffer[40:80]))
		if queue.Peek().t == t {
			instr = queue.Dequeue()
			x += instr.v
		}
	}
	for i := 0; i < 240; i += 40 {
		fmt.Println(string(buffer[i : i+40]))
	}
	fmt.Println(s)
}
