package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

type node struct {
	name     string
	parent   *node
	size     int
	children map[string]*node
}

func emptyNode(name string, parent *node) *node {
	return &node{name, parent, 0, make(map[string]*node)}
}

func main() {
	root := buildTree("input1.txt")
	size := sumSmallFiles(root, 100000)
	log.Println(size)

	minSize := root.size - 40000000
	smallest := findSmallestDirAbove(root, minSize)
	log.Println(smallest)
}

func printTree(root *node, depth int) {
	for i := 0; i < depth; i++ {
		fmt.Print("-")
	}
	fmt.Printf(" %v %v\n", root.name, root.size)
	for _, child := range root.children {
		printTree(child, depth+1)
	}
}

func buildTree(filename string) *node {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	current := emptyNode("/", nil)
	for scanner.Scan() {
		line := scanner.Text()
		if isCdCommand(line) {
			dir := getCdDir(line)
			if dir == "/" {
				current = cdRoot(current)
			} else if dir == ".." {
				current = cdParent(current)
			} else {
				current = cdChild(current, dir)
			}
		} else if isDir(line) {
			name := parseDirName(line)
			current.addDir(name)
		} else if isFile(line) {
			size, name, err := parseFile(line)
			if err != nil {
				log.Fatal(err)
			}
			current.addFile(name, size)
		}
	}
	return cdRoot(current)
}

func isCdCommand(line string) bool {
	return strings.HasPrefix(line, "$ cd")
}

func getCdDir(line string) string {
	return strings.Split(line, " ")[2]
}

func isFile(line string) bool {
	if line[0] == '$' {
		return false
	}
	tokens := strings.Split(line, " ")
	if tokens[0] == "dir" {
		return false
	}
	return true
}

func isDir(line string) bool {
	return strings.HasPrefix(line, "dir ")
}

func parseFile(line string) (size int, name string, err error) {
	tokens := strings.Split(line, " ")
	size, err = strconv.Atoi(tokens[0])
	name = tokens[1]
	return
}

func parseDirName(line string) string {
	tokens := strings.Split(line, " ")
	return tokens[1]
}

func cdRoot(current *node) *node {
	for current.parent != nil {
		current = current.parent
	}
	return current
}

func cdParent(current *node) *node {
	return current.parent
}

func cdChild(current *node, dir string) *node {
	_, present := current.children[dir]
	if !present {
		log.Fatal("Missing child " + dir)
	}
	return current.children[dir]
}

func (current *node) addFile(name string, size int) {
	current.size += size
	for current.parent != nil {
		current = current.parent
		current.size += size
	}
}

func (current *node) addDir(name string) {
	_, present := current.children[name]
	if present {
		log.Fatal("Revisiting child " + name)
	}
	newChild := emptyNode(name, current)
	current.children[name] = newChild
}

func sumSmallFiles(root *node, maxSize int) int {
	c := Walker(root)
	sum := 0
	for {
		node, ok := <-c
		if !ok {
			break
		}
		if node.size <= maxSize {
			sum += node.size
		}
	}
	return sum
}

func min(x, y int) int {
	if x < y {
		return x
	} else {
		return y
	}
}

func findSmallestDirAbove(root *node, lowerBound int) int {
	c := Walker(root)
	minSize := math.MaxInt
	for {
		node, ok := <-c
		if !ok {
			return minSize
		}
		fmt.Printf("Got node %v\n", node)
		if node.size >= lowerBound {
			fmt.Println("Reducing")
			minSize = min(minSize, node.size)
		}
		fmt.Println(minSize)
	}
}

func Walker(root *node) <-chan *node {
	ch := make(chan *node)
	go func() {
		Walk(root, ch)
		close(ch)
	}()
	return ch
}

func Walk(root *node, ch chan *node) {
	ch <- root
	for _, child := range root.children {
		Walk(child, ch)
	}
}
