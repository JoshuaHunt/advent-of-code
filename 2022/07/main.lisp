(load "~/scripts/lib.lisp")
(ql:quickload :str)

(defun group-outputs (lines)
  (loop with result = '()
	with current = '()
        for line in lines
	do (if (str:starts-with-p "$ " line)
	       (progn
		 (push (nreverse current) result)
		 (setq current (list line)))
	       (push line current))
        finally (progn
		  (push (nreverse current) result)
		  ; We push a NIL on when we see the first line, so need to remove it with `rest`
		  (return (rest (nreverse result))))))

(defun find-subdir (dirname pwd)
  (let ((contents (getf pwd :contents)))
    (find-if #'(lambda (d) (equal (getf d :dir) dirname)) contents)))

(defun cd (dirname pwd)
  (when (not (find-subdir dirname pwd))
    (push `(:dir ,dirname :contents ()) (getf pwd :contents)))  
  (find-subdir dirname pwd))

(defun build-dir (command-output)
  (if (str:starts-with-p "dir " command-output)
      (list :dir (second (str:words command-output)) :contents '())
      nil))

(defun build-file (command-output)
  (if (not (str:starts-with-p "dir " command-output))
      (let ((filename (second (str:words command-output)))
	    (size (parse-integer (first (str:words command-output)))))
	(list :file filename :size size))
      nil))

(defun add-file-or-dir (result pwd)
  (let ((dir (build-dir result))
	(file (build-file result)))
      (if file
	(push file (getf pwd :contents))
	(push dir (getf pwd :contents)))))  

(defun ls (results pwd)
  (loop for result in results
	do (add-file-or-dir result pwd)))

(defun build-tree (outputs)
  "Each entry of outputs is a list whose 0th element is the command run and whose remaining elements
   are the lines of output returned by the command. The return value of build-tree is a tree represented
   as nested lists: each element is a list of the form
      (:file \"filename\" :size 123)
   or
      (:dir \"dirname\" :contents (...))"
  (loop with pwds = (copy-list '())
	with root = (copy-list '(:dir "/" :contents ()))
	for output in outputs
	do (let ((cmd (first output))
		 (result (rest output)))
	     (cond ((equal "$ cd /" cmd)
		    (setq pwds (list root)))
		   ((equal "$ cd .." cmd)
		    (pop pwds))
		   ((str:starts-with-p "$ cd " cmd)
		    (let ((dirname (third (str:words cmd))))
		      (push (cd dirname (first pwds)) pwds)))
		   ((equal "$ ls" cmd)
		    (ls result (first pwds)))))
	finally (return root)))

(defun directory-sizes (tree)
  (let ((result '()))
    (labels ((compute-size (x)
	     (let ((size (if (getf x :file)
			     (getf x :size)
			     (loop for y in (getf x :contents)
   				   sum (compute-size y)))))
	       (when (getf x :dir)
		 (push size result))
	       size)))
      (compute-size tree))
    result))
			    
(let* ((sizes (fn-stream (read-lines "input1.txt")
 			 (group-outputs)
			 (build-tree)
			 (directory-sizes)
			 (funcall (lambda (xs) (sort xs '>)))))
       (used (first sizes))
       (total 70000000)
       (required 30000000))
  (print (fn-stream sizes
		    (remove-if-not #'(lambda (x) (<= x 100000)))
		    (apply '+)))
  (print (fn-stream sizes
		    (remove-if-not #'(lambda (x) (<= required (- total (- used x)))))
		    (funcall (lambda (xs) (sort xs '<)))
		    (first))))	 
  
	   


