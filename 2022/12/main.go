package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
)

type pos struct {
	x, y int
	h    int
}

func parseMap(filename string) (start, end pos, grid [][]pos) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	grid = make([][]pos, 0)
	y := 0
	for scanner.Scan() {
		grid = append(grid, make([]pos, 0))
		x := 0
		for _, c := range scanner.Bytes() {
			node := pos{x, y, int(c - 'a')}
			if c == 'S' {
				node.h = 0
				// Reversed this for part 2:
				end = node
			} else if c == 'E' {
				node.h = int('z' - 'a')
				// Reversed this for part 2:
				start = node
			}
			grid[y] = append(grid[y], node)
			x++
		}
		y++
	}
	return
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func dist(a, b pos) int {
	return abs(a.x-b.x) + abs(a.y-b.y)
}

type node struct {
	p pos
	// Dist from start
	d int
}

type queue []node

func getAdjacent(grid [][]pos, c pos) []pos {
	next := make([]pos, 0, 4)
	dxs := [4]int{-1, 0, 1, 0}
	dys := [4]int{0, -1, 0, 1}
	for i := 0; i < 4; i++ {
		x := c.x + dxs[i]
		y := c.y + dys[i]
		if y < 0 || y >= len(grid) {
			continue
		}
		if x < 0 || x >= len(grid[y]) {
			continue
		}
		// Reversed this for part 2:
		if c.h > grid[y][x].h+1 {
			continue
		}
		next = append(next, grid[y][x])
	}
	return next
}

func main() {
	start, _, grid := parseMap("input1.txt")
	queue := make([]node, 1)
	queue[0] = node{start, 0}
	visited := make(map[pos]int)

	for len(queue) > 0 {
		sort.Slice(queue, func(i, j int) bool { return queue[i].d < queue[j].d })
		current := queue[0]
		queue = queue[1:]
		log.Printf("Visit %v\n", current)

		if _, ok := visited[current.p]; ok {
			// already visited this pos
			continue
		}

		visited[current.p] = current.d
		// Changed this for part 2:
		// if current.p == end {
		if current.p.h == 0 {
			fmt.Println(current.d)
			break
		}

		for _, newPos := range getAdjacent(grid, current.p) {
			if _, ok := visited[newPos]; ok {
				// already visited
				continue
			}
			queue = append(queue, node{newPos, current.d + 1})
		}
	}
}
