package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type tree struct {
	row       int
	col       int
	height    int
	leftDist  int
	rightDist int
	upDist    int
	downDist  int
}

func newTree(row, col, height int) tree {
	return tree{row, col, height, -1, -1, -1, -1}
}

func (t tree) seenDist() int {
	if t.leftDist < 0 || t.rightDist < 0 || t.downDist < 0 || t.upDist < 0 {
		log.Fatal(t)
	}
	return t.leftDist * t.rightDist * t.downDist * t.upDist
}

func buildGrid(filename string) [][]tree {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	grid := make([][]tree, 1)
	scanner.Scan()
	for c, h := range scanner.Bytes() {
		grid[0] = append(grid[0], newTree(0, c, int(h-'0')))
	}
	rowWidth := len(grid[0])
	r := 1
	for scanner.Scan() {
		grid = append(grid, make([]tree, rowWidth))
		for c, h := range scanner.Bytes() {
			grid[r][c] = newTree(r, c, int(h-'0'))
		}
		r++
	}
	return grid
}

// List of tree in decreasing order of height
type treeLine []*tree

func (s treeLine) Peek() *tree {
	return s[len(s)-1]
}

func (s treeLine) Push(v *tree) treeLine {
	return append(s, v)
}

func (s treeLine) PopShorter(v *tree) (treeLine, *tree) {
	if len(s) == 0 {
		return s, nil
	}
	if s.Peek().height >= v.height {
		return s, nil
	}
	return s.Pop()
}

func (s treeLine) Pop() (treeLine, *tree) {
	l := len(s)
	return s[:l-1], s[l-1]
}

func main() {
	grid := buildGrid("input1.txt")
	h := len(grid)
	w := len(grid[0])

	rows := make([]treeLine, h)
	cols := make([]treeLine, w)
	var leftTree, upTree *tree

	for r := 0; r < h; r++ {
		for c := 0; c < w; c++ {
			for {
				rows[r], leftTree = rows[r].PopShorter(&grid[r][c])
				if leftTree != nil {
					leftTree.rightDist = c - leftTree.col
				} else {
					// stack is empty or top tree is not shorter
					break
				}
			}
			if len(rows[r]) == 0 {
				// Can see to edge of grid
				grid[r][c].leftDist = c
			} else {
				grid[r][c].leftDist = c - rows[r].Peek().col
				if rows[r].Peek().height == grid[r][c].height {
					// Old tree can't see edge of grid
					rows[r], leftTree = rows[r].Pop()
					leftTree.rightDist = c - leftTree.col
				}
			}
			rows[r] = rows[r].Push(&grid[r][c])

			for {
				cols[c], upTree = cols[c].PopShorter(&grid[r][c])
				if upTree != nil {
					upTree.downDist = r - upTree.row
				} else {
					// stack is empty or top tree is not shorter
					break
				}
			}
			if len(cols[c]) == 0 {
				// Can see to edge of grid
				grid[r][c].upDist = r
			} else {
				grid[r][c].upDist = r - cols[c].Peek().row
				if cols[c].Peek().height == grid[r][c].height {
					// Old tree can't see edge of grid
					cols[c], upTree = cols[c].Pop()
					upTree.downDist = r - upTree.row
				}
			}
			cols[c] = cols[c].Push(&grid[r][c])
		}
	}

	for r := 0; r < h; r++ {
		for len(rows[r]) > 0 {
			rows[r], leftTree = rows[r].Pop()
			// Can see to edge of grid
			leftTree.rightDist = (w - 1) - leftTree.col
		}
	}
	for c := 0; c < w; c++ {
		for len(cols[c]) > 0 {
			cols[c], upTree = cols[c].Pop()
			// Can see to edge of grid
			upTree.downDist = (h - 1) - upTree.row
		}
	}

	var bestTree *tree
	for r := 0; r < h; r++ {
		for c := 0; c < w; c++ {
			fmt.Printf("%v %v\n", grid[r][c], grid[r][c].seenDist())
			seen := grid[r][c].seenDist()
			if bestTree == nil || seen > bestTree.seenDist() {
				bestTree = &grid[r][c]
			}
		}
	}
	fmt.Printf("%v %v\n", bestTree, bestTree.seenDist())
}
