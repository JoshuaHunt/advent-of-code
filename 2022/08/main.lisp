(load "~/scripts/lib.lisp")
(ql:quickload :cl-utilities)

(defun build-row (line)
  (map 'vector (cl-utilities:compose #'parse-integer #'string) line))

(defun build-grid (lines)
  (map 'vector #'build-row lines))

(defun dims (grid)
  (let ((height (length grid))
	(width (length (elt grid 0))))
    (cons width height)))

(defun bounded-sequence (x0 dx min max)
  "Generates the sequence (x0, x0+dx, x0+2*dx, ...) until it reaches the bounds. min is inclusive, max is exclusive."
  (cond ((equal dx 0) `(,x0))
	((< dx 0) (loop for x from x0 downto min by (abs dx) collect x))
	((> dx 0) (loop for x from x0 below max by dx collect x))))

(defun cross-product (xs ys)
  (loop for x in xs
	nconcing (loop for y in ys
		       collecting (cons x y))))

(defun ray (grid pos dpos)
  (destructuring-bind ((x0 . y0) (dx . dy) (width . height)) `(,pos ,dpos ,(dims grid))
    (loop for (x . y) in (cross-product (bounded-sequence x0 dx 0 width)
  	  				(bounded-sequence y0 dy 0 height))
	  collect (elt (elt grid y) x))))
  

(defun visible-in-dir-p (pos dpos grid)
  (let* ((heights (ray grid pos dpos))
	 (tree-height (first heights))
	 (other-heights (rest heights)))
      (every #'(lambda (h) (> tree-height h)) other-heights)))

(defparameter *deltas* '((1 . 0) (0 . 1) (-1 . 0) (0 . -1)))

(defun visible-p (pos grid)
  (some #'(lambda (dpos) (visible-in-dir-p pos dpos grid)) *deltas*))

(defun grid-points (grid)
  (destructuring-bind (width . height) (dims grid)
    (loop for x from 0 below width
	  nconcing (loop for y from 0 below height
			 collect (cons x y)))))

(defun find-visible-trees (grid)
  (remove-if-not #'(lambda (pos) (visible-p pos grid)) (grid-points grid)))

(defun count-visible-trees (h0 heights)
  (let ((first-tall-tree (position-if #'(lambda (x) (>= x h0)) heights)))
    (if first-tall-tree
	(1+ first-tall-tree)
	; all trees are shorter, so we can see them all
	(length heights))))

(defun scenic-score-in-dir (grid pos dpos)
  (let* ((heights (ray grid pos dpos))
	 (tree-height (first heights))
	 (other-heights (rest heights)))    
    (count-visible-trees tree-height other-heights)))


(defun scenic-score (grid pos)
  (apply '* (mapcar #'(lambda (dpos) (scenic-score-in-dir grid pos dpos)) *deltas*)))

(defun find-max-scenic-score (grid)
  (fn-stream grid
	     (grid-points)
	     (mapcar #'(lambda (pos) (scenic-score grid pos)))
	     (apply 'max)))
	  
(fn-stream (read-lines "input1.txt")
	   (build-grid)
	   (find-visible-trees)
	   (length)
	   (print))
(fn-stream (read-lines "input1.txt")
	   (build-grid)
	   (find-max-scenic-score)
	   (print))
