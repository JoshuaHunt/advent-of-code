package main

// import (
// 	"bufio"
// 	"fmt"
// 	"log"
// 	"os"
// )

// type tree struct {
// 	height int
// 	seen   bool
// }

// func buildGrid(filename string) [][]tree {
// 	file, err := os.Open(filename)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	defer file.Close()

// 	scanner := bufio.NewScanner(file)

// 	grid := make([][]tree, 1)
// 	scanner.Scan()
// 	for _, h := range scanner.Bytes() {
// 		grid[0] = append(grid[0], tree{int(h - '0'), false})
// 	}
// 	rowWidth := len(grid[0])
// 	r := 1
// 	for scanner.Scan() {
// 		grid = append(grid, make([]tree, rowWidth))
// 		for c, h := range scanner.Bytes() {
// 			grid[r][c] = tree{int(h - '0'), false}
// 		}
// 		r++
// 	}
// 	return grid
// }

// type treeLine struct {
// 	numPeaks int
// 	peaks    [2]int // index of first and last tallest tree
// }

// func update(grid [][]tree, r int, c int, tallest int) int {
// 	// fmt.Printf("%v, %v, tallest is %v\n", r, c, tallest)
// 	if grid[r][c].height > tallest {
// 		if grid[r][c].seen == false {
// 			fmt.Printf("Seen %v, %v\n", r, c)
// 		}
// 		grid[r][c].seen = true
// 		return grid[r][c].height
// 	}
// 	return tallest
// }

// func main() {
// 	grid := buildGrid("input1.txt")
// 	h := len(grid)
// 	w := len(grid[0])
// 	// for r := 0; r < h; r++ {
// 	// 	for c := 0; c < w; c++ {
// 	// 		fmt.Print(grid[r][c].height)
// 	// 	}
// 	// 	fmt.Println()
// 	// }
// 	for c := 0; c < w; c++ {
// 		tallest := -1
// 		for r := 0; r < h; r++ {
// 			tallest = update(grid, r, c, tallest)
// 		}
// 		tallest = -1
// 		for r := h - 1; r >= 0; r-- {
// 			tallest = update(grid, r, c, tallest)
// 		}
// 	}
// 	for r := 0; r < h; r++ {
// 		tallest := -1
// 		for c := 0; c < w; c++ {
// 			tallest = update(grid, r, c, tallest)
// 		}
// 		tallest = -1
// 		for c := w - 1; c >= 0; c-- {
// 			tallest = update(grid, r, c, tallest)
// 		}
// 	}
// 	sum := 0
// 	for r := 0; r < h; r++ {
// 		for c := 0; c < w; c++ {
// 			if grid[r][c].seen {
// 				sum++
// 				// fmt.Print("X")
// 			} else {
// 				// fmt.Print(".")
// 			}
// 		}
// 		// fmt.Println()
// 	}
// 	fmt.Println(sum)
// }
