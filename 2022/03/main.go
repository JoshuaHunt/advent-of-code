package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"sort"
)

func splitHalves(line string) [2][]byte {
	length := len(line) / 2
	firstHalf := []byte(line[0:length])
	secondHalf := []byte(line[length:])
	return [2][]byte{firstHalf, secondHalf}
}

func readLines(scanner *bufio.Scanner, k int) ([][]byte, error) {
	lines := make([][]byte, k)
	for i := 0; i < k; i++ {
		if !scanner.Scan() {
			return lines, errors.New("End of file")
		}
		// successive calls to Bytes can re-use memory returned from previous calls, so we need to allocate memory to store the line
		line := scanner.Bytes()
		tmp := make([]byte, len(line))
		copy(tmp, line)
		lines[i] = tmp
	}
	return lines, nil
}

func getDuplicate(comparables ...[]byte) (byte, error) {
	for _, c := range comparables {
		fmt.Println(string(c))
		sort.Slice(c, func(i, j int) bool { return c[i] < c[j] })
		fmt.Println(string(c))
	}
	indices := make([]int, len(comparables))
	for {
		minIndex := 0
		minChar := comparables[0][indices[0]]
		allSame := true
		for i, j := range indices {
			if comparables[i][j] < minChar {
				minChar = comparables[i][j]
				minIndex = i
				allSame = false
			} else if comparables[i][j] > minChar {
				allSame = false
			}
		}
		if allSame {
			return minChar, nil
		}
		indices[minIndex]++
		if indices[minIndex] == len(comparables[minIndex]) {
			return 0, errors.New("No match")
		}
	}
}

func getPriority(char byte) int {
	if 'a' <= char && char <= 'z' {
		return (int)(char-'a') + 1
	} else {
		return (int)(char-'A') + 27
	}
}

func main() {
	filename := "input1.txt"
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	sum := 0
	i := 0
	for {
		lines, err := readLines(scanner, 3)
		i += 3
		if err != nil {
			// probably EOF
			break
		}
		duplicate, err := getDuplicate(lines[:]...)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(i, string(duplicate), getPriority(duplicate))
		sum += getPriority(duplicate)
	}
	fmt.Println(sum)
}
