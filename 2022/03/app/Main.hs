import Data.List (elemIndex)
import Data.List.Split (chunksOf)
import qualified Data.Set as Set

main = do
    contents <- getContents
    print . solvePart1 . lines $ contents
    print . solvePart2 . lines $ contents

solvePart1 :: [String] -> Int
solvePart1 = foldr acc 0
               where acc rucksack s = rucksackPriority rucksack + s

solvePart2 :: [String] -> Int
solvePart2 xs = sum . map (priority . findBadge) $ chunksOf 3 xs

findBadge :: [String] -> Char
findBadge xs = Set.elemAt 0 . foldr1 Set.intersection . map Set.fromList $ xs

rucksackPriority :: String -> Int
rucksackPriority = priority . commonElement . splitCompartments

priority :: Char -> Int
priority x = 1 + p
           where Just p = elemIndex x (['a' .. 'z'] ++ ['A' .. 'Z'])

splitCompartments :: String -> (String, String)
splitCompartments xs = (first, second)
                     where first = take (l `div` 2) xs
                           second = drop (l `div` 2) xs
                           l = length xs

commonElement :: (String, String) -> Char
commonElement (x,y) = Set.elemAt 0 (Set.intersection (Set.fromList x) (Set.fromList y))
