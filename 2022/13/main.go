package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

const (
	kInt  = iota
	kList = iota
)

type packet struct {
	// type: int or list
	t int
	v int
	l packets
}

func newListPacket(ps ...*packet) *packet {
	p := packet{
		kList,
		0,
		ps,
	}
	return &p
}

func newIntPacket(v int) *packet {
	p := packet{
		kInt,
		v,
		nil,
	}
	return &p
}

func wrapIntPacket(p *packet) *packet {
	newP := newListPacket()
	newP.l = newP.l.push(p)
	return newP
}

func (this packet) print() {
	if this.t == kInt {
		fmt.Print(this.v)
	} else {
		fmt.Print("[")
		for i, p := range this.l {
			p.print()
			if i+1 < len(this.l) {
				fmt.Print(",")
			}
		}
		fmt.Print("]")
	}
}

type packets []*packet

func (ps packets) push(p *packet) packets {
	return append(ps, p)
}

func (ps packets) pop() (packets, *packet) {
	l := len(ps)
	return ps[:l-1], ps[l-1]
}
func (ps packets) peek() *packet {
	return ps[len(ps)-1]
}

func compareInt(l, r int) int {
	if l < r {
		return kLt
	} else if l == r {
		return 0
	} else {
		return kGt
	}
}

const debug = false
const kLt = -1
const kGt = 1

func compare(l, r *packet, depth int) int {
	if l.t == kInt && r.t == kInt {
		if debug {
			fmt.Printf("%vCompare ints %v & %v\n", strings.Repeat(" ", depth), l.v, r.v)
		}
		return compareInt(l.v, r.v)
	} else if l.t == kList && r.t == kList {
		if debug {
			fmt.Printf("%vCompare lists of len %v & %v\n", strings.Repeat(" ", depth), len(l.l), len(r.l))
		}
		for i := 0; i < len(l.l) && i < len(r.l); i++ {
			c := compare(l.l[i], r.l[i], depth+1)
			if c != 0 {
				if debug {
					fmt.Printf("%vElement %v not eq: %v\n", strings.Repeat(" ", depth), i, c)
				}
				return c
			}
		}
		c := compareInt(len(l.l), len(r.l))
		if debug {
			fmt.Printf("%vComparing list lengths: %v\n", strings.Repeat(" ", depth), c)
		}
		return c
	} else if l.t == kInt && r.t == kList {
		if debug {
			fmt.Println(strings.Repeat(" ", depth) + "Wrap left")
		}
		return compare(wrapIntPacket(l), r, depth+1)
	} else if l.t == kList && r.t == kInt {
		if debug {
			fmt.Println(strings.Repeat(" ", depth) + "Wrap right")
		}
		return compare(l, wrapIntPacket(r), depth+1)
	}
	panic(fmt.Sprintf("Whoops: %v %v", l.t, r.t))
}

func parsePacket(text string) *packet {
	stack := make(packets, 0).push(newListPacket())

	for i := 0; i < len(text); i++ {
		c := text[i]
		if c == '[' {
			top := stack.peek()
			top.l = top.l.push(newListPacket())
			stack = stack.push(top.l.peek())
		} else if c == ']' {
			stack, _ = stack.pop()
		} else if c == ',' {
			continue
		} else {
			j := i
			for ; j < len(text) && '0' <= text[j] && text[j] <= '9'; j++ {
			}
			// Assume for now we only see single-digit numbers
			v, err := strconv.Atoi(string(text[i:j]))
			i = j - 1 // we'll advance at the end of the loop
			if err != nil {
				log.Fatal(err)
			}
			top := stack.peek()
			top.l = top.l.push(newIntPacket(v))
		}
	}
	return stack.peek()
}

func parsePacketsTwoChan(filename string, leftCh, rightCh chan *packet) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		leftCh <- parsePacket(scanner.Text())
		if !scanner.Scan() {
			break
		}
		rightCh <- parsePacket(scanner.Text())
		if !scanner.Scan() {
			break
		}
		// empty line between packets
	}
	close(leftCh)
	close(rightCh)
}

func parsePackets(filename string) packets {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	packets := make(packets, 0)

	for scanner.Scan() {
		if len(scanner.Text()) > 0 {
			packets = append(packets, parsePacket(scanner.Text()))
		}
	}

	return packets
}

func part1() {
	leftCh := make(chan *packet)
	rightCh := make(chan *packet)
	go parsePacketsTwoChan("input1.txt", leftCh, rightCh)
	i := 1
	s := 0
	for {
		l, lOk := <-leftCh
		r, rOk := <-rightCh
		if !(lOk && rOk) {
			break
		}
		if compare(l, r, 0) < 0 {
			fmt.Println(i)
			s += i
		}
		i++
	}
	fmt.Println(s)
}

func main() {
	packets := parsePackets("input1.txt")
	twoPacket := newListPacket(newListPacket(newIntPacket(2)))
	sixPacket := newListPacket(newListPacket(newIntPacket(6)))
	packets = packets.push(twoPacket)
	packets = packets.push(sixPacket)
	sort.Slice(
		packets,
		func(i, j int) bool { return compare(packets[i], packets[j], 0) == kLt },
	)
	twoIndex := 0
	sixIndex := 0
	for i, packet := range packets {
		fmt.Printf("%v: ", i+1)
		packet.print()
		fmt.Println()
		if packet == twoPacket {
			twoIndex = i + 1
		}
		if packet == sixPacket {
			sixIndex = i + 1
		}
	}

	fmt.Println(twoIndex * sixIndex)
}
