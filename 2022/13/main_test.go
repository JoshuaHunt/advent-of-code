package main

import (
	"fmt"
	"strings"
	"testing"
)

func prettyPrint(s string) {
	depth := 0
	indented := false
	for _, c := range s {
		if c == '[' {
			if indented {
				fmt.Println()
			}
			fmt.Println(strings.Repeat(" ", depth) + "[")
			depth++
			indented = false
		} else if c == ']' {
			if indented {
				fmt.Println()
			}
			depth--
			fmt.Println(strings.Repeat(" ", depth) + "]")
			indented = false
		} else if c != ',' {
			if !indented {
				fmt.Printf("%v", strings.Repeat(" ", depth))
				indented = true
			}
			fmt.Printf("%v", string(c))
		}
	}
}

func TestIsLeq(t *testing.T) {
	type test struct {
		left        string
		right       string
		wantCompare int
	}

	tests := []test{
		{left: "[[4,4],4,4]", right: "[[4,4],4,4,4]", wantCompare: -1},
		{left: "[[1],[2,3,4]]", right: "[[1],4]", wantCompare: -1},
		{
			left:        "[[1,[[],3,[2,7,7,5]],[10,[7,7,7,1]],[[9],4,4,7],[[7],1]],[],[5,10,7],[[0,[],[]]],[]]",
			right:       "[[2,2],[[0,[9,5,0,6],10,[],[8,1,6,6,0]],0,[[],[],[7,3,1,8,1]],10,1]]",
			wantCompare: -1,
		},
		{
			left:        "[[],[[5,[4,7,9,5]],[6,4]]]",
			right:       "[[[[7,1,7,3,2]],2,0,[[6,10]],[3]],[[],[8,[],[3]],[6,[],0,8,[10,5]]],[],[6,9,[[8,3,2,1],6]],[[3],[5,[]],[[0,4,7,0],0],[[1,2,7],[0,3],[10,6,0,5,5],[],4]]]",
			wantCompare: -1,
		},
		{
			left:        "[[[[10,1,5,7,10],10,[],[1,7]],8,10],[[10,[9,6,10,9]],8,4,[[2,1,0],0],[6,7,8,[6,8],10]],[[],[[],[1,2,1],8],7,[[3,0,2,7,9],9],[5]],[5,[1,1,[]]]]",
			right:       "[[[[8],0,8,[]]],[[7],[[6],6,[9,0,6],[1,2,1,0],2]],[[],2,5,4,[1,[3,8],[5],7]],[[[0,1]],[5,[9,9,7,2,8],4],9],[0,3]]",
			wantCompare: 1,
		},
	}

	for i, tc := range tests {
		fmt.Println(tc.left)
		prettyPrint(tc.left)
		fmt.Println(tc.right)
		prettyPrint(tc.right)
		c := compare(parsePacket(tc.left), parsePacket(tc.right), 0)
		if c != tc.wantCompare {
			t.Fatalf("[%v] expected: %v, got: %v", i, tc.wantCompare, c)
		}
	}
}
