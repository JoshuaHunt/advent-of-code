(defun read-lines (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
	  while line
	  collect line)))

(defun group-non-empty (xs)
  (let ((result '())
	(current '()))
    (dolist (x xs)
      (if (string= x "")
	  (progn
	     (push (nreverse current) result)	   
	     (setq current '()))
	  (push x current)))
    (push (nreverse current) result)
    (nreverse result)))

(defun sum-strings (xs)
  (reduce #'+ (map 'list #'parse-integer xs)))

(defun take (n xs)
  (loop repeat n for x in xs collect x))

(let* ((lines (read-lines "input1.txt"))
      (groups (group-non-empty lines))
      (sums (map 'list #'sum-strings groups))
      (top (take 3 (sort sums #'>))))
  (format t "~a~%" (reduce #'+ top)))
      
