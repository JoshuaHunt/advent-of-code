import Data.List (sort)

main = do
    contents <- getContents
    print . solvePart1 . lines $ contents
    print . solvePart2 . lines $ contents

solvePart1 = maximum . elfCalories
solvePart2 = sum . (take 3) . reverse . sort . elfCalories
elfCalories = (map sum) . (map $ map read) . (splitElves [])
    where splitElves acc [] = [acc]
          splitElves acc (x:xs) = if x == ""
                                  then acc : splitElves [] xs
                                  else splitElves (x:acc) xs
