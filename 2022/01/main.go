package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
)

func max(x int, y int) int {
	if x > y {
		return x
	}
	return y
}

// Returns (elf calories, more to read from scanner, error)
func getElfCalories(scanner *bufio.Scanner) (int, bool, error) {
	elfCalories := 0
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			return elfCalories, true, nil
		}
		itemCalories, err := strconv.Atoi(line)
		if err != nil {
			return 0, true, err
		}
		elfCalories += itemCalories
	}
	return elfCalories, false, nil
}

func getKMaxCalories(filename string, k int) ([]int, error) {
	file, err := os.Open(filename)
	if err != nil {
		return []int{}, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	maxCalories := make([]int, k+1)
	for {
		elfCalories, canContinue, err := getElfCalories(scanner)
		if err != nil {
			return []int{}, err
		}
		maxCalories[0] = elfCalories
		sort.Ints(maxCalories)
		if !canContinue {
			break
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return maxCalories[1 : k+1], nil
}

func sum(xs []int) int {
	total := 0
	for _, x := range xs {
		total += x
	}
	return total
}

func main() {
	maxCalories, err := getKMaxCalories("input1.txt", 3)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(sum(maxCalories))
}
