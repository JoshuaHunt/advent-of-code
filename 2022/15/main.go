package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

type point struct {
	x, y int
}

type disk struct {
	centre point
	radius int
}

type interval struct {
	start, end int
}

func parseInt(descr string) int {
	x, err := strconv.Atoi(descr[2 : len(descr)-1])
	if err != nil {
		log.Fatal(err)
	}
	return x
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func parseDisks(filename string) ([]disk, []point) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	disks := make([]disk, 0)
	distinctBeacons := make(map[point]bool)
	beacons := make([]point, 0)
	for scanner.Scan() {
		tokens := strings.Split(scanner.Text(), " ")
		centreX := parseInt(tokens[2])
		centreY := parseInt(tokens[3])
		boundaryX := parseInt(tokens[8])
		boundaryY := parseInt(tokens[9] + ".")
		radius := abs(boundaryY-centreY) + abs(boundaryX-centreX)
		disks = append(disks, disk{point{centreX, centreY}, radius})
		beacon := point{boundaryX, boundaryY}
		if _, ok := distinctBeacons[beacon]; !ok {
			beacons = append(beacons, beacon)
		}
		distinctBeacons[beacon] = true
	}
	return disks, beacons
}

func extractIntervals(disks []disk, y int) []interval {
	intervals := make([]interval, 0)
	for _, d := range disks {
		if y < d.centre.y-d.radius {
			continue
		}
		if y > d.centre.y+d.radius {
			continue
		}
		// abs(start - d.centre.x) + abs(y - d.centre.y) = d.radius
		start := d.centre.x + abs(y-d.centre.y) - d.radius
		end := d.centre.x - abs(y-d.centre.y) + d.radius
		intervals = append(intervals, interval{start, end})
	}
	return intervals
}

func countPoints(intervals []interval, beacons []point, y int) int {
	sort.Slice(intervals, func(i, j int) bool { return intervals[i].start < intervals[j].start })
	sort.Slice(beacons, func(i, j int) bool { return beacons[i].x < beacons[j].x })
	x := intervals[0].start
	i := 0
	covered := 1
	for _, interval := range intervals {
		if x < interval.start {
			for ; i < len(beacons) && beacons[i].x <= interval.end; i++ {
				if interval.start <= beacons[i].x && beacons[i].y == y {
					covered--
				}
			}
			covered += interval.end + 1 - interval.start
			x = interval.end
		} else if x < interval.end {
			for ; i < len(beacons) && beacons[i].x <= interval.end; i++ {
				if x < beacons[i].x && beacons[i].y == y {
					covered--
				}
			}
			covered += interval.end - x
			x = interval.end
		}
	}
	return covered
}

func main1() {
	disks, beacons := parseDisks("input1.txt")
	y := 2_000_000
	intervals := extractIntervals(disks, y)
	log.Println(countPoints(intervals, beacons, y))
}

func getCandidates(ch chan point, disks []disk) {
	for _, d := range disks {
		for x := d.centre.x - d.radius - 1; x <= d.centre.x+d.radius+1; x++ {
			y := d.centre.y + abs(x-d.centre.x) - (d.radius + 1)
			ch <- point{x, y}
			y = d.centre.y - abs(x-d.centre.x) + (d.radius + 1)
			ch <- point{x, y}
		}
	}
	close(ch)
}

func dist(p, q point) int {
	return abs(p.x-q.x) + abs(p.y-q.y)
}

func main() {
	disks, _ := parseDisks("input1.txt")
	ch := make(chan point)
	go getCandidates(ch, disks)
	w := 4_000_000
	for {
		p, ok := <-ch
		if !ok {
			break
		}
		if p.x < 0 || p.y < 0 || p.x > w || p.y > w {
			continue
		}
		valid := true
		for _, d := range disks {
			if dist(p, d.centre) <= d.radius {
				valid = false
				break
			}
		}
		if valid {
			fmt.Println(p.x*w + p.y)
			return
		}
	}
}
