package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type Assignment struct {
	start int
	end   int
}

func newAssignment(desc string) (*Assignment, error) {
	ends := strings.Split(desc, "-")
	start, err := strconv.Atoi(ends[0])
	if err != nil {
		return nil, err
	}
	end, err := strconv.Atoi(ends[1])
	if err != nil {
		return nil, err
	}
	return &Assignment{start, end}, nil
}

func fullyContains(a, b Assignment) bool {
	return a.start <= b.start && b.end <= a.end
}

func hasOverlap(a, b Assignment) bool {
	return !(a.end < b.start || b.end < a.start)
}

func main() {
	filename := "input1.txt"
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	overlaps := 0
	for scanner.Scan() {
		assignments, err := parseAssignments(scanner.Text())
		if err != nil {
			log.Fatal(err)
		}
		if hasOverlap(assignments[0], assignments[1]) {
			overlaps++
		}
	}
	fmt.Println(overlaps)
}

func parseAssignments(line string) ([2]Assignment, error) {
	parts := strings.Split(line, ",")
	a0, err0 := newAssignment(parts[0])
	a1, err1 := newAssignment(parts[1])
	assignments := [2]Assignment{*a0, *a1}
	if err0 != nil {
		return assignments, err0
	}
	if err1 != nil {
		return assignments, err1
	}
	return assignments, nil
}
