(load "~/scripts/lib.lisp")

(defun parse-ranges (descr)
  (destructuring-bind (l0-descr r0-descr l1-descr r1-descr)
      (uiop:split-string descr :separator "-,")
    (let ((l0 (parse-integer l0-descr))
	  (r0 (parse-integer r0-descr))
	  (l1 (parse-integer l1-descr))
	  (r1 (parse-integer r1-descr)))
      `((,l0 ,r0) (,l1 ,r1)))))

(defun has-overlap (ranges)
  (destructuring-bind ((l0 r0) (l1 r1)) ranges
    (not (or (< r0 l1) (< r1 l0)))))

(defun contains (range0 range1)
  (destructuring-bind ((l0 r0) (l1 r1)) `(,range0 ,range1)
    (<= l0 l1 r1 r0)))

(defun are-nested (ranges)
  (or (contains (first ranges) (second ranges))
      (contains (second ranges) (first ranges))))

(fn-stream (read-lines "input1.txt")
	   (mapcar #'parse-ranges)
	   (remove-if-not #'are-nested)
	   (length)
	   (format t "~a~%"))

(fn-stream (read-lines "input1.txt")
	   (mapcar #'parse-ranges)
	   (remove-if-not #'has-overlap)
	   (length)
	   (format t "~a~%"))
