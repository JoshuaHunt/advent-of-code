main = do
    contents <- getContents
    print . solvePart1 . lines $ contents
    print . solvePart2 . lines $ contents

solvePart1 :: [String] -> Int
solvePart1 = length . filter (uncurry isNestedRange) . map parseRanges

solvePart2 = length . filter (uncurry areOverlappingRanges) . map parseRanges

data Range = Range Int Int

parseRanges :: String -> (Range, Range)
parseRanges descr = let [xRange, yRange] = split ',' descr
                        [x0, x1] = split '-' xRange
                        [y0, y1] = split '-' yRange
                    in (Range (read x0) (read x1), Range (read y0) (read y1))

isNestedRange :: Range -> Range -> Bool
isNestedRange (Range x0 x1) (Range y0 y1) = (x0 <= y0 && y1 <= x1) || (y0 <= x0 && x1 <= y1)

areOverlappingRanges :: Range -> Range -> Bool
areOverlappingRanges (Range x0 x1) (Range y0 y1) = not (x1 < y0 || y1 < x0)

split :: (Eq a) => a -> [a] -> [[a]]
split sep xs = split' [] xs
               where split' acc [] = [acc]
                     split' acc (y:ys) = if y == sep
                                         then acc : split' [] ys
                                         else split' (acc ++ [y]) ys