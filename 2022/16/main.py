from heapq import heappop, heappush
from copy import deepcopy
from functools import lru_cache

def parse(filename) -> dict[str,(str,int,list[str])]:
    nodes = {}
    with open(filename) as f:
        for line in f.readlines():
            name = line.split(" ")[1]
            flow = int(line[line.index("=")+1:line.index(";")])
            neighbours = [n.strip(",\n") for n in line.split(" ")[9:]]
            nodes[name] = (name, flow, neighbours)
    return nodes


@lru_cache(maxsize=None)
def dist(source, dest):
    global nodes
    visited = set()
    queue = [(0, source)]
    while len(queue) > 0:
        (dist, cur) = heappop(queue)
        if cur == dest:
            return dist
        for next in nodes[cur][2]:
            if next in visited:
                continue
            heappush(queue, (dist+1, next))
        visited.add(cur)
    raise RuntimeError(f"Couldn't find route from {source} to {dest}")

def getSequences(nodes, t, startNode, visited=None):
    if visited is None:
        visited = set()
    nextNodes = [n for n in nodes if n not in visited and dist(startNode, n) + 1 <= t]
    if len(nextNodes) == 0:
        return [[]]
    sequences = []
    for n in nextNodes:
        visited.add(n)
        travelTime = dist(startNode, n) + 1
        nextSequences = getSequences(nextNodes, t - travelTime, n, visited)
        for i in range(len(nextSequences)):
            sequences.append([n] + nextSequences[i])
        sequences.extend(nextSequences)
        visited.remove(n)
    return sequences

def getVolume(s, nodes, maxT):
    if not s:
        return 0
    path = s.split(",")
    volume = 0
    flow = 0
    curr = "AA"
    t = 0
    for n in path:
        dt = dist(curr, n) + 1
        curr = n
        volume += flow * dt
        flow += nodes[n][1]
        t += dt
    volume += (maxT - t) * flow
    return volume

def findMaxVolume(volumes):
    best = 0
    for i in range(len(volumes)):
        v1, p1 = volumes[i]
        s1 = set(p1.split(","))
        for j in range(i+1, len(volumes)):
            v2, p2 = volumes[j]
            s2 = set(p2.split(","))
            if s1.isdisjoint(s2):
                best = max(best, v1 + v2)
                break
    return best

nodes = parse("input1.txt")
flowNodes = [n[0] for n in nodes.values() if n[1] > 0]

print(" --- Part 1 ---")
maxT = 30
print("Get sequences")
sequences = set([",".join(s) for s in getSequences(flowNodes, maxT, "AA")])
print("Get volumes")
volumes = [(getVolume(s, nodes, maxT),s) for s in sequences]
print("Sort volumes")
volumes.sort(reverse=True)
print("Max volume:", volumes[0][0])

print(" --- Part 1 ---")
maxT = 26
print("Get sequences")
sequences = set([",".join(s) for s in getSequences(flowNodes, maxT, "AA")])
print("Get volumes")
volumes = [(getVolume(s, nodes, maxT),s) for s in sequences]
print("Sort volumes")
volumes.sort(reverse=True)
print("Max volume:")
print(findMaxVolume(volumes))