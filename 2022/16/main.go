package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

type node struct {
	name       string
	flow       int
	neighbours []string
	open       bool
}

func parseFlowRate(descr string) int {
	descr = strings.TrimPrefix(descr, "rate=")
	descr = strings.TrimSuffix(descr, ";")
	x, err := strconv.Atoi(descr)
	if err != nil {
		log.Fatal(err)
	}
	return x
}

func parseNode(descr string) node {
	// "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB"
	tokens := strings.Split(descr, " ")
	name := tokens[1]
	flowRate := parseFlowRate(tokens[4])
	neighbourNames := tokens[9:]
	for i := range neighbourNames {
		neighbourNames[i] = strings.TrimSuffix(neighbourNames[i], ",")
	}
	node := node{name, flowRate, neighbourNames, false}
	return node
}

func parseNodes(filename string) map[string]*node {
	nodes := make(map[string]*node)
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		node := parseNode(scanner.Text())
		nodes[node.name] = &node
	}
	return nodes
}

type pathSearch struct {
	n    string
	dist int
}

var shortestPathCache = make(map[string]map[string]int)

func shortestPath(s, d string, nodes map[string]*node) int {
	// if _, okS := shortestPathCache[s]; okS {
	// 	if _, okD := shortestPathCache[s][d]; okD {
	// 		return shortestPathCache[s][d]
	// 	}
	// }
	visited := make(map[string]bool)
	queue := []pathSearch{{s, 0}}
	for len(queue) > 0 {
		sort.Slice(queue, func(i, j int) bool { return queue[i].dist < queue[j].dist })
		var cur pathSearch
		cur, queue = queue[0], queue[1:]
		if cur.n == d {
			// if _, okS := shortestPathCache[s]; !okS {
			// 	shortestPathCache[s] = make(map[string]int)
			// }
			// shortestPathCache[s][d] = cur.dist
			return cur.dist
		}
		for _, neighbour := range nodes[cur.n].neighbours {
			if _, ok := visited[neighbour]; ok {
				continue
			}
			queue = append(queue, pathSearch{neighbour, cur.dist + 1})
		}
		visited[cur.n] = true
	}
	panic("No path found")
}

const kMaxT = 30

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func max(x, y int) int {
	return -min(-x, -y)
}

type searchState1 struct {
	currentNode  string
	visitedNodes map[string]bool
	t            int
	flow         int
	volume       int
}

func findBestVolume1(nodes map[string]*node, interestingNodeNames []string, state searchState1) searchState1 {
	// default option is do nothing
	bestState := state
	bestState.t = kMaxT
	bestState.volume += (kMaxT - state.t) * state.flow
	currentNode := state.currentNode

	for _, n := range interestingNodeNames {
		if state.visitedNodes[n] {
			continue
		}
		dt := shortestPath(state.currentNode, n, nodes) + 1
		if state.t+dt >= kMaxT {
			continue
		}

		state.visitedNodes[n] = true
		state.t += dt
		state.volume += state.flow * dt
		state.flow += nodes[n].flow
		state.currentNode = n
		newState := findBestVolume1(nodes, interestingNodeNames, state)
		state.currentNode = currentNode
		state.flow -= nodes[n].flow
		state.volume -= state.flow * dt
		state.t -= dt
		state.visitedNodes[n] = false
		if newState.volume > bestState.volume {
			bestState = newState
		}
	}
	return bestState
}

func getPositiveFlowNodes(nodes map[string]*node) []string {
	flowNodes := make([]string, 0)
	for name, node := range nodes {
		if node.flow > 0 {
			flowNodes = append(flowNodes, name)
		}
	}
	return flowNodes
}

func main1() {
	nodes := parseNodes("input1.txt")
	positiveFlowNodes := getPositiveFlowNodes(nodes)
	bestState := findBestVolume1(nodes, positiveFlowNodes, searchState1{"AA", make(map[string]bool), 0, 0, 0})
	fmt.Println(bestState)
}

type traveler struct {
	name       string
	dest       string
	travelTime int
}

type travelerHeap []traveler

func (h *travelerHeap) pop() traveler {
	if len(*h) > 2 {
		panic("What a big heap")
	}
	old := *h
	if len(old) == 1 || old[0].travelTime <= old[1].travelTime {
		item := old[0]
		*h = old[1:]
		return item
	} else {
		item := old[1]
		*h = old[:1]
		return item
	}
}

func (h *travelerHeap) push(t traveler) {
	*h = append(*h, t)
}

type searchState struct {
	travelers    travelerHeap
	visitedNodes map[string]bool
	t            int
	flow         int
	volume       int
}

func findBestVolume(nodes map[string]*node, interestingNodeNames []string, state searchState) searchState {
	nextArrival := state.travelers.pop()
	fmt.Printf(strings.Repeat(".", state.t)+" New arrival %v\n", nextArrival)
	dt := min(nextArrival.travelTime, kMaxT-state.t)
	state.t += dt
	for i := 1; i < len(state.travelers); i++ {
		state.travelers[0].travelTime -= dt
	}
	state.volume += state.flow * dt
	state.flow += nodes[nextArrival.dest].flow

	// default option is do nothing
	bestState := state
	bestState.t = kMaxT
	bestState.volume += (kMaxT - state.t) * state.flow

	for _, n := range interestingNodeNames {
		if state.visitedNodes[n] {
			continue
		}
		travelTime := shortestPath(nextArrival.dest, n, nodes) + 1
		if state.t+travelTime >= kMaxT {
			continue
		}

		state.visitedNodes[n] = true
		state.travelers.push(traveler{nextArrival.name, n, travelTime})
		fmt.Printf("%v %v journeys towards %v\n", strings.Repeat(".", state.t), nextArrival.name, n)
		newState := findBestVolume(nodes, interestingNodeNames, state)
		state.travelers = state.travelers[:1]
		state.visitedNodes[n] = false

		if newState.volume > bestState.volume {
			fmt.Printf("%v New best state: %v\n", strings.Repeat(".", state.t), bestState)
			bestState = newState
		}
	}
	return bestState
}

func main() {
	nodes := parseNodes("input0.txt")
	positiveFlowNodes := getPositiveFlowNodes(nodes)
	bestState := findBestVolume(nodes, positiveFlowNodes,
		searchState{[]traveler{{"Me", "AA", 0} /*, {"Nelly", "AA", 0}*/},
			make(map[string]bool), 0, 0, 0})
	fmt.Println(bestState)
}

// t := 0
// flow := 0
// volume := 0
// currentNode := "AA"
// for t < kMaxT {
// 	var nextNode *node
// 	var bestDt, bestDvolume int
// 	for _, node := range nodes {
// 		if node.open {
// 			continue
// 		}
// 		dt := shortestPath(currentNode, node.name, nodes) + 1
// 		dvolume := max(kMaxT-t-dt, 0) * node.flow

// 		if dvolume > bestDvolume {
// 			bestDvolume = dvolume
// 			bestDt = dt
// 			nextNode = node
// 		}
// 	}
// 	if nextNode == nil {
// 		break
// 	}

// 	fmt.Printf("Visit %v\n", nextNode)
// 	bestDt = min(bestDt, kMaxT-t)
// 	volume += bestDt * flow
// 	t += bestDt
// 	fmt.Printf("Vol: +%v for %v minutes => %v @ %v)\n", flow, bestDt, volume, t)
// 	flow += nodes[nextNode.name].flow
// 	fmt.Printf("New flow is %v / min\n", flow)
// 	nodes[nextNode.name].open = true
// 	currentNode = nextNode.name
// }
// fmt.Printf("Remaining time:\n")
// dt := kMaxT - t
// volume += dt * flow
// t += dt
// fmt.Printf("Vol: +%v for %v minutes => %v @ %v)\n", flow, dt, volume, t)
