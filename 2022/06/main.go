package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func findRepetition(bytes []byte) int {
	counts := make(map[byte]int)
	mlen := 14
	for i := 0; i < len(bytes); i++ {
		counts[bytes[i]]++
		if i >= mlen {
			counts[bytes[i-mlen]]--
			if counts[bytes[i-mlen]] == 0 {
				delete(counts, bytes[i-mlen])
			}
		}
		if len(counts) == mlen {
			return i + 1
		}
	}
	log.Fatal("End of line")
	return 0
}

func main() {
	filename := "input1.txt"
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		bytes := scanner.Bytes()
		fmt.Println(findRepetition(bytes))
	}
}
