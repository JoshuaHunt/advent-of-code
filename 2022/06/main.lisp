(load "~/scripts/lib.lisp")

(defun find-marker (size)
  (fn-stream (read-lines "input1.txt")
	     (first)
	     (funcall (lambda (x) (coerce x 'list)))
	     (windows size)
	     (mapcar #'list->set)
	     (position-if #'(lambda (set) (equal (length set) size)))
	     (+ size) ; add on the length of the window, we want the index of the last char	     
	     (print)))

(find-marker 4)
(find-marker 14)
  
  

 
