package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type point struct {
	x, y, z int
}

func newPoint(coords []string) point {
	x, err := strconv.Atoi(coords[0])
	if err != nil {
		log.Fatal(err)
	}
	y, err := strconv.Atoi(coords[1])
	if err != nil {
		log.Fatal(err)
	}
	z, err := strconv.Atoi(coords[2])
	if err != nil {
		log.Fatal(err)
	}
	return point{2 * x, 2 * y, 2 * z}
}

func (p point) shift(dp point) point {
	return point{p.x + dp.x, p.y + dp.y, p.z + dp.z}
}

func (p point) scale(a int) point {
	return point{a * p.x, a * p.y, a * p.z}
}

func (p point) half() point {
	return point{p.x / 2, p.y / 2, p.z / 2}
}

const (
	kEmpty   = iota
	kFilled  = iota
	kSurface = iota
)

var deltas = []point{{-1, 0, 0}, {1, 0, 0}, {0, -1, 0}, {0, 1, 0}, {0, 0, 1}, {0, 0, -1}}

func fill(cubeCentres map[point]int, p point, v int) int {
	if cubeCentres[p] != kSurface {
		return 0
	}
	cubeCentres[p] = v
	changed := 1
	for _, d := range deltas {
		changed += fill(cubeCentres, p.shift(d.scale(2)), v)
	}
	return changed
}

func main() {
	file, err := os.Open("input1.txt")
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	faceCentres := make(map[point]int)
	cubeCentres := make(map[point]int)
	points := 0
	for scanner.Scan() {
		points++
		coords := newPoint(strings.Split(scanner.Text(), ","))
		cubeCentres[coords] = kFilled
		for _, d := range deltas {
			faceCentres[coords.shift(d)] += 1
			c := coords.shift(d.scale(2))
			if cubeCentres[c] == kEmpty {
				cubeCentres[c] = kSurface
			}
			for _, d2 := range deltas {
				c = coords.shift(d.scale(2)).shift(d2.scale(2))
				if cubeCentres[c] == kEmpty {
					cubeCentres[c] = kSurface
				}
			}
		}
	}
	uniqueFaces := len(faceCentres)
	expectedFreeFaces := 6 * points
	blockedFaces := expectedFreeFaces - uniqueFaces
	freeFaces := expectedFreeFaces - 2*blockedFaces
	fmt.Println("Free faces", freeFaces)

	currentSurface := kSurface + 1
	maxXP := point{-1, -1, -1}
	surfaceCounts := make(map[int]int)
	for p, v := range cubeCentres {
		if v == kSurface {
			surfaceCounts[currentSurface] = fill(cubeCentres, p, currentSurface)
			currentSurface++
			if p.x > maxXP.x {
				maxXP = p
				log.Println("New maxXP", maxXP)
			}
		}
	}
	fmt.Println(surfaceCounts)
	exteriorSurface := cubeCentres[maxXP]
	exteriorCount := 0
	for p := range faceCentres {
		for _, d := range deltas {
			if cubeCentres[p.shift(d)] == exteriorSurface {
				exteriorCount++
				break
			}
		}
	}
	fmt.Println(exteriorCount)
}
