(use-modules (utils)
	     (srfi srfi-26))

(define my-input (call-with-input-file "2024/input20.txt" read-lines))

(define (parse-grid lines)
  (grid.new (map string->list lines)))

(define (passable? c)
  (or (equal? c #\.)
      (equal? c #\S)
      (equal? c #\E)))

(define (path->distances path)
  (define (iter rest distance distances)
    (if (null? rest)
	distances
	(begin
	  (hash-table.set! distances (car rest) distance)
	  (iter (cdr rest) (1- distance) distances))))
  (iter path
	(1- (length path))
	(hash-table.new)))

(define (get-distance-to-goal g)
  (let* ((start (only-element (grid.filter g (cut equal? <> #\S))))
	 (end (only-element (grid.filter g (cut equal? <> #\E))))
	 (path (bfs-result->path
		(bfs-one start
			 (lambda (s)
			   (filter (compose passable?
					    (cut grid.get g <>))
				   (orthogonal-neighbours s)))
			 (const 1)
			 (cut equal? end <>)))))
    (path->distances path)))

(define (ball min max)
  (define (iter x y)
    (cond ((< max x)
	   '())
	  ((< (- max (abs x))
	      y)
	   (iter (1+ x)
		 (- (- max (abs (1+ x))))))
	  ((< (+ (abs x) (abs y))
	      min)
	   (iter x (1+ y)))
	  (else
	   (cons (list x y)
		 (iter x (1+ y))))))
  (iter (- max) 0))

(define (get-time-saved from-to distances)
  (let ((from (car from-to))
	(to (cadr from-to)))
    (- (hash-table.get distances (car from-to))
       (+ (point.taxi-cab-distance from to) (hash-table.get distances (cadr from-to))))))

(define (cheat->pretty cheat grid)
  (let ((g (grid.copy grid))
	(dp (dir->vector (dir-from (car cheat) (cadr cheat)))))
    (grid.set! g (point.add (car cheat) dp) #\1)
    (grid.set! g (cadr cheat) #\2)
    (grid->string g)))

(define (get-good-cheats grid distances len saving-min)
  (define dposes (ball 2 len))
  (parallel-flat-map
   (lambda (p)
     (->> dposes
	  (map (lambda (dpos) (list p
				    (point.add p dpos))))
	  (filter (lambda (c)
		    (and (hash-table.contains-key? distances (car c))
			 (hash-table.contains-key? distances (cadr c)))))
	  (filter (lambda (c) (<= saving-min (get-time-saved c distances))))))
   (grid.filter grid passable?)))

(define (count-cheats len saving-min lines)
  (let* ((grid (parse-grid lines))
	 (distances (get-distance-to-goal grid)))
    (length (get-good-cheats grid distances len saving-min))))

(define (solve-1 lines)
  (count-cheats 2 100 lines))

(define (solve-2 lines)
  (count-cheats 20 100 lines))

(define (debug c g d)
  (for-each (lambda (c) (format #t "~a~%~a~%~%" (cheat->pretty c g) (get-time-saved c d))) (filter (lambda (c) (<= 11 (get-time-saved c d))) (get-cheats g 2))))

(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
