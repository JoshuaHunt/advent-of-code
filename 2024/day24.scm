(use-modules (utils)
	     (srfi srfi-1)
	     (srfi srfi-9 gnu)
	     (srfi srfi-26)
	     (srfi srfi-64)
	     (ice-9 match)
	     (ice-9 futures)
	     (ice-9 format))

(define (parse-wire-state line)
  (let ((parts (string-split line #\:)))
    (cons (car parts)
	  (equal? (string-ref (cadr parts) 1) #\1))))

(test-begin "parse-wire-state")
(test-equal "parse true" (parse-wire-state "x01: 1") (cons "x01" #t))
(test-equal "parse false" (parse-wire-state "x02: 0") (cons "x02" #f))
(test-end "parse-wire-state")

(define (parse-wire-states lines)
  (map parse-wire-state (take-until-before empty-string? lines)))

(define (parse-wire-rule line)
  ;; Returns (output operator input0 input1)
  (let ((tokens (string-split line #\space)))
    (list (list-ref tokens 4)
	  (string->symbol (list-ref tokens 1))
	  (list-ref tokens 0)
	  (list-ref tokens 2))))

(define (rule->output rule) (car rule))
(define (rule->inputs rule) (cddr rule))
(define (rule-operator rule) (cadr rule))

(define (parse-wire-rules lines)
  (map parse-wire-rule (drop-until-after empty-string? lines)))

(define (output-gates rules)
  ;; Returns all gate outputs (i.e. all gates apart from x?? and y??).
  (map car rules))

(define (x-gate? g)
  (and (string? g)
       (equal? (string-ref g 0) #\x)))

(define (y-gate? g)
  (and (string? g)
       (equal? (string-ref g 0) #\y)))

(define (xy-input-gate? g)
  (or (x-gate? g) (y-gate? g)))

(define (z-output-gate? g)
  (equal? (string-ref g 0) #\z))

(define (find-z-outputs rules)
  ;; Returns z outputs in descending lexicographical order (i.e. most
  ;; significant bit first).
  (sort (->> rules
	     (map car)
	     (filter z-output-gate?))
	string>))

(define (all-gates rules)
  (uniq (append (map car rules)
		(map caddr rules)
		(map cadddr rules))))

(define (rule-has-input? gate)
  (lambda (rule)
    (or (equal? (list-ref rule 2) gate)
	(equal? (list-ref rule 3) gate))))

(define (rule-has-output? gate)
  (lambda (r)
    (equal? (rule->output r) gate)))

(define (children rules gate)
  (map rule->output (filter (rule-has-input? gate) rules)))

(define (parents rules gate)
  (let ((r (find (rule-has-output? gate) rules)))
    (if r
        (rule->inputs r)
	'())))

(define (descendants rules gate)
  ;; Returns all descendants of `gate`, not including gate itself.
  (define (foo g)
    (let ((children (filter (rule-has-input? g) rules)))
      (if children
	  (cons g (flat-map (lambda (c) (foo (car c)))
			    children))
	  (list g))))
  (uniq (cdr (foo gate))))

(define (uniq xs)
  (hash-set->list (list->hash-set xs)))

(test-begin "find-z-outputs")
(test-equal "foo"
  '("z1" "z0")
  (find-z-outputs
   (parse-wire-rules
    '(""
      "x AND y -> z1"
      "a OR B -> y"
      "c XOR d -> z0"))))
(test-end "find-z-outputs")


(define (compute-output-value cache rules states o)
  (let ((cached (hash-table.get cache o)))
    (if cached
	cached
	(let* ((rule (assoc-ref rules o))
	       (hardcoded (assoc o states))
	       (value (cond (hardcoded
			     (cdr hardcoded))
			    (rule
			     (match rule
			       (('AND x y)
				(and (compute-output-value cache rules states x)
				     (compute-output-value cache rules states y)))
			       (('OR x y)
				(or (compute-output-value cache rules states x)
				    (compute-output-value cache rules states y)))
			       (('XOR x y)
				(let ((ox (compute-output-value cache rules states x))
				      (oy (compute-output-value cache rules states y)))
				  (and (or ox oy)
				       (not (and ox oy)))))))
			    (else #f))))
	  (hash-table.set! cache o value)
	  value))))

(test-begin "compute-output-value")
(test-equal "initial"
  (compute-output-value (hash-table.new) '() '(("x00" . #t)) "x00")
  #t)
(test-equal "initial overrides computed"
  #f
  (compute-output-value (hash-table.new)
			'(("z" . (AND "x" "y")))
			'(("x" . #t)
			  ("y" . #t)
			  ("z" . #f))
			"z"))
(test-equal "and true"
  (compute-output-value
   (hash-table.new)
   '(("z" . (AND "x" "y")))
   '(("x" . #t)
     ("y" . #t))
   "z")
  #t)
(test-equal "and false"
  (compute-output-value
   (hash-table.new) 
   '(("z" . (AND "x" "y")))
   '(("x" . #t)
     ("y" . #f))
   "z")
  #f)
(test-equal "or true"
  (compute-output-value
   (hash-table.new) 
   '(("z" . (OR "x" "y")))
   '(("x" . #t)
     ("y" . #f))
   "z")
  #t)
(test-equal "or false"
  (compute-output-value
   (hash-table.new) 
   '(("z" . (OR "x" "y")))
   '(("x" . #f)
     ("y" . #f))
   "z")
  #f)
(test-equal "xor true"
  #t
  (compute-output-value
   (hash-table.new) 
   '(("z" . (XOR "x" "y")))
   '(("x" . #t)
     ("y" . #f))
   "z"))
(test-equal "xor false"
  (compute-output-value
   (hash-table.new)
   '(("z" . (XOR "x" "y")))
   '(("x" . #t)
     ("y" . #t))
   "z")
  #f)
(test-end "compute-output-value")

(define (bool->bit b)
  (if b #\1 #\0))

(define (run rules inputs)
  (let* ((outputs (find-z-outputs rules))
 	 (output (map (lambda (o) (compute-output-value (hash-table.new) rules inputs o)) outputs)))
    (string->number (list->string (map bool->bit output))
		    2)))

(define (gate-label prefix i)
  ;; e.g. (gate-label "x" 3) is "x03".
  (format #f "~a~2,,,'0@a" prefix i))

(define (gate-number g)
  (string->number (substring g 1)))

(define (number->bits prefix x)
  (define (iter rest i)
    (if (<= rest 0)
	'()
	(let ((label (gate-label prefix i))
	      (value (= 1 (modulo rest 2))))
	  (cons (cons label value)
		(iter (floor (/ rest 2))
		      (1+ i))))))
  (iter x 0))

(define (solve-1 lines)
  (let* ((wire-states (parse-wire-states lines))
	 (rules (parse-wire-rules lines)))
    (run rules wire-states)))

(define (add rules x y)
  (run rules (append (number->bits #\x x) (number->bits #\y y))))

(define ancestors
  ;; Returns all ancestors of `gate`, not including the gate itself.
  (with-caching
   (lambda (rules gate)
     (define (foo g)
       (let ((rule (assoc-ref rules g)))
	 (match rule
	   ((op x y)
	    (append (list g)
		    (foo x)
		    (foo y)))
	   (#f
	    (list g)))))
     (uniq (cdr (foo gate))))))

(define (get-set-bits x)
  (define (iter rest i acc)
    (cond ((<= rest 0)
	   acc)
	  ((= 0 (modulo rest 2))
	   (iter (floor (/ rest 2))
		 (1+ i)
		 acc))
	  (else
	   (iter (floor (/ rest 2))
		 (1+ i)
		 (cons i acc)))))
  (iter x 0 '()))

(define (wrong-bits actual expected)
  (define (iter actual expected i)
    (if (= 0 actual expected)
	'()
	(let ((rest (iter (floor (/ actual 2))
			  (floor (/ expected 2))
			  (1+ i))))
	  (if (= (modulo actual 2)
	         (modulo expected 2))
	      rest
	      (cons (gate-label "z" i)
		    rest)))))

  (iter actual expected 0))

(define (probe rules i j)
  ;; Checks whether (add 2^i 2^j) = 2^i + 2^j. If not, then we know
  ;; that at least one of the descendants of x_i or y_j was
  ;; swapped. Indeed, all other gates' ancestors take the value 0, so
  ;; swapping any 2 of those doesn't change the output. Returns a list
  ;; of list - each sublist is a set of gates, at least one of which
  ;; was swapped.
  (let* ((x (expt 2 i))
	 (y (expt 2 j))
	 (output (add rules x y))
	 (x-label (gate-label "x" i))
	 (y-label (gate-label "y" j)))
    (if (= output (+ x y))
	'()
	(cons (sort (uniq (append (descendants rules x-label)
				  (descendants rules y-label)))
		    string<)
	      (map (cut ancestors rules <>)
		   (wrong-bits output (+ x y)))))))

(define (probe-all rules)
  (define (pair->probe-result pair)
    (let ((result (apply probe rules pair)))
      (if (null? result)
	  #f
	  result)))

  (map (lambda (p)
	 (format #t "~a -> ~a~%" (car p) (cadr p))
	 (cdr p))
       (filter cdr
	       (map touch
		    (map (lambda (p) (future (cons p (pair->probe-result p))))
			 (cross-product (iota 45) (iota 45)))))))

(define (gate-wrong-probs suspects)
  (let ((probs (hash-table.new)))
    (for-each (lambda (xs)
		(for-each (lambda (x)
			    (hash-table.set! probs x (+ (or (hash-table.get probs x) 0)
							(exact->inexact (/ 1 (length xs))))))
			  xs))
	      (map car suspects))
    (sort (hash-table->alist probs)
	  (lambda (x y)
	    (> (cdr x) (cdr y))))))

(define my-input (call-with-input-file "2024/input24.txt" read-lines))

(test-begin "probe")
(let ((r (parse-wire-rules my-input)))
  (test-equal "gate-6"
    (list (sort (uniq (append (descendants r "x06")
			      (descendants r "y06")))
		string<)
	  (ancestors r "z07")
	  (ancestors r "z08"))
    (probe r 6 6)))
(test-end "probe")

#;(define (swap-candidates rules gates)
(define (candidates-starting-with acc rest-gates)
(cond ((= (length acc) 8)
(list acc))
((= 0 (modulo (length acc) 2))
(let* ((unused-gates (filter (lambda (g) (not (member g acc))) gates))
(candidates (filter (lambda (g) (not (rela)
TODO TODO						       
(if (null? gates)
'()
(append (candidates-starting-with (take gates 1) (cdr gates))
(swap-candidates rules (cdr gates))))))))))))))


#;(define (solve-2 lines)
(let* ((r (parse-wire-rules lines))
;; bad-gates' first element is the gate that is most likely to be wrong
(bad-gates (map car (gate-wrong-probs (probe-all r))))
(cs (swap-candidates r bad-gates)))
(find (lambda (c)
(probably-correct? (apply-swaps c r)))
cs)))

(define (get-all-swaps rules x y)
  ;; Assuming that rules causes x+y to return the wrong answer,
  ;; returns all possible swaps that could fix the situation. More
  ;; specifically, this is all swaps from a descendant of the set
  ;; input gates of x0 and x1 to any output gates in rules. This does
  ;; not check for creating loops.
  ;;
  ;; TODO: is it really all output gates that are worth swapping with?
  (let* ((inputs0 (map (cut gate-label "x" <>) (get-set-bits x)))
	 (inputs1 (map (cut gate-label "y" <>) (get-set-bits y)))
	 (bad-gates (sort (uniq (flat-map (cut descendants rules <>)
					  (append inputs0 inputs1)))
			  string<)))
    (->> (cross-product bad-gates (output-gates rules))
	 (filter (lambda (xs) (not (equal? (car xs) (cadr xs)))))
	 (map (lambda (xs) (sort xs string<)))
	 (uniq))))

(test-begin "get-all-swaps")
(let ((r (parse-wire-rules (list
			    ""
			    "x00 AND y00 -> z05"
			    "x01 AND y01 -> z02"
			    "x02 AND y02 -> z01"))))
  (test-equal "swap-x00-y00"
    '(("z02" "z05")
      ("z01" "z05"))
    (get-all-swaps r 1 1)))
(test-end "get-all-swaps")

(define (apply-swap rules swap)
  (map (lambda (r)
	 (cond ((equal? (car r) (car swap))
		(cons (cadr swap) (cdr r)))
	       ((equal? (car r) (cadr swap))
		(cons (car swap) (cdr r)))
	       (else r)))
       rules))

(test-begin "apply-swap")
(let ((r (parse-wire-rules (list
			    ""
			    "x00 AND y00 -> z05"
			    "x01 AND y01 -> z02"
			    "x02 AND y02 -> z01"))))
  (test-equal "swap-x00-y00"
    (parse-wire-rules (list ""
			    "x00 AND y00 -> z05"
			    "x01 AND y01 -> z01"
			    "x02 AND y02 -> z02"))
    (apply-swap r '("z01" "z02"))))
(test-end)


(define (apply-swaps rules swaps)
  (fold-left apply-swap rules swaps))

(test-begin "apply-swaps")
(let ((r (parse-wire-rules (list
			    ""
			    "x00 AND y00 -> z05"
			    "x01 AND y01 -> z02"
			    "x02 AND y02 -> z01"))))
  (test-equal "swap-x00-y00"
    (parse-wire-rules (list ""
			    "x00 AND y00 -> z05"
			    "x01 AND y01 -> z01"
			    "x02 AND y02 -> z02"))
    (apply-swaps r '(("z01" "z02")))))
(test-end)

(define (not-already-swapped c s)
  ;; candidates (a list of swaps), a new swap
  (let ((flat-cs (apply append c)))
    (and (not (member (car s) flat-cs))
	 (not (member (cadr s) flat-cs)))))

(define (no-loop rules s)
  ;; Checks that applying the swap s to rules does not introduce a loop.
  ;;
  ;; rules (possibly after some existing swaps were applied), a new swap
  (and (not (member (car s) (ancestors rules (cadr s))))
       (not (member (cadr s) (ancestors rules (car s))))))

(define (passes-test rules test)
  #;(format #t "Check passes ~a ?" test)
  (let* ((x (car test))
	 (y (cadr test))
	 (expected (caddr test))
	 (actual (add rules x y)))
    #;(format #t "~a!~%" (if (= actual expected) "Pass" "Fail"))
    (= actual expected)))

(define (pipeline-dbg . args)
  (lambda (x)
    (apply format #t args)
    x))

(define (next-candidates-one rules test passed-tests c)
  (let* ((x0 (car test))
	 (x1 (cadr test))
	 (swapped-rules (apply-swaps rules c)))
    (cond ((passes-test swapped-rules test)
	   (list c))
	  ;; test failed
	  ((= (length c) 4)
	   '())
	  ;; room to add another transposition
	  (else
	   (format #t "Generating swaps...~%")
	   (->> (get-all-swaps swapped-rules x0 x1)
		((pipeline-dbg "Not already swapped...~%"))
		(filter (cut not-already-swapped c <>))
		((pipeline-dbg "No loops...~%"))
		(filter (cut no-loop swapped-rules <>))
		((lambda (x) (format #t "1,") x))
		(filter (lambda (s) passes-test (apply-swap swapped-rules s) test))
		((lambda (x) (format #t "Checking previous tests still pass, (~a*~a=~a to eval)~%" (length x) (length passed-tests) (* (length x) (length passed-tests))) x))
		(parallel-filter (lambda (s)
				   #;(format #t "Try new swap: ~a~%" s)
				   (every (cut passes-test (apply-swap swapped-rules s) <>)
					  passed-tests)))
		((lambda (x) (format #t "3!~%") x))
		(map (cut cons <> c)))))))


(define (next-candidates rules test passed-tests candidates)
  (flat-map (lambda (c) (next-candidates-one rules test passed-tests c)) candidates))

(define (test->string t)
  (format #f "~a+~a=~a"
	  (car t)
	  (cadr t)
	  (caddr t)))

(define (run-all-tests-iter rules rest-tests passed-tests candidate-swaps)
  (cond ((null? candidate-swaps)
	 (error "No more candidate swaps!"))
	((null? rest-tests)
	 candidate-swaps)
	(else
	 (format #t
		 "~a/~a passed, ~a candidates. Testing ~a~%"
		 (length passed-tests)
		 (+ (length passed-tests)
		    (length rest-tests))
		 (length candidate-swaps)
		 (test->string (car rest-tests)))
	 (run-all-tests-iter rules
			     (cdr rest-tests)
			     (cons (car rest-tests)
				   passed-tests)
			     (next-candidates rules (car rest-tests) passed-tests candidate-swaps)))))

(define (build-test-cases)
  (->> (cross-product (iota 45) (iota 45))
       (map (lambda (xs)
	      (list (expt 2 (car xs))
		    (expt 2 (cadr xs))
		    (+ (expt 2 (car xs))
		       (expt 2 (cadr xs))))))
       ((cut sort <> (lambda (xs ys) (< (caddr xs) (caddr ys)))))))

(define (build-test-cases-2)
  (->> (iota 45)
       (map (cut expt 2 <>))
       (flat-map (lambda (x) `((,x 0 ,x) (0 ,x ,x))))
       ((cut sort <> (lambda (xs ys) (< (caddr xs) (caddr ys)))))))

(define (solve-2 lines)
  (let* ((r (parse-wire-rules lines))
	 (cs (run-all-tests-iter r
				 (build-test-cases-2)
				 '()
				 '(()))))
    cs))

(define-immutable-record-type <topology>
  (make-topology gate output-histogram input-histogram)
  topology?
  (gate topology-gate)
  (output-histogram topology-output-histogram)
  (input-histogram topology-input-histogram))

(define (terminals rules gate)
  (list gate
        (sort (filter xy-input-gate? (cons gate (ancestors rules gate))) string<)
	(sort (filter z-output-gate? (cons gate (descendants rules gate))) string<)))

(define (histogram->keys hist)
  (map car hist))

(define (average-histograms . hists)
  (let ((n (length hists))
	(keys (uniq (flat-map histogram->keys hists))))
    (map (lambda (k)
	   (let ((values (map (lambda (hist) (or (assoc-ref hist k) 0)) hists)))
	     (cons k (/ (apply + values) n))))
	 keys)))

(define (descendant-histogram rules gate)
  (if (z-output-gate? gate)
      (list (cons (gate-number gate) 1))
      (->> (children rules gate)
	   (map (cut descendant-histogram rules <>))
	   (apply average-histograms)
	   ((cut sort <> (lambda (kv0 kv1) (> (cdr kv0) (cdr kv1))))))))

(define (ancestor-histogram rules gate)
  (if (xy-input-gate? gate)
      (list (cons (gate-number gate) 1))
      (apply average-histograms (map (cut ancestor-histogram rules <>)
				     (parents rules gate)))))

(define (gate->topology rules gate)
  (make-topology gate
		 (descendant-histogram rules gate)
		 (ancestor-histogram rules gate)))

(define (dot-product h0 h1)
  (let ((keys (uniq (flat-map histogram->keys (list h0 h1)))))
    (->> keys
	 (map (lambda (k) (list (or (assoc-ref h0 k) 0)
				(or (assoc-ref h1 k) 0))))
	 (map (cut apply * <>))
	 (apply +))))

(define (suspicious-terminals? io)
  ;; True if max z-gate has a higher number than the max x/y-gate.
  (< (apply max (map gate-number (caddr io)))
     (apply max (map gate-number (cadr io)))))

(define (solve-2-2 lines)
  (let* ((r (parse-wire-rules lines))
	 (ios (map (cut terminals r <>) (all-gates r))))
    (->> ios
	 (filter suspicious-terminals?)
	 (for-each (lambda (xs)
		     (apply format #t "~a: ~a -> ~a~%" xs))
		   ))))

(define (in-out-similarity top)
  (dot-product (topology-output-histogram top)
	       (topology-input-histogram top)))

(define (map-keys f alist)
  (map (lambda (kv)
	 (cons (f (car kv))
	       (cdr kv)))
       alist))

(define (shifted-in-out-similarity top)
  (dot-product (topology-output-histogram top)
	       (map-keys 1+ (topology-input-histogram top))))

(define (sort-by-number key-fn)
  (lambda (k0 k1)
    (> (key-fn k0) (key-fn k1))))

(define (solve-2-3 lines)
  (define (similarity in-h out-h)
    (+ (dot-product (topology-output-histogram out-h)
		    (topology-input-histogram in-h))
       (dot-product (topology-output-histogram out-h)
		    (map-keys 1+ (topology-input-histogram in-h)))))
  (define (swap-improvement pair)
    (let ((h0 (car pair))
	  (h1 (cadr pair)))
      (- (+ (similarity h0 h1)
	    (similarity h1 h0))
	 (+ (similarity h0 h0)
	    (similarity h1 h1)))))
  (let* ((r (parse-wire-rules lines))
	 (hists (map (cut gate->topology r <>) (all-gates r)))
	 (pairs (cross-product hists hists)))
    (->> (filter (lambda (p) (< 0 (swap-improvement p))) pairs)
	 (map (lambda (p) (list (car p)
				(cadr p)
				(swap-improvement p))))
	 ((cut sort <> (lambda (x0 x1) (> (caddr x0) (caddr x1)))))
	 ((cut take <> 10))
	 (for-each (lambda (p)
		     (format #t "~a <-> ~a~%In: ~a~%Out: ~a~%~%In: ~a~%Out: ~a~%~%"
			     (topology-gate (car p))
			     (topology-gate (cadr p))
			     (topology-input-histogram (car p))
			     (topology-output-histogram (car p))
			     (topology-input-histogram (cadr p))
			     (topology-output-histogram (cadr p))))))))

#;(->> hists
((cut sort <> (sort-by-number (lambda (top) (+ (in-out-similarity top)
(shifted-in-out-similarity top))))))
(for-each (lambda (top)
(format #t "~a: ~a / ~a%~%~a~%~a~%~%"
(topology-gate top)
(* 100 (exact->inexact (in-out-similarity top)))
(* 100 (exact->inexact (shifted-in-out-similarity top)))
(topology-input-histogram top)
(topology-output-histogram top)))))


(define (expected-algebraic-expression z-gate)
  (define (build-carry i)
    ;; Expression for the number carried out of bit i (added to bit
    ;; (1+ i)).
    (if (= i 0)
	(list 'AND "x00" "y00")
	`(OR (AND ,(gate-label "x" i)
		  ,(gate-label "y" i))
	     (AND ,(gate-label "x" i)
		  ,(build-carry (1- i)))
	     (AND ,(gate-label "y" i)
		  ,(build-carry (1- i))))))
  
  (define (build i)
    (if (= i 0)
	(list 'XOR "x00" "y00")
	(list 'XOR
	      (gate-label "x" i)
	      (gate-label "y" i)
	      (build-carry (1- i)))))
  (build (gate-number z-gate)))

(define (carry-op? expr)
  ;; Checks whether the generated label represents the carry op from a
  ;; previous bit.
  (and (list? expr)
       (= (length expr) 2)
       (equal? (car expr) 'carry)))

(define (flattened-parents rules gate)
  (define (foo op g)
    (let ((r (find (rule-has-output? g) rules)))
      (if (and r (equal? op (rule-operator r)))
	  ;; This rule has the same operator as the target, so flatten
	  ;; it into its parents.
	  (flat-map (cut foo op <>) (parents rules (rule->output r)))
	  ;; This rule is an input or has a different operator, so
	  ;; include it as a leaf node.
	  (list (label-gate rules g)))))
  
  (let* ((r (find (rule-has-output? gate) rules))
	 (op (rule-operator r)))
    (foo op gate)))

(define (naive-label-gate rules gate)
  (let ((rule (find (rule-has-output? gate) rules)))
    (normalise-expression
     (if rule
	 (cons* (rule-operator rule)
		(label-gates rules (parents rules gate)))
	 ;; It's an input gate
	 gate))))

(define (matches? expr preds)
  (cond ((not (list? expr))
	 #f)
	((null? expr)
	 (null? preds))
	;; expr is non-empty
	((null? preds)
	 #f)
	((procedure? (car preds))
	 (and ((car preds) (car expr))
	      (matches? (cdr expr) (cdr preds))))
	((and (list? (car expr)) (list? (car preds)))
	 (and (matches? (car expr) (car preds))
	      (matches? (cdr expr) (cdr preds))))
	(else
	 (and (equal? (car preds) (car expr))
	      (matches? (cdr expr) (cdr preds))))))

(test-begin "matches")
(test-assert "recursive match"
  (matches? '((OR 1))
	    `((,symbol? ,number?))))
(test-end "matches")


(define (deep-list-ref xs . is)
  (fold-left (lambda (ys i) (list-ref ys i))
	     xs
	     is))

(define label-gate
  (with-caching
   (lambda (rules gate)
     (let ((expr (naive-label-gate rules gate)))
       (cond ((equal? expr '(XOR "x00" "y00"))
	      "z00")
	     ((equal? expr '(AND "x00" "y00"))
	      '(carry 0))
	     ((and (matches? expr `(XOR ,x-gate? ,y-gate? ,carry-op?))
		   (equal? (gate-number (list-ref expr 1))
			   (gate-number (list-ref expr 2))
			   (1+ (cadr (list-ref expr 3)))))
	      (gate-label "z" (gate-number (list-ref expr 1))))
	     ((and (matches? expr `(OR (AND ,x-gate? ,y-gate?)
				       (AND ,x-gate? ,carry-op?)
				       (AND ,y-gate? ,carry-op?)))
		   (equal? (gate-number (deep-list-ref expr 1 1))
			   (gate-number (deep-list-ref expr 1 2))
			   (gate-number (deep-list-ref expr 2 1))
			   (1+ (cadr (deep-list-ref expr 2 2)))
			   (gate-number (deep-list-ref expr 3 1))
			   (1+ (cadr (deep-list-ref expr 3 2)))))
	      `(carry ,(gate-number (deep-list-ref expr 1 1))))
	     (else expr))))))

(define (compare-by comparators e0 e1)
  (if (null? comparators)
      (error "No comparators left for" e0 e1)
      (let ((pred? (caar comparators))
	    (compare (cdar comparators)))
	(cond ((and (pred? e0) (pred? e1))
	       (compare e0 e1))
	      ((pred? e0)
	       #t)
	      ((pred? e1)
	       #f)
	      (else (compare-by (cdr comparators) e0 e1))))))

(define (comparing-strings keyfn)
  (lambda (e0 e1)
    (string< (keyfn e0) (keyfn e1))))

(define (comparing-numbers keyfn)
  (lambda (e0 e1)
    (< (keyfn e0) (keyfn e1))))

(define (comparing-lists less)
  (lambda (xs ys)
    (cond ((and (null? xs) (null? ys))
	   #f)
	  ((null? xs)
	   #t)
	  ((null? ys)
	   #f)
	  ((equal? (car xs) (car ys))
	   ((comparing-lists less) (cdr xs) (cdr ys)))
	  (else (less (car xs) (car ys))))))

(define (expression< e0 e1)
  (compare-by (list (cons string? string<)
		    (cons carry-op? (comparing-strings (compose symbol->string car)))
		    (cons list? (comparing-lists expression<)))
	      e0 e1))

(define (normalise-expression xs)
  (define (has-op? op)
    (lambda (e)
      (and (list? e)
	   (not (null? e))
	   (equal? op (car e)))))
  
  (if (or (string? xs) (carry-op? xs))
      xs
      (let* ((same-op-subexp (filter (has-op? (car xs)) xs))
	     (rest (filter (lambda (e) (not (member e same-op-subexp)))
			   (cdr xs))))
	(cons (car xs)
	      (sort (map normalise-expression (append rest
						      (flat-map cdr same-op-subexp)))
		    expression<)))))

(test-begin "normalise-expression")
(test-equal "string"
  "x00"
  (normalise-expression "x00"))
(test-equal "list"
  '(XOR "x00" "y00" (carry 0) (AND "w" "z"))
  (normalise-expression '(XOR "y00" (carry 0) (AND "z" "w") "x00")))
(test-equal "flattens operands"
  '(XOR "x" "y" "z")
  (normalise-expression '(XOR (XOR "y" "z") "x")))
(test-end)

(define (label-gates rules gates)
  (map (cut label-gate rules <>) gates))

(test-begin "label-gate")
(let ((r (parse-wire-rules (list
			    ""
			    "x00 XOR y00 -> w00"
			    "y00 XOR x00 -> w00'"
			    
			    "x00 AND y00 -> c00"
			    "x01 XOR y01 -> w01-0"
			    "w01-0 XOR c00 -> w01"

			    "x01 AND y01 -> c01-0"
			    "x01 AND c00 -> c01-1"
			    "y01 AND c00 -> c01-2"
			    "c01-0 OR c01-2 -> c01-3"
			    "c01-3 OR c01-1 -> c01"
			    "c01 XOR y02 -> w02-0"
			    "x01 XOR w02-0 -> w02"))))
  (test-equal "input gate"
    "x00"
    (label-gate r "x00"))
  (test-equal "output gate z00"
    "z00"
    (label-gate r "w00"))
  (test-equal "output gate z00 commutative"
    "z00"
    (label-gate r "w00'"))
  (test-equal "carry 00"
    '(carry 0)
    (label-gate r "c00"))
  (test-equal "output gate z01"
    "z01"
    (label-gate r "w01"))
  (test-equal "intermediate-gate"
    '(XOR "x01" "y01")
    (label-gate r "w01-0"))
  (test-equal "carry 01"
    '(carry 1)
    (label-gate r "c01")))
(test-end)  

(define (solve-2-4 lines)
  (let* ((r (parse-wire-rules lines))
	 (labels (label-gate r "x01")))
    labels))

(define (layers rules gate)
  (let ((out-layer (->> gate
			(descendants rules)
			((lambda (xs) (cons gate xs)))
			(filter z-output-gate?)
			(map gate-number)
			(apply min)))
	(in-layer (->> gate
		       (ancestors rules)
		       ((lambda (xs) (cons gate xs)))
		       (filter xy-input-gate?)
		       (map gate-number)
		       (apply max))))
    (list in-layer out-layer)))

;; ---------------------

(define (determined-by? rules known-gates g)
  (let* ((ps (parents rules g))
	 (unknowns (filter (lambda (p) (not (member p known-gates))) ps)))
    (cond ((null? ps)
	   ;; We're an input gate that's not in known-gates
	   #f)
	  ((null? unknowns)
	   #t)	  
	  (else
	   (every (cut determined-by? rules known-gates <>)
		  unknowns)))))

(define (find-gates-matching rules known-gates cases)
  (fold-left (lambda (gates c)
	       (filter (lambda (g)
			 (equal? (compute-output-value
				  (hash-set.new)
				  rules
				  (map cons known-gates (cdr c))
				  g)
				 (car c)))
		       gates))
	     (filter (cut determined-by? rules known-gates <>) (all-gates rules))
	     cases))
					 
(define (find-carry-gates rules i)
  ;; i is the index of the layer *out of which* the carry is happening.
  (if (= i 0)
      (find-gates-matching rules
			   '("x00" "y00")
			   '((#f #f #f)
			     (#f #f #t)
			     (#f #t #f)
			     (#t #t #t)))
      (let ((prev-carries (find-carry-gates rules (1- i))))
	(find-gates-matching rules
			     (cons* (gate-label "x" i)
				    (gate-label "y" i)
				    prev-carries)
			     (list (cons* #f #f #f (map (const #f) prev-carries))
			           (cons* #f #f #f (map (const #t) prev-carries))
				   (cons* #f #f #t (map (const #f) prev-carries))
				   (cons* #f #t #f (map (const #f) prev-carries))

				   (cons* #t #f #t (map (const #t) prev-carries))
				   (cons* #t #t #f (map (const #t) prev-carries))
				   (cons* #t #t #t (map (const #f) prev-carries))
				   (cons* #t #t #t (map (const #t) prev-carries)))))))

(define (find-output-gate rules i)
  (if (= i 0)
      (find-gates-matching rules
			   '("x00" "y00")
			   '((#f #f #f)
			     (#t #f #t)
			     (#t #t #f)
			     (#f #t #t)))
      (let ((prev-carries (find-carry-gates rules (1- i))))
	(find-gates-matching rules
			     (cons* (gate-label "x" i)
				    (gate-label "y" i)
				    prev-carries)
			     (list (cons* #f #f #f (map (const #f) prev-carries))
			           (cons* #f #f #t (map (const #t) prev-carries))
				   (cons* #f #t #f (map (const #t) prev-carries))
				   (cons* #f #t #t (map (const #f) prev-carries))

				   (cons* #t #f #f (map (const #t) prev-carries))
				   (cons* #t #f #t (map (const #f) prev-carries))
				   (cons* #t #t #f (map (const #f) prev-carries))
				   (cons* #t #t #t (map (const #t) prev-carries)))))))

(define (brute-force old-rules swaps max-out i)
  (define (iter new-rules candidates)
    (let* ((candidates-0 (filter (cut no-loop new-rules <>) candidates))
	   (candidates-1 (parallel-filter (progress-bar
					   (length candidates-0)
					   (lambda (c)
					    (not (null? (find-output-gate (apply-swap new-rules c) i)))))
				 candidates-0))
	   (candidate-2 (find (lambda (c) (swap-missing-outputs old-rules
								(cons c swaps)
								max-out))
			      candidates-1)))
      (cons candidate-2 swaps)))
  
  (let* ((new-rules (apply-swaps old-rules swaps))
	 (correct-gates (uniq (flat-map
			       (lambda (i)
				 (cons (gate-label "z" i)
				       (ancestors new-rules (gate-label "z" i))))
			       (iota i))))
	 (last-carry-gates (find-carry-gates new-rules (1- i)))
	 (bad-gates (filter (cut determined-by? new-rules (cons* (gate-label "x" i)
								 (gate-label "y" i)
								 last-carry-gates)
				 <>)
			    (all-gates new-rules)))
	 (other-gates (sort (filter (lambda (g) (not (member g correct-gates)))
				    (all-gates new-rules))
			    string<)))
    ;(format #t "chv in bad gates? ~a / ~a~%" (length (member "chv" bad-gates)) (length bad-gates))
    ;(format #t "vvw in other-gates? ~a / ~a~%" (length (member "vvw" other-gates)) (length other-gates))
    ;(iter new-rules '(("chv" "vvw")))
    (or (iter new-rules (cross-product bad-gates other-gates))
	;; Shame-faced desire to have a more general solution, even if
	;; this is going to be super slow.
	(iter new-rules (cross-product other-gates other-gates)))))

(define (find-first-bad-output old-rules swaps max-out)
  (let ((new-rules (apply-swaps old-rules swaps)))
    (define (iter i)
      (if (< max-out i)
	  #f
	  (let* ((expected-z (gate-label "z" i))
		 (actual-zs (find-output-gate new-rules i)))
	    (if (member expected-z actual-zs)
		(iter (1+ i))
		expected-z))))

    (iter 0)))

(define (swap-missing-outputs old-rules swaps max-out)
  (let* ((new-rules (apply-swaps old-rules swaps))
	 (expected-z (find-first-bad-output old-rules swaps max-out))
	 (actual-zs (if expected-z
			(find-output-gate new-rules (gate-number expected-z))
			#f)))
    (format #t "With ~a can't find ~a~%" swaps expected-z)
    (cond ((not expected-z)
	   swaps)
	  ((and (equal? max-out (gate-number expected-z))
		(member expected-z (find-carry-gates new-rules (1- max-out))))
	   ;; The last output gate actually behaves like a carry gate,
	   ;; because there are no inputs in that layer. (i.e. z45 is
	   ;; the carry from x44 and y44).
	   swaps)
	  ((null? actual-zs)
	   (if (< (length swaps) 4)
	       (brute-force old-rules swaps max-out (gate-number expected-z))
	       #f))
	  (else (swap-missing-outputs old-rules
				      (cons (list expected-z (car actual-zs))
					    swaps)
				      max-out)))))
	   
(define (solve-4 my-input)
  (let* ((r (parse-wire-rules my-input))
	 (max-out (apply max (map gate-number (filter z-output-gate? (all-gates r))))))
    (->> (swap-missing-outputs r '() max-out)
	 (apply append)
	 ((cut sort <> string<))
	 ((cut string-join <> ",")))))
     

#;(display (solve-1 my-input))
#;(newline)
(display (solve-4 my-input))
(newline)
