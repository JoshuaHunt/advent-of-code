(use-modules (utils)
	     (srfi srfi-26))

(define (machine.new a-vec b-vec prize)
  (list a-vec b-vec prize))

(define (mach.a-cost m) (car (car m)))
(define (mach.a-dx m) (cadr (car m)))
(define (mach.a-dy m) (caddr (car m)))
(define (mach.b-cost m) (car (cadr m)))
(define (mach.b-dx m) (cadr (cadr m)))
(define (mach.b-dy m) (caddr (cadr m)))
(define (mach.prize-x m) (car (caddr m)))
(define (mach.prize-y m) (cadr (caddr m)))

(define (mach.a-vec m) (cdr (car m)))
(define (mach.b-vec m) (cdr (cadr m)))
(define (mach.prize m) (caddr m))

(define (min-tokens m)
  ;; machine -> min tokens or #f
  #;(define (normalise machine)
    ;; machine -> machine
    ;; Make A.dX the largest increment.
  (cond (())))

  (define (cost-with a-presses)
    ;; Cost to solve the machine pressing button A a times, or #f
    (let* ((remaining-x (- (mach.prize-x m)
			   (* (mach.a-dx m) a-presses)))
	   (b-presses (/ remaining-x (mach.b-dx m)))
	   (y-result (+ (* a-presses (mach.a-dy m))
			(* b-presses (mach.b-dy m)))))
      (if (and (integer? b-presses)
	       (<= 0 b-presses 100)
	       (<= 0 a-presses 100)
	       (equal? (mach.prize-y m) y-result))
	  (+ (* (mach.a-cost m) a-presses)
	     (* (mach.b-cost m) b-presses))
	  #f)))

  (define (iter a old-min)
    (cond ((< 100 a)
	   old-min)
	  ((not old-min)
	   (iter (+ a 1) (cost-with a)))
	  (else
	   (let ((new-cost (cost-with a)))
	     (if new-cost
		 (iter (+ a 1) (min old-min new-cost))
		 (iter (+ a 1) old-min))))))

  (iter 0 #f))

(define (parse-vec line)
  ;; 'Button A: X+94, Y+34'
  (let* ((foo (string-split line #\+))
	 (x (string->number (car (string-split (cadr foo) #\,))))
	 (y (string->number (caddr foo))))
    (list x y)))

(define (parse-prize line)
  ;; 'Prize: X=8400, Y=5400'
  (let* ((foo (string-split line #\=))
	 (x (string->number (car (string-split (cadr foo) #\,))))
	 (y (string->number (caddr foo))))
    (list x y)))

(define (parse-machines lines offset)
  (if (null? lines)
      '()
      (let ((a-vec (parse-vec (cadr lines)))
	    (b-vec (parse-vec (caddr lines)))
	    (prize-pt (point.add (parse-prize (cadddr lines))
				 (list offset offset))))
	(cons (machine.new (cons 3 a-vec) (cons 1 b-vec) prize-pt)
	      (parse-machines (list-tail lines 4) offset)))))

(define (colinear? v1 v2)
  (let ((a (/ (car v1) (car v2)))
	(b (/ (cadr v1) (cadr v2))))
    (equal? a b)))

(define (columns->matrix e1 e2)
  (list (list (car e1) (car e2))
	(list (cadr e1) (cadr e2))))

(define (invert m)
  (let ((det (- (* (car (car m)) (cadr (cadr m)))
		(* (car (cadr m)) (cadr (car m))))))
    (list (list (/ (cadr (cadr m)) det)
		(/ (- (cadr (car m))) det))
	  (list (/ (- (car (cadr m))) det)
		(/ (car (car m)) det)))))

(define (dot-product v1 v2)
  (apply + (map * v1 v2)))

(define (matrix-vec-mult mat vec)
  (map (cut dot-product <> vec) mat))

(define (change-basis-to e1 e2 v)
  (let ((matrix (columns->matrix e1 e2)))
    (matrix-vec-mult (invert matrix) v)))

(define (all pred? xs)
  (if (null? xs)
      #t
      (and (pred? (car xs))
	   (all pred? (cdr xs)))))

(define (vec-cost m vec)
  (+ (* (mach.a-cost m) (car vec))
     (* (mach.b-cost m) (cadr vec))))

(define (vector-cost vec goal cost)
  (let ((coeff (/ (car goal) (car vec))))
    (if (and (integer? coeff)
	     (equal? (* coeff (cadr vec) (cadr goal))))
	(* cost coeff)
	#f)))

(define (colinear-cost m)
  (if (< (/ (mach.a-dx m) (mach.a-cost m))
	 (/ (mach.b-dx m) (mach.b-cost m)))
      (vector-cost (mach.a-vec m) (mach.prize m) (mach.a-cost m))
      (vector-cost (mach.b-vec m) (mach.prize m) (mach.b-cost m))))

(define (min-tokens-2 m)
  (if (colinear? (mach.a-vec m) (mach.b-vec m))
      (colinear-cost m)
      (let ((exact-coords (change-basis-to (mach.a-vec m) (mach.b-vec m) (mach.prize m) )))
	(if (all integer? exact-coords)
	    (vec-cost m exact-coords)
	    #f))))

(define my-input (call-with-input-file "2024/input13.txt" read-lines))

(define (solve-1 lines)
  (let ((machines (parse-machines (cons "" lines) 0)))
    (apply + (map (lambda (x) (if x x 0)) (map min-tokens-2 machines)))))

(define (solve-2 lines)
  (let ((machines (parse-machines (cons "" lines) 10000000000000)))
    (apply + (map (lambda (x) (if x x 0)) (map min-tokens-2 machines)))))
	
(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
