(use-modules (utils)
	     (srfi srfi-1)
	     (srfi srfi-26))

(define my-input (call-with-input-file "2024/input10.txt" read-lines))

(define (parse-grid lines)
  (grid.new (map (compose (lambda (r) (map (compose string->number string) r)) string->list) lines)))

(define (trailhead-score start grid only-count-unique-trails)
  (define (dfs x)
    (let* ((x-height (grid.get grid x))
	   (neighbours (->> orthogonal-dirs
			    (map dir->vector)
			    (map (cut point.add x <>))
			    (map (lambda (y) (list y (grid.get grid y))))
			    (filter (lambda (y) (equal? (cadr y) (+ 1 x-height))))
			    (map car))))
      (if (equal? x-height 9)
	  (list x)
	  (flat-map dfs neighbours))))

  (if only-count-unique-trails
      (length (delete-duplicates (dfs start)))
      (length (dfs start))))
  

(define (solve-1 lines)
  (let* ((grid (parse-grid lines))
	 (trailheads (grid.filter grid (cut equal? 0 <>))))
    (apply + (map (cut trailhead-score <> grid #t) trailheads))))

(define (solve-2 lines)
  (let* ((grid (parse-grid lines))
	 (trailheads (grid.filter grid (cut equal? 0 <>))))
    (apply + (map (cut trailhead-score <> grid #f) trailheads))))

(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
	 
