(use-modules (utils))

(define (parse-test line)
  (let* ((parts (string-split line #\:))
	 (target (string->number (car parts)))
	 (operands (map string->number (string-split (substring (cadr parts) 1) #\space))))
    (apply list target operands)))

(define (state.new operands)
  (list (car operands) (cdr operands)))

(define (state.operands s)
  (cadr s))

(define (state.concat s)
  (let ((ops (state.operands s)))
    (list (string->number (string-append (number->string (car s))
					 (number->string (car ops))))
	  (cdr ops))))

(define (state.op s op)
  (let ((operands (state.operands s)))
    (list (op (car s) (car operands))	  
	  (cdr operands))))
  

(define (state.equal? s goal)
  (equal? (car s) goal))

(define (state.current-lower-bound s)
  (car s))

(define (is-possible-2? testcase)
		  
  (define (iter goal state)
    (cond ((null? (state.operands state))
	   (state.equal? state goal))
	  ((> (state.current-lower-bound state) goal)
	   #f)
	  (else (or (iter goal (state.concat state))
		    (iter goal (state.op state *))
		    (iter goal (state.op state +))))))

  (iter (car testcase) (state.new (cdr testcase))))

(define (is-possible-1? testcase)
  (define (iter goal state)
    (cond ((null? (state.operands state))
	   (state.equal? state goal))
	  ((> (state.current-lower-bound state) goal)
	   #f)
	  (else (or (iter goal (state.op state *))
		    (iter goal (state.op state +))))))

  (iter (car testcase) (state.new (cdr testcase))))


(define my-input (call-with-input-file "2024/input07.txt" read-lines))

(define (solve-1 lines)
  (apply + (map car (filter is-possible-1? (map parse-test lines)))))

(define (solve-2 lines)
  (apply + (map car (filter is-possible-2? (map parse-test lines)))))

(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
