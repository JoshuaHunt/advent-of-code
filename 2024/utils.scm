(define-module (utils)
  ;; For read-line
  #:use-module (ice-9 rdelim)
  ;; For cut
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-1)
  ;; For current-processor-count
  #:use-module (ice-9 threads)
  ;; Futures
  #:use-module (ice-9 futures)
  #:export(->>))

(define-public (empty-string? s)
  (equal? s ""))

(define-public (read-lines port)
  (let ((line (read-line port)))
    (if (eof-object? line)
	'()
	(cons line (read-lines port)))))

(define (safe-car xs)
  (if (null? xs)
      #f
      (car xs)))

(define-public (or-fn . xs)
  (if (null? xs)
      #f
      (or (car xs)
	  (apply or-fn (cdr xs)))))

(define-public (and-fn . xs)
  (if (null? xs)
      #t
      (and (car xs)
	   (apply and-fn (cdr xs)))))

(define-public (all? pred? xs)
  (if (null? xs)
      #t
      (and (pred? (car xs))
	   (all? pred? (cdr xs)))))

(define-public (zip . xss)
  (if (apply or-fn (map null? xss))
      '()
      (cons (map car xss)
	    (apply zip (map cdr xss)))))

(define-public (repeat f n x)
  (if (<= n 0)
      x
      (repeat f (1- n) (f x))))

(define-public (windows n xs)
  (define (iter rest i acc result)
    (cond ((null? rest)
	   (reverse (cons (reverse acc) result)))
	  ((equal? i n)
	   (iter rest 0 '() (cons (reverse acc) result)))
	  (else
	   (iter (cdr rest) (1+ i) (cons (car rest) acc) result))))
  (iter xs 0 '() '()))

(define-public (sliding-windows n xs)
  (let ((window (take n xs)))
    (if (< (length window) n)
	'()
	(cons window
	      (sliding-windows n (cdr xs))))))

(define-public (flat-map f xs)
  (apply append (map f xs)))

(define-public (parallel-functor f xs)
  (let* ((num-blocks (current-processor-count))
	 (block-length (ceiling (/ (length xs) num-blocks)))
	 (blocks (windows block-length xs))
	 (mapped-blocks (map (lambda (b)
			       (future (f b)))
			     blocks)))
    (map touch mapped-blocks)))

(define-public (parallel-flat-map f xs)
  (apply append (parallel-functor (cut flat-map f <>) xs)))

(define-public (parallel-filter f xs)
  (apply append (parallel-functor (cut filter f <>) xs)))

(define-public (parallel-every pred? xs)
  (apply and-fn (parallel-functor (cut every pred? <>) xs)))

(define-public (fold-left f acc xs)
  (if (null? xs)
      acc
      (fold-left f (f acc (car xs)) (cdr xs))))

(define-public (take n xs)
  (if (or (<= n 0) (null? xs))
      '()
      (cons (car xs)
	    (take (1- n) (cdr xs)))))

(define-public (take-until-before pred? xs)
  (cond ((null? xs)
	 '())
	((pred? (car xs))
	 '())
	(else (cons (car xs)
		    (take-until-before pred? (cdr xs))))))

(define-public (drop-until-after pred? xs)
  (cond ((null? xs)
	 '())
	((pred? (car xs))
	 (cdr xs))
	(else (drop-until-after pred? (cdr xs)))))

(define-public (cross-product xs ys)
  (define (iter rest-x rest-y)
    (cond ((null? rest-y)
	   '())
	  ((null? rest-x)
	   (iter xs (cdr rest-y)))
	  (else
	   (cons (list (car rest-x) (car rest-y))
		 (iter (cdr rest-x) rest-y)))))
  (iter xs ys))

(define (clamp min max val)
  ;; Clamps val to lie in [min, max)
  (cond ((< val min)
	 (clamp min max (+ val (- max min))))
	((<= max val)
	 (clamp min max (- val (- max min))))
	(else val)))

(define-public (point.new x y)
  (list x y))
(define-public point.x car)
(define-public point.y cadr)
(define-public (point.add point vector)
  (map + point vector))
(define-public (point.minus point0 point1)
  (map - point0 point1))
(define-public (point.taxi-cab-distance p0 p1)
  (apply + (map abs (point.minus p0 p1))))

(define-public (vector.scale v n)
  (map (cut * n <>) v))
(define-public vector.add point.add)

(define-public (grid.new xss)
  (list->vector (map list->vector xss)))

(define-public (grid.with-size size fill)
  (define (list-with-size len)
    (if (<= len 0)
	'()
	(cons fill (list-with-size (- len 1)))))

  (grid.new (map (lambda (ignored) (list-with-size (car size)))
		 (list-with-size (cadr size)))))

(define-public (grid.copy g)
  (let ((new-grid (vector-copy g)))
    (define (iter y)
      (if (>= y (grid.height g))
	  new-grid
	  (begin
	    (vector-set! new-grid y (vector-copy (vector-ref g y)))
	    (iter (+ 1 y)))))
    (iter 0)))

(define-public (grid.get g point)
  (if (grid.is-inside? g point)
      (vector-ref (vector-ref g (point.y point)) (point.x point))
      #f))

(define-public (grid.set! g point value)
  (if (grid.is-inside? g point)
      (let ((row (vector-ref g (point.y point))))
	(vector-set! row (point.x point) value)
	g)
      (error
       point
       " out of range for grid with width "
       (grid.width g)
       " and height "
       (grid.height g))))


(define-public (grid.height grid)
  (vector-length grid))

(define-public (grid.width grid)
  (if (equal? (grid.height grid) 0)
      0
      (vector-length (vector-ref grid 0))))

(define-public (grid.points grid)
  ;; Returns the positions in the grid
  (define (iter x y result)
    (cond ((>= y (grid.height grid))
	   result)
	  ((>= x (grid.width grid))
	   (iter 0 (+ 1 y) result))
	  (else
	   (iter (+ 1 x)
		 y
		 (cons (point.new x y) result)))))
  (iter 0 0 '()))

(define-public (grid.filter grid pred?)
  ;; Returns the positions that match pred?
  (filter (lambda (p) (pred? (grid.get grid p)))
	  (grid.points grid)))

(define-public (grid.is-inside? g point)
  (and (< -1 (point.x point) (grid.width g))
       (< -1 (point.y point) (grid.height g))))

(define-public (grid->string g)
  (string-join (map (compose list->string vector->list)
		    (vector->list g))
	       "\n"))

(define-public (only-element xs)
  (if (not (equal? (length xs) 1))
      (error "Expected xs to have a single element but was " xs)
      (car xs)))

(define-public (hash-set.new)
  (make-hash-table))

(define-public (hash-set.of . xs)
  (let ((s (hash-set.new)))
    (for-each (cut hash-set.add! s <>) xs)
    s))

(define (hash-set.copy s)
  (list->hash-set (hash-set->list s)))

(define-public (hash-set.add s val)
  (let ((c (hash-set.copy s)))
    (hash-set.add! c val)))

(define-public (hash-set.add! s val)
  (hash-set! s val #t)
  s)

(define-public (hash-set.intersection xs ys)
  (define (iter rest acc)
    (cond ((null? rest)
	   acc)
	  ((hash-set.contains? ys (car rest))
	   (iter (cdr rest)
		 (hash-set.add acc (car rest))))
	  (else
	   (iter (cdr rest) acc))))

  (iter (hash-set->list xs) (hash-set.new)))
	

(define-public (hash-set.contains? s val)
  (hash-ref s val))

(define-public (hash-set.length s)
  (hash-count (const #t) s))

(define-public (list->hash-set xs)
  (fold-left (lambda (s x) (hash-set.add! s x)) (hash-set.new) xs))

(define-public (hash-set->list s)
  (hash-map->list (lambda (k v) k) s))

(define-public (hash-table.new)
  (make-hash-table))

(define-public hash-table.length hash-set.length)

(define-public (hash-table.set! t k v)
  (hash-set! t k v)
  t)

(define-public (hash-table.map-value! t k f)
  (hash-table.set! t k (f (hash-table.get t k))))

(define-public (hash-table.get t k)
  (hash-ref t k))

(define-public hash-table.contains-key? hash-table.get)

(define-public (alist->hash-table kvs)
  (fold-left (lambda (t x) (hash-table.set! t (car x) (cdr x))) (hash-table.new) kvs))

(define-public (hash-table->alist t)
  (hash-map->list cons t))

(define-syntax ->>
  (syntax-rules ()
    [(->> x)
     x]
    [(->> x (func args ...) rest ...)
     (->> (func args ... x) rest ...)]
    [(->> x func rest ...)
     (->> (func x) rest ...)]))

(define-public orthogonal-dirs '(north south east west))
(define-public orthodiagonal-dirs '(north north-east east south-east south south-west west north-west))

(define-public (dir->vector dir)
  (cond
   ((equal? dir 'north) (list 0 -1))
   ((equal? dir 'south) (list 0 1))
   ((equal? dir 'east) (list 1 0))
   ((equal? dir 'west) (list -1 0))
   ((equal? dir 'north-west) (list -1 -1))
   ((equal? dir 'north-east) (list 1 -1))
   ((equal? dir 'south-east) (list 1 1))
   ((equal? dir 'south-west) (list -1 1))
   (else (error "Unknown dir " dir))))

(define-public (dir-from from to)
  (cond
   ((and (equal? (point.x from) (point.x to))
	 (< (point.y from) (point.y to)))
    'south)
   ((and (equal? (point.x from) (point.x to))
	 (> (point.y from) (point.y to)))
    'north)
   ((and (equal? (point.y from) (point.y to))
	 (< (point.x from) (point.x to)))
    'east)
   ((and (equal? (point.y from) (point.y to))
	 (> (point.x from) (point.x to)))
    'west)
   (else (error from "and" to "are not colinear!"))))

(define (dir->degrees dir)
  (cond
   ((equal? dir 'east) 0)
   ((equal? dir 'north-east) 45)
   ((equal? dir 'north) 90)
   ((equal? dir 'north-west) 135)
   ((equal? dir 'west) 180)
   ((equal? dir 'south-west) 225)
   ((equal? dir 'south) 270)
   ((equal? dir 'south-east) 315)
   (else (error "Unknown dir " dir))))

(define-public (degrees-between dir0 dir1)
  (clamp -180 180
	 (- (dir->degrees dir1)
	    (dir->degrees dir0))))

(define-public (vector->dir vec)
  (dir-from '(0 0) (point.add '(0 0) vec)))

(define-public (orthogonal-neighbours point)
  (->> orthogonal-dirs
       (map dir->vector)
       (map (cut point.add point <>))))

(define-public (progress-bar n f)
  ;; Wraps f in a lambda that displays an indicator of how many times f has been called.
  ;; Useful for slow filters or maps.
  ;;
  ;; n is the expected number of times f will be called in total.
  (let ((i (list 0))
	(start-time (list 'unset)))
    (lambda args
      (when (equal? 'unset (car start-time))
	(list-set! start-time 0 (current-time)))
      (list-set! i 0 (+ 1 (car i)))
      
      (let* ((val (apply f args))
	     (now (current-time))
	     (elapsed (- now (car start-time)))
	     (expected-duration (ceiling (/ (* n elapsed) (car i))))
	     (expected-end (+ (car start-time) expected-duration)))
	(format #t "~a / ~a (ETA ~a)" (car i) n (strftime "%c" (localtime expected-end)))
	(if (equal? (car i) n)
	    (newline)
	    (display #\return))
	val))))

(define (bfs-result.new pos cost prevs)
  (list pos cost prevs))
(define (bfs-result.pos r) (car r))
(define-public (bfs-result.cost r) (cadr r))
(define (bfs-result.prevs r) (caddr r))
(define (bfs-result.merge-prevs r0 r1)
  (when (not (and (equal? (bfs-result.pos r0)
			  (bfs-result.pos r1))
		  (equal? (bfs-result.cost r0)
			  (bfs-result.cost r1))))
    (error r0 "and" r1 "not for the same position and cost"))
  (bfs-result.new (bfs-result.pos r0)
		  (bfs-result.cost r1)
		  (append (bfs-result.prevs r0)
			  (bfs-result.prevs r1))))
(define-public (bfs-result->reverse-paths r)
    (if (null? (bfs-result.prevs r))
	(list (list (bfs-result.pos r)))
	(->> (bfs-result.prevs r)
	     (flat-map bfs-result->reverse-paths)
	     (map (cut cons (bfs-result.pos r) <>)))))

(define-public (bfs-result->paths r)
  (map reverse (bfs-result->reverse-paths r)))

(define-public (bfs-result->reverse-path r)
  (if (null? (bfs-result.prevs r))
      (list (bfs-result.pos r))
      (cons (bfs-result.pos r)
	    (bfs-result->reverse-path (car (bfs-result.prevs r))))))

(define-public (bfs-result->path r)
  (reverse (bfs-result->reverse-path r)))

(define-public (bfs-all start nexts cost-from end?)
  ;; bfs-all :: T -> (T -> list[T]) -> (T -> T -> number) -> (T -> bool) -> list[bfs-result]
  ;;
  ;; Assumes no two nodes have a 0-cost transition.
  (define (bring-cheapest-first q)
    ;; bring-cheapest-first :: list[bfs-result] -> list[bfs-result]
    ;;
    ;; Assumes that q is non-empty. Assumes that the position of each
    ;; element of q is a key in cost-by-pos.
    (define (iter min rest result)
      (cond ((null? rest)
	     (cons min result))
	    ((< (bfs-result.cost (car rest))
		(bfs-result.cost min))
	     ;; This is the cheapest element we've found so far
	     (iter (car rest) (cdr rest) (cons min result)))
	    (else
	     ;; The current min is cheaper, add the element to the result
	     (iter min (cdr rest) (cons (car rest) result)))))
    (iter (car q) (cdr q) '()))

  (define (reduce-cost! cost-by-pos curr)
    (hash-table.map-value! cost-by-pos
			   (bfs-result.pos curr)
			   (lambda (old-val)
			     (if old-val
				 (min old-val
				      (bfs-result.cost curr))
				 (bfs-result.cost curr)))))

  (define (maintain-queue new-nodes old-queue cost-by-pos)
    ;; - Merges new-nodes into old-queue.
    ;;
    ;; - Reduces the cost of any nodes in cost-by-pos we can now reach cheaper
    ;;
    ;; - We don't need to discard nodes we have already visited,
    ;;   because we filter them out when generating neighbours.
    ;;
    ;; - Discards any elements of the queue that are more expensive than
    ;;   the cheapest known way to get to a square.
    ;;
    ;; - Ensures we have at most one copy of each node in the queue
    ;;   (by pos), merging their prevs lists. Assumes that this
    ;;   already holds for old-queue.
    (define (merge-nodes n0 n1)
      (cond ((< (bfs-result.cost n0)
		(bfs-result.cost n1))
	     n0)
	    ((< (bfs-result.cost n1)
		(bfs-result.cost n0))
	     n1)
	    (else (bfs-result.merge-prevs n0 n1))))
    
    (define (include-new-node old-q new-node)
      (let* ((partner (find (lambda (r) (equal? (bfs-result.pos r)
						(bfs-result.pos new-node)))
			    old-q))
	     (others (filter (compose not (cut equal? <> partner))
			     old-q)))
	(if partner
	    (cons (merge-nodes new-node partner)
		  others)
	    (cons new-node others))))

    (define (discard-expensives q)
      (filter (lambda (r) (<= (bfs-result.cost r)
			      (hash-table.get cost-by-pos (bfs-result.pos r))))
	      q))
    
    (for-each (lambda (r) (reduce-cost! cost-by-pos r)) new-nodes)
    (discard-expensives (fold-left include-new-node old-queue new-nodes)))

  (define (next-elements curr cost-by-pos visited)
    ;; next-elements :: bfs-result -> hash-table[T, number] -> hash-set[T] -> list[bfs-result]
    (let* ((next-positions (filter (lambda (p) (not (hash-set.contains? visited p)))
				   (nexts (bfs-result.pos curr))))
	   (next-costs (map (lambda (p) (+ (cost-from (bfs-result.pos curr)
						      p)
					   (bfs-result.cost curr)))
			    next-positions))
	   (result (map (cut bfs-result.new <> <> (list curr)) next-positions next-costs)))
      result))
  
  (define (search queue lowest-cost-by-pos visited)
    (if (null? queue)
	'()
	(let ((queue (bring-cheapest-first queue)))
	  #;(format #t "@~a for ~a with #Q:~a #V:~a~%"
		  (bfs-result.pos (car queue))
		  (bfs-result.cost (car queue))
		  (length queue)
		  (hash-table.length lowest-cost-by-pos))
	  (if (end? (bfs-result.pos (car queue)))
	      ;; Return all queue elements at the end position. This
	      ;; is complete, because (car queue) is the cheapest
	      ;; element of the queue, so all not-yet-generated paths
	      ;; are strictly more expensive.
	      (filter (compose end? bfs-result.pos) queue)
	      (let ((new-costs (reduce-cost! lowest-cost-by-pos (car queue)))
		    (new-visited (hash-set.add! visited (bfs-result.pos (car queue)))))
	      (search (maintain-queue (next-elements (car queue) lowest-cost-by-pos new-visited)
				      (cdr queue)
				      new-costs)
		      new-costs
		      new-visited))))))

  (search (list (bfs-result.new start 0 '()))
	  (hash-table.new)
	  (hash-set.new)))

(define-public (bfs-one start nexts cost-from end?)
  (safe-car (bfs-all start nexts cost-from end?)))

(define-public (binary-search lo hi pred?)
  ;; pred? must satisfy that if (pred? i) is true, then so is
  ;; (pred? (1+ i))
  ;; Returns the smallest index for which (pred? i) is true,
  ;; or #f if no such index exists.

  (define (search lo hi)
    ;; The range of possibilities is [lo, hi]
    (format #t "Searching [~a, ~a]~%" lo hi)
    (cond ((< hi lo)
	   #f)
	  ((equal? lo hi)
	   lo)
	  (else (split-range lo hi))))

  (define (split-range lo hi)    	
    (let ((mid (floor (/ (+ lo hi) 2))))
      (if (pred? mid)
	  (search lo mid)
	  (search (1+ mid) hi))))
  
  (cond ((pred? lo)
	 lo)
	(else (search lo hi))))

(define-public (with-caching f)
  (let ((cache (hash-table.new)))
    (lambda args
      (if (hash-table.contains-key? cache args)
	  (hash-table.get cache args)
	  (let ((val (apply f args)))
	    (hash-table.set! cache args val)
	    val)))))
