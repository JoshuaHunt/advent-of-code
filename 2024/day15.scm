(use-modules (utils)
	     (srfi srfi-26))

(define (maze.new grid robot-pos)
  (list grid robot-pos))

(define maze.grid car)
(define (maze.get m pos)
  (grid.get (maze.grid m) pos))
(define (maze.set! m pos val)
  (grid.set! (maze.grid m) pos val)
  m)

(define (maze.robot-pos m) (cadr m))
(define (maze.with-robot-pos m pos)
  (maze.new (maze.grid m)
	    pos))
(define (maze.copy m)
  (maze.new (grid.copy (maze.grid m))
	    (maze.robot-pos m)))

(define (parse-maze lines)
  (let* ((lines (take-until-before empty-string? lines))
	 (grid (grid.new (map string->list lines)))
	 (robot-pos (only-element (grid.filter grid (cut equal? <> #\@)))))
    (maze.new (grid.set! grid robot-pos #\.)
	      robot-pos)))

(define (parse-wide-maze lines)
  (define (expand xs)
    (flat-map (lambda (c)
		(cond ((equal? c #\.)
		       '(#\. #\.))
		      ((equal? c #\#)
		       '(#\# #\#))
		      ((equal? c #\O)
		       '(#\[ #\]))
		      ((equal? c #\@)
		       '(#\@ #\.))
		      (else
		       (error "Unexpected input" c))))
	      xs))
  
  (let* ((lines (take-until-before empty-string? lines))
	 (grid (grid.new (map (compose expand string->list) lines)))
	 (robot-pos (only-element (grid.filter grid (cut equal? <> #\@)))))
    (maze.new (grid.set! grid robot-pos #\.)
	      robot-pos)))

(define (parse-commands lines)
  (let ((lines (drop-until-after empty-string? lines))
	(char-to-dir (lambda (c)
		       (cond ((equal? c #\<)
			      'west)
			     ((equal? c #\^)
			      'north)
			     ((equal? c #\>)
			      'east)
			     ((equal? c #\v)
			      'south)
			     (else
			      (error "Unknown direction for" c))))))
    (->> (apply string-append lines)
	 (string->list)
	 (map char-to-dir))))

(define (find-box-positions maze)
  (grid.filter (maze.grid maze)
	       (cut equal? <> #\O)))

(define (find-box-left-positions maze)
  (grid.filter (maze.grid maze)
	       (cut equal? <> #\[)))

(define (get-gps-coord pos)
  (+ (* 100 (point.y pos))
     (point.x pos)))
	 

(define (get-body maze pos)
  (let ((val (maze.get maze pos)))
    (cond ((equal? val #\[)
	   (list pos (point.add pos '(1 0))))
	  ((equal? val #\])
	   (list pos (point.add pos '(-1 0))))
	  (else (list pos)))))

(define (block? val)
  (member val '(#\O #\[ #\])))

(define (movement-sources maze src dpos)
  ;; Returns list of positions that are being moved from
  ;; if an entity moves from src in direction dpos
  (let ((body (get-body maze src)))
    (append body
	    (->> body
		 (map (cut point.add <> dpos))
		 (filter (compose not (cut member <> body)))
		 (filter (compose block? (cut maze.get maze <>)))
		 (flat-map (cut movement-sources maze <> dpos))))))

(define (movement-destinations maze src dpos)
  ;; Returns list of (list val-to-write pos-to-write) when
  ;; an entity moves from src in direction dpos
  (map (lambda (src)
	 (list (maze.get maze src)
	       (point.add src dpos)))
       (movement-sources maze src dpos)))


(define (move-in-dir maze command)
  (let* ((dpos (dir->vector command))
	 (src (maze.robot-pos maze))
	 (dest (point.add src dpos))
	 (new-maze (maze.with-robot-pos (maze.copy maze) dest)))
    (for-each (lambda (pos)
		(maze.set! new-maze pos #\.))
	      (movement-sources maze src dpos))
    (for-each (lambda (xs)
		(let ((val (car xs))
		      (pos (cadr xs)))
		  (maze.set! new-maze pos val)))
	      (movement-destinations maze src dpos))
    new-maze))

(define (robot-can-move? maze command)
  (let ((destinations (movement-destinations maze
					     (maze.robot-pos maze)
					     (dir->vector command))))
    (apply and-fn (map (lambda (xs) (not (equal? (maze.get maze (cadr xs)) #\#)))
		       destinations))))


(define (format-maze m)
  (let ((maze-with-bot (maze.set! (maze.copy m)
				  (maze.robot-pos m)
				  #\@)))
    (string-join (list (grid->string (maze.grid maze-with-bot))
		       (format #f "~a" (maze.robot-pos maze-with-bot))
		       "")
		 "\n")))

(define (peek x f)
  (display (f x))
  (newline)
  x)

(define (debug-apply-command maze command)
  (peek (if (robot-can-move? maze (peek command identity))
	    (move-in-dir maze command)
	    maze)
	format-maze))

(define (apply-command maze command)
  (if (robot-can-move? maze command)
      (move-in-dir maze command)
      maze))

(define my-input (call-with-input-file "2024/input15.txt" read-lines))

(define (solve-1 lines)
  (let* ((maze (parse-maze lines))
	 (commands (parse-commands lines))
	 (final-maze (fold-left apply-command maze commands)))
    (->> final-maze
	 (find-box-positions)
	 (map get-gps-coord)
	 (apply +))))

(define (solve-2 lines)
  (let* ((maze (parse-wide-maze lines))
	 (commands (parse-commands lines))
	 (final-maze (fold-left apply-command maze commands)))
    (->> final-maze
	 (find-box-left-positions)
	 (map get-gps-coord)
	 (apply +))))
    
(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
