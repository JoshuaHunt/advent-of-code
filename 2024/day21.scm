(use-modules (utils)
	     (srfi srfi-26))

(define (num-pad-contains? p)
  (and (<= 0 (point.x p) 2)
       (<= 0 (point.y p) 3)
       (not (equal? p '(0 3)))))

(define (dir-pad-contains? p)
  (and (<= 0 (point.x p) 3)
       (<= 0 (point.y p) 2)
       (not (equal? p '(0 0)))))

(define (digit->dir-key d)
  (case d
    ((#\^) '(1 0))
    ((#\A) '(2 0))
    ((#\<) '(0 1))
    ((#\V) '(1 1))
    ((#\>) '(2 1))
    (else (error "Unknown digit" d))))

(define (digit->num-key d)
  (case d
    ((#\7) '(0 0))
    ((#\8) '(1 0))
    ((#\9) '(2 0))
    ((#\4) '(0 1))
    ((#\5) '(1 1))
    ((#\6) '(2 1))
    ((#\1) '(0 2))
    ((#\2) '(1 2))
    ((#\3) '(2 2))
    ((#\0) '(1 3))
    ((#\A) '(2 3))))

(define (dir->digit d)
  (cond ((equal? d 'north) #\^)
	((equal? d 'south) #\V)
	((equal? d 'east) #\>)
	((equal? d 'west) #\<)
	(else (error "Unknown dir" d))))

(define dir->dir-key
  (compose digit->dir-key dir->digit))

(define (num-key->digit k)
  (car (filter (lambda (d) (equal? (digit->num-key d) k))
	       (string->list "A0123456789"))))

(define (dir-key->digit pos)
  (cond ((equal? pos '(1 0)) #\^)
	((equal? pos '(0 1)) #\<)
	((equal? pos '(1 1)) #\V)
	((equal? pos '(2 1)) #\>)
	((equal? pos '(2 0)) #\A)
	(else (error "Unknown dir pad key" pos))))

(define (neighbours layer-contains? p)
  ;; Accepts (prev layer pos, current layer pos)
  (cons (list (digit->dir-key #\A)
	      (cadr p))
	(->> orthogonal-dirs
	     (map (lambda (d)
		    (list (dir->dir-key d)
			  (point.add (cadr p) (dir->vector d)))))
	     (filter (compose layer-contains? cadr)))))

(define (shortest-path-bfs prev-path layer-contains? start end)
  ;; Computes the keypresses to move to end and press it, starting from start.
  (if (equal? start end)
      ;; Need to special-case this, because bfs-one will return an empty path in
      ;; this case.
      1
      (let ((path (bfs-result->path (bfs-one
				     ;; Positions are (position on penultimate dir-pad
				     ;; layer, position on last layer).  We know the cost to
				     ;; move between positions on the penultimate layer by
				     ;; recursion, so it suffices to incorporate the
				     ;; distances on this layer. Any path on this layer
				     ;; starts and ends at A on the previous layer, because
				     ;; we need to end up there to push the button on this
				     ;; layer.
				     (list (digit->dir-key #\A) start)
				     (cut neighbours layer-contains? <>)
				     (lambda (from to)
				       (prev-path (car from)
						  (car to)))
				     (cut equal? (list (digit->dir-key #\A) end) <>)))))
					;(format #t "Path ~a~%" path)
	(->> path
	     (map car)
	     (sliding-windows 2)
	     (map (cut apply prev-path <>))
	     (apply +)))))

(define shortest-path-dir-pad
  (with-caching
   (lambda (num-dir-pads from to)
     ;; From / to are keys on the dir pad. All previous layers are assumed
     ;; to start and end pointing at A.
     (if (= 1 num-dir-pads)
	 1 ; just press the key
	 (shortest-path-bfs
	  ;; Here s / e are dir keys on the previous layer
	  (lambda (s e)	    
	    (shortest-path-dir-pad (1- num-dir-pads) s e))
	  dir-pad-contains?
	  from
	  to)))))


(define (shortest-path-num-pad num-dir-pads from to)
  ;; Here from / to are keys on the num pad
  (shortest-path-bfs
   (lambda (s e)
     ;; Here from / to are dir keys on the previous layer
     (shortest-path-dir-pad num-dir-pads s e))
   num-pad-contains?
   from to))

(define (shortest-seq num-dir-pads line)
  (->> (cons #\A (string->list line))
       (map digit->num-key)
       (sliding-windows 2)
       (map (cut apply shortest-path-num-pad num-dir-pads <>))
       (apply +)))

(define (prefixes line)
  (map (cut substring line 0 <>)
       (iota (1+ (string-length line)))))

(define (numeric-prefix line)
  (apply or-fn (reverse (map string->number (prefixes line)))))

(define (complexity num-dir-pads line)
  (* (shortest-seq num-dir-pads line)
     (numeric-prefix line)))

(define my-in (call-with-input-file "2024/input21.txt" read-lines))

(define (solve-1 lines)
  (apply + (map (cut complexity 3 <>) lines)))

(define (solve-2 lines)
  (apply + (map (cut complexity 26 <>) lines)))

(display (solve-1 my-in))
(newline)
(display (solve-2 my-in))
(newline)
