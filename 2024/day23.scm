(use-modules (utils)
	     (srfi srfi-26)
	     (srfi srfi-64))
(define (hash-table.keys t)
  (map car (hash-table->alist t)))

(define (graph.new) (hash-table.new))
(define (graph.ensure-vertex g v)
  (when (not (hash-table.contains-key? g v))
    (hash-table.set! g v (hash-set.new))))
(define (graph.add-directed-edge g from to)
  (graph.ensure-vertex g from)
  (graph.ensure-vertex g to)
  (hash-set.add! (hash-table.get g from) to)
  g)
(define (graph.nodes g)
  (hash-table.keys g))
(define (graph.add-edge g from to)
  (graph.add-directed-edge g from to)
  (graph.add-directed-edge g to from))
(define (graph.filter g pred?) (uniq (filter pred? (hash-table.keys g))))
(define (graph.neighbours g n) (hash-table.get g n))
(define (graph.neighbours? g n1 n2)
  (hash-set.contains? (hash-table.get g n1) n2))

(define (node.id n) n)

(define (any pred? xs)
  (fold-left
   (lambda (acc x)
     (or acc (pred? x)))
   #f
   xs))

(define (triangles-containing g n1)
  (define (triangles-containing-2 g n1 n2)
    (let ((n3s (graph.neighbours g n2)))
      (->> n3s
	   (hash-set->list)
	   ;; n1 neighbours n2, which neighbours n3, so check n1 neighbours n3.
	   (filter (cut graph.neighbours? g n1 <>))
	   ;; Sort the triangle to avoid overcounting.
	   (map (lambda (n3) (sort (list n1 n2 n3) string<))))))
				 
  (let ((n2s (graph.neighbours g n1)))
    (flat-map (lambda (n2) (triangles-containing-2 g n1 n2)) (hash-set->list n2s))))

(define (nodes-starting-with c graph)
  (graph.filter graph (lambda (n) (equal? (string-ref (node.id n) 0) c))))

(define (uniq xs)
  (hash-set->list (list->hash-set xs)))

(define (find-triangles graph)
  (let ((ts (nodes-starting-with #\t graph)))
    (uniq (flat-map (cut triangles-containing graph <>) ts))))

(define (parse-graph lines)
  (fold-left (lambda (g line)
	       (let* ((parts (string-split line #\-))
		      (from (car parts))
		      (to (cadr parts)))
		 (graph.add-edge g from to)))
	     (graph.new)
	     lines))

(define (next-candidates g c)
  ;; Accepts a pair (hashset of contained nodes, hashset of nodes
  ;; adjacent to all those nodes). Returns a list of such pairs, where
  ;; each pair's car contains one more node than the input pair's.
  ;; To avoid counting multiple times, we only add elements from the
  ;; neighbours who are lexicographically greater than all other elements.
  (let ((ns (cadr c))
	(subgraph (car c)))
    (->> (hash-set->list ns)
	 (filter (lambda (n)
		   (all? (lambda (o) (string< o n)) (hash-set->list subgraph))))
	 (map (lambda (n)
		(list (hash-set.add subgraph n)
		      (hash-set.intersection ns
					     (graph.neighbours g n))))))))


(test-begin "next-candidates")
(let* ((g (parse-graph '("1-2"
			 "2-3"
			 "1-3"
			 "1-4"
			 "2-4")))
       (r (next-candidates g (list (hash-set.of "1") (hash-set.of "2" "3" "4")))))
  (test-assert (= (length r) 3)))
(test-end "next-candidates")

(define (find-complete-subgraph g)
  (define (reduce-candidates candidates)
    (when (null? candidates)
      (error "No remaining candidates!"))
    (if (null? (cdr candidates))
	(car candidates)
	(reduce-candidates (flat-map (cut next-candidates g <>) candidates))))
  
  (let ((candidates (map (lambda (n)
			   (list (hash-set.of n)
				 (graph.neighbours g n)))
			 (graph.nodes g))))
    (car (reduce-candidates candidates))))

(define (solve-1 lines)
  (length (find-triangles (parse-graph lines))))

(define (compute-password subg)
  (string-join (sort (hash-set->list subg) string<) ","))

(define (solve-2 lines)
  (compute-password (find-complete-subgraph (parse-graph lines))))

(define my-input (call-with-input-file "2024/input23.txt" read-lines))
(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
