{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/0c5b4ecbed5b155b705336aa96d878e55acd8685";
  };

  outputs = { self, nixpkgs, ...}: let
    system = "x86_64-linux";
  in {
    devShells."${system}".default =
      with import nixpkgs { inherit system; };
      mkShell {
        buildInputs = [
          ((emacsPackagesFor emacs-nox).emacsWithPackages(epkgs: [
            epkgs.geiser
            epkgs.geiser-guile
          ]))
        ];
        packages = [
          guile
	        just
        ];
        shellHook = ''
          exec fish
        '';
      };
  };
}
      
