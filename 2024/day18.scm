(use-modules (utils)
	     (srfi srfi-26))

(define (parse-bytes lines)
  (map (compose (cut map string->number <>)
		(cut string-split <> #\,))
       lines))

(define (corrupt! bytes grid)
  (for-each (cut grid.set! grid <> #\#) bytes)
  grid)

(define (do-bfs bytes)
  (let* ((grid-size (list 71 71))
	 (grid (corrupt! bytes (grid.with-size grid-size #\.)))
	 (bfs-result (bfs-one
		      '(0 0)
		      (lambda (p)
			(filter (lambda (n) (equal? (grid.get grid n)
						    #\.))
				(orthogonal-neighbours p)))
		      (const 1)
		      (lambda (p) (equal? (point.add p '(1 1))
					  grid-size)))))
    bfs-result))

(define (solve-1 lines)
  (let ((bytes (take 1024 (parse-bytes lines))))
    (1- (length (bfs-result->path (do-bfs bytes))))))

(define (solve-2 lines)
  (let* ((bytes (parse-bytes lines))
	 (i (binary-search 0 (length bytes)
			   (lambda (i)			     
			     (not (do-bfs (take i bytes)))))))
    (list-ref bytes (1- i))))




(define my-input (call-with-input-file "2024/input18.txt" read-lines))
(display (solve-1 my-input))
(newline)    
(display (solve-2 my-input))
(newline)
