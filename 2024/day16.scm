(use-modules (utils)
	     (srfi srfi-8)
	     (srfi srfi-26))

(define (is-wall? c)
  (equal? c #\#))

(define (neighbours pos other-poses)
  (filter (cut hash-table.contains-key? other-poses <>) (orthogonal-neighbours pos)))

(define (maze.new start-pos end-pos nodes grid)
  (list start-pos
	end-pos
	(alist->hash-table (map (lambda (n) (cons (node.pos n) n)) nodes))
	(* (grid.height grid)
	   (grid.width grid))))
(define (maze.start-pos m) (car m))
(define (maze.start-dir m) 'east)
(define (maze.goal maze) (cadr maze))
(define (maze.neighbours m pos) (neighbours pos (caddr m)))
(define (maze.area maze) (cadddr maze))

(define (node.new pos neighbour-poses)
  (list pos neighbour-poses))
(define (node.pos n) (car n))

(define (neighbour-positions-cb maze)
  (lambda (pos-dir)
    (let* ((next-positions (maze.neighbours maze (car pos-dir)))
	   (next-dirs (map (cut dir-from (car pos-dir) <>) next-positions)))
      (map list next-positions next-dirs))))

(define (transition-cost from to)
  (+ (point.taxi-cab-distance (car from) (car to))
     (* 1000 (/ (abs (degrees-between (cadr from)
				      (cadr to)))
		90))))

(define (end?-cb maze)
  (lambda (pos-dir)
    (equal? (maze.goal maze) (car pos-dir))))

(define (find-shortest-path maze)
  (bfs-one (list (maze.start-pos maze)
		 (maze.start-dir maze))
	   (neighbour-positions-cb maze)
	   transition-cost
	   (end?-cb maze)))

(define (find-shortest-paths maze)
  (bfs-all (list (maze.start-pos maze)
		 (maze.start-dir maze))
	   (neighbour-positions-cb maze)
	   transition-cost
	   (end?-cb maze)))

(define (parse-nodes grid)
  (let* ((positions (grid.filter grid (compose not is-wall?)))
	 (pos-set (list->hash-set positions)))
    (map (lambda (pos)
	   (node.new pos (neighbours pos pos-set)))
	 positions)))

(define (parse-start-pos grid)
  (only-element (grid.filter grid (cut equal? <> #\S))))

(define (parse-end-pos grid)
  (only-element (grid.filter grid (cut equal? <> #\E))))

(define (parse-maze lines)
  (let* ((grid (grid.new (map string->list lines)))
	 (nodes (parse-nodes grid))
	 (start-pos (parse-start-pos grid))
	 (end-pos (parse-end-pos grid)))
    (maze.new start-pos
	      end-pos
	      nodes
	      grid)))

(define my-input (call-with-input-file "2024/input16.txt" read-lines))

(define (print-path lines path)
  (let ((grid (grid.new (map string->list lines))))
    (for-each (lambda (pos-dir) (grid.set! grid (car pos-dir) #\@))
	      path)
    (display (grid->string grid))
    (newline)))

(define (solve-1 lines)
  (let ((result (find-shortest-path (parse-maze lines))))
    (print-path lines (car (bfs-result->paths result)))
    (bfs-result.cost result)))

(define (solve-2 lines)
  (let* ((results (find-shortest-paths (parse-maze lines)))
	 (tiles (list->hash-set (map car (apply append (flat-map bfs-result->paths results))))))
    (hash-set.length tiles)))

(define (main)
  (display (solve-1 my-input))
  (newline)
  (display (solve-2 my-input))
  (newline))
