(use-modules (utils))
(use-modules (srfi srfi-26))
(use-modules (srfi srfi-1))

(define (antenna.new point type)
  (list point type))
(define antenna.pos car)
(define antenna.type cadr)

(define (parse-antennas lines)
  (let* ((g (grid.new (map string->list lines)))
	 (matches (grid.filter g (lambda (x) (not (equal? x #\.))))))
    (map (lambda (p) (antenna.new p (grid.get g p))) matches)))

(define (find-same-type-antennas a bs)
  (filter (lambda (b) (equal? (antenna.type a) (antenna.type b))) bs))

(define (get-antinode a b n)
  (let ((pa (antenna.pos a))
	(pb (antenna.pos b)))
    (point.add pa (vector.scale (point.minus pa pb) n))))

(define (get-pair-antinodes antennas)
  (define (iter rest)  ; inefficient
    (if (null? rest)
	'()
	(let* ((others (find-same-type-antennas (car rest) (cdr rest)))
	       (antinodes0 (map (cut get-antinode (car rest) <> 1) others))
	       (antinodes1 (map (cut get-antinode <> (car rest) 1) others)))
	  (append antinodes0
		  antinodes1
		  (iter (cdr rest))))))

  (iter antennas))

(define (get-antinode-ray a b width height)
  (define (iter n acc)
    (let ((new-antinode (get-antinode a b n)))
      (if (in-grid? width height new-antinode)
	  (iter (+ n 1) (cons new-antinode acc))
	  acc)))
  (iter 0 '()))

(define (get-line-antinodes antennas width height)
  (define (iter rest)
    (if (null? rest)
	'()
	(let* ((others (find-same-type-antennas (car rest) (cdr rest)))
	       (antinodes0 (flat-map (cut get-antinode-ray (car rest) <> width height) others))
	       (antinodes1 (flat-map (cut get-antinode-ray <> (car rest) width height) others)))
	  (append antinodes0
		  antinodes1
		  (iter (cdr rest))))))
  (iter antennas))
	

(define (in-grid? width height pos)
  (and (< -1 (point.x pos) width)
       (< -1 (point.y pos) height)))

(define my-input (call-with-input-file "2024/input08.txt" read-lines))

(define (solve-1 lines)
  (let ((antennas (parse-antennas lines))
	(width (string-length (car lines)))
	(height (length lines)))
    (length (delete-duplicates (filter (cut in-grid? width height <>) (get-pair-antinodes antennas))))))

(define (solve-2 lines)
  (let ((antennas (parse-antennas lines))
	(width (string-length (car lines)))
	(height (length lines)))
    (length (delete-duplicates (get-line-antinodes antennas width height)))))

(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
