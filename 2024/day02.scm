(use-modules (utils))

(define (parse-report line)
  (map string->number (string-split line #\space)))

(define (parse-reports lines)
  (map parse-report lines))

(define (safe-pair? x y less)
  (or (equal? x 'sentinel)
      (equal? y 'sentinel)
      (and (less x y)
	   (< 0 (abs (- x y)) 4))))

(define (damped-safe-dir? reports less damps)
  (cond ((> 0 damps)
	 #f)
	((or (null? reports) (null? (cdr reports)))
	 #t)
	(else
	 (or (and (safe-pair? (car reports) (cadr reports) less)
		  (damped-safe-dir? (cdr reports) less damps))
	     ; There are only 2 elements left, we can ignore the last one.
	     (and (null? (cddr reports))
		  (< 0 damps))
	     (and (not (null? (cddr reports)))
		  (safe-pair? (car reports) (caddr reports) less)
		  (damped-safe-dir? (cddr reports) less (- damps 1)))))))

(define (undamped-safe? reports)
  (or (damped-safe-dir? (cons 'sentinel reports) < 0)
      (damped-safe-dir? (cons 'sentinel reports) > 0)))

(define (damped-safe? reports)
  (or (damped-safe-dir? (cons 'sentinel reports) < 1)
      (damped-safe-dir? (cons 'sentinel reports) > 1)))

(define (solve-part-1 lines)
  (length (filter undamped-safe? (parse-reports lines))))

(define (solve-part-2 lines)
  (length (filter damped-safe? (parse-reports lines))))

(define my-input (call-with-input-file "2024/input02.txt" read-lines))

(format #t "~a~%~a~%"
	(solve-part-1 my-input)
	(solve-part-2 my-input))

(for-each
 (lambda (r) (format #t "~a~%" r))
 (filter (compose not damped-safe?) (parse-reports my-input)))
