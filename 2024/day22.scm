(use-modules (utils)
	     (srfi srfi-26)
	     (srfi srfi-64))

(define (mix secret val)
  (logxor secret val))

(define (prune secret)
  (modulo secret 16777216))

(define (next-secret secret)
  (->> secret
       ((lambda (s) (mix s (* 64 s))))
       (prune)
       ((lambda (s) (mix s (floor (/ s 32)))))
       (prune)
       ((lambda (s) (mix s (* 2048 s))))
       (prune)))

(define my-input (call-with-input-file "2024/input22.txt" read-lines))

(define (solve-1 lines)
  (apply + (map (cut repeat next-secret 2000 <>) (map string->number lines))))

(define (secrets-seq s)
  (define (iter cur n)
    (if (= n 0)
	'()
	(cons cur (iter (next-secret cur)
			(1- n)))))
  (iter s 2001))

(test-begin "secrets-seq")
(test-assert "length"
  (= (length (secrets-seq 123))
     2001))
(test-assert "initial segment"
  (equal? (take 11 (secrets-seq 123))
	  '(123 15887950 16495136 527345 704524 1553684 12683156 11100544 12249484 7753432 5908254)))
(test-end "secrets-seq")
	    

(define (with-diffs ss)
  (->> ss
       (sliding-windows 2)
       (map (lambda (x0-x1)
	      (let ((x0 (modulo (car x0-x1) 10))
		    (x1 (modulo (cadr x0-x1) 10)))
		(list x1 (- x1 x0)))))))

(test-begin "with-diffs")
(test-equal "123"
  '((0 -3) (6 6) (5 -1) (4 -1))
  (with-diffs (take 5 (secrets-seq 123))))
(test-end "with-diffs")

(define (build-profits diffs)
  (fold-left
   (lambda (t window)
     (let ((seq (map cadr window))
	   (price (car (list-ref window 3))))
       (if (hash-table.contains-key? t seq)
	   t
           (hash-table.set! t seq price))))
   (hash-table.new)
   (sliding-windows 4 diffs)))

(test-begin "build-profits")
(test-equal "123"
  6
  (let ((t (build-profits (with-diffs (secrets-seq 123)))))
    (hash-table.get t '(-1 -1 0 2))))
(test-equal "first occurrence of sequence"
  3
  (hash-table.get (build-profits '((0 0) (1 1) (2 2) (3 3)
				   (4 0) (5 1) (6 2) (7 3)))
		  '(0 1 2 3)))
(test-end "build-profits")

(define (hash-table.merge-sum . ts)
  (if (null? ts)
      (hash-table.new)
      (let ((rest (apply hash-table.merge-sum (cdr ts))))
	(for-each (lambda (kv)
		    (let ((old (or (hash-table.get rest (car kv)) 0)))
		      (hash-table.set! rest (car kv) (+ (cdr kv) old))))
		  (hash-table->alist (car ts)))
	rest)))

(test-begin "hash-table.merge-sum")
(test-equal "sums values"
    '((1 . 1) (2 . 4) (3 . 5))
    (hash-table->alist
     (hash-table.merge-sum
      (alist->hash-table '((1 . 1) (2 . 1)))
      (alist->hash-table '((2 . 3) (3 . 5))))))
(test-end "hash-table.merge-sum")

(define (hash-table.values t)
  (map cdr (hash-table->alist t)))

(test-begin "hash-table.values")
(test-assert "values"
  (let ((t (alist->hash-table '((1 . 2) (3 . 4)))))
    (or (equal? (hash-table.values t) '(2 4))
	(equal? (hash-table.values t) '(4 2)))))
(test-end "hash-table.values")

(define (solve-2 lines)
  (let* ((secrets (map string->number lines))
	 (profits-by-seq (map (compose build-profits with-diffs secrets-seq) secrets))
	 (overall-profits (apply hash-table.merge-sum profits-by-seq)))
    (apply max (hash-table.values overall-profits))))

(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
