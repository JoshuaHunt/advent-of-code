(use-modules (utils)
	     (srfi srfi-1))

(define (parse-grid rev-lines)
  (grid.new (map string->list (reverse rev-lines))))

(define (key? grid)
  (equal? (grid.get grid '(0 0)) #\.))

(define (lock? grid)
  (equal? (grid.get grid '(0 0)) #\#))

(define (parse-grids lines)
  (define (iter rest acc result)
    (cond ((null? rest)
	   (cons (parse-grid acc) result))
	  ((empty-string? (car rest))
	   (iter (cdr rest) '() (cons (parse-grid acc) result)))
	  (else
	   (iter (cdr rest) (cons (car rest) acc) result))))
  (iter lines '() '()))

(define (key-heights grids)
  (map heights (filter key? grids)))

(define (lock-heights grids)
  (map heights (filter lock? grids)))

(define (heights grid)
  (let ((hs (fold-left (lambda (t pt)
			 (if (equal? (grid.get grid pt) #\#)
			     (hash-table.set! t (point.x pt)
					      (1+ (or (hash-table.get t (point.x pt)) 0)))
			     t))
		       (hash-table.new)
		       (grid.points grid))))
    (map (lambda (x) (or (hash-table.get hs x) 0)) (iota (grid.width grid)))))

(define my-input (call-with-input-file "2024/input25.txt" read-lines))

(define (compatible? key-and-lock-heights)
  (every (lambda (h) (<= h 7)) (apply map + key-and-lock-heights)))

(define (solve-1 lines)
  (let* ((grids (parse-grids lines))
	 (keys (key-heights grids))
	 (locks (lock-heights grids)))
    (length (filter compatible? (cross-product keys locks)))))

(display (solve-1 my-input))
(newline)
