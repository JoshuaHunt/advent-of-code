(use-modules (utils))

(define (mul.new first-op second-op)
  (list first-op second-op))

(define (mul.evaluate m)
  (* (car m) (cadr m)))

(define (mul? m)
  (list? m))

(define (scan-for-second-operand line first-operand)
  (define (foo i)
    (cond ((<= (string-length line) i)
	   '())
	  ((char-numeric? (string-ref line i))
	   (foo (+ i 1)))
	  ((equal? #\) (string-ref line i))
	   (let ((second-operand (string->number (substring line 0 i))))
	     (cons (mul.new first-operand second-operand)
		   (scan-for-op (substring line (+ i 1))))))
	  (else
	   (scan-for-op (substring line i)))))
  (foo 0))

(define (scan-for-first-operand line)
  (define (foo i)
    (cond ((<= (string-length line) i)
	   '())
	  ((< 3 i)	; We allow a max of 3-digit numbers
	   (scan-for-op (substring line i)))
	  
	  ((char-numeric? (string-ref line i))
	   (foo (+ i 1)))
	  ((equal? #\, (string-ref line i))
	   (let ((first-operand (string->number (substring line 0 i))))
	     (scan-for-second-operand (substring line (+ i 1)) first-operand)))
	  (else
	   (scan-for-op (substring line i)))))
  (foo 0))

;; Edge case: "mul(12mul(3,4))"

(define (scan-for-op line)	     
  (cond ((empty-string? line)
	 '())
	((string-prefix? "do()" line)
	 (cons 'do (scan-for-op (substring line 4))))
	((string-prefix? "don't()" line)
	 (cons 'dont (scan-for-op (substring line 7))))
	((string-prefix? "mul(" line)
	 (scan-for-first-operand (substring line 4)))
	(else
	 (scan-for-op (substring line 1)))))

(define my-input (call-with-input-file "2024/input03.txt" read-lines))

(define (solve-part-1 code)
  (let ((matches (flat-map scan-for-op my-input)))
    (apply + (map mul.evaluate (filter mul? matches)))))

(define (evaluate ops)
  (define (foo state op)
;;    (format #t "~a ~a~%" state op)
    (let ((active (car state))
	  (sum (cdr state)))
      (cond ((and (equal? active 'do) (mul? op))
	     (cons active (+ sum (mul.evaluate op))))
	    ((mul? op)
	     (cons active sum))
	    (else
	     (cons op sum)))))
  (cdr (fold-left foo (cons 'do 0) ops)))
    

(define (solve-part-2 code)
  (evaluate (flat-map scan-for-op my-input)))

(format #t "~a~%~a~%"
	(solve-part-1 my-input)
	(solve-part-2 my-input))
