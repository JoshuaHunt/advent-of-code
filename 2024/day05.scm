(use-modules (utils))
(use-modules (srfi srfi-26))

(define (drop-last xs)
  (reverse (cdr (reverse xs))))

(define (take-until pred? xs)
  (cond ((null? xs)
	 '())
	((pred? (car xs))
	 (list (car xs)))
	(else
	 (cons (car xs)
	       (take-until pred? (cdr xs))))))

(define (drop-until pred? xs)
  (cond ((null? xs)
	 '())
	((pred? (car xs))
	 xs)
	(else (drop-until pred? (cdr xs)))))

(define (intersection xs ys)
  (if (> (length xs) (length ys))
      (intersection ys xs)
      (filter (cut member <> ys) xs)))

(define (set-minus xs ys)
  (filter (compose not (cut member <> ys)) xs))

(define (graph.empty)
  '())

(define (graph.add-edge g from to)
  (cons (list from to) g))

(define (graph.out-nodes g from)
  (map cadr (filter (lambda (e) (equal? (car e) from)) g)))

(define (graph.edges g)
  g)

(define (graph.remove-edge g e)
  (filter (compose not (cut equal? <> e)) g))

(define (graph.simplify g)
  (define (removable-edges g)
    (define (iter edges)
      (if (null? edges)
	  '()
	  (let* ((edge (car edges))
		 (from (car edge))
		 (to (cadr edge))
		 (from-children (graph.out-nodes g from))
		 (to-children (graph.out-nodes g to))
		 (commoners (intersection from-children to-children)))
	    (append (map (lambda (c) (list from c)) commoners)
		    (iter (cdr edges))))))
    (iter (graph.edges g)))

  (let ((to-remove (removable-edges g)))
    (if (null? to-remove)
	g
	(graph.simplify (fold-left (lambda (g e) (graph.remove-edge g e))
				   g
				   to-remove)))))

(define (graph.linearise g)
  (define (iter acc-rev)
    (let ((nexts (graph.out-nodes g (car acc-rev))))
      (if (or (null? nexts) (member (car nexts) acc-rev))
	  (reverse acc-rev)
	  (iter (cons (only-element nexts)
		      acc-rev)))))
  
  ;; Returns a list of the correct order for pages
  (let* ((srcs (map car (graph.edges g)))
	 (dsts (map cadr (graph.edges g)))
	 (both (intersection srcs dsts))
	 (only-srcs (set-minus srcs dsts)))
    (if (null? only-srcs)
	;; Graph is cyclic, choose an arbitrary source
	(iter (list (car srcs)))
	;; Assume graph has a single origin
	(iter (list (only-element only-srcs))))))

(define (graph.filter-to-book g book)
  (filter (lambda (e) (and (member (car e) book)
			   (member (cadr e) book)))
	  g))
  
(define (parse-graph lines)
  (define (parse-edge xs)
    (let* ((nodes (string-split xs #\|))
	   (from (string->number (car nodes)))
	   (to (string->number (cadr nodes))))
      (list from to)))

  (define (parse-edges xs)
    (map parse-edge xs))
  
  (let ((my-lines (drop-last (take-until empty-string? lines))))
    (fold-left (lambda (g e) (apply graph.add-edge g e))
	       (graph.empty)
	       (parse-edges my-lines))))

(define (parse-books lines)
  (let ((my-lines (cdr (drop-until empty-string? lines))))
    (map (lambda (l) (map string->number (string-split l #\,)))
	 my-lines)))

(define (is-ordered? book graph)
  (if (null? (cdr book))
      #t
      (and (member (cadr book) (graph.out-nodes graph (car book)))
	   (is-ordered? (cdr book) graph))))

(define (middle-page book)
  ;; 3 -> 1
  ;; 5 -> 2
  (let ((index (/ (- (length book) 1) 2)))
    (list-ref book index)))

(define (fix-book book graph)
  (let ((fixed (graph.linearise (graph.simplify (graph.filter-to-book graph book)))))
    (if (equal? (length fixed) (length book))
	fixed
	(error fixed " has a different length to " book))))

(define my-input (call-with-input-file "2024/input05.txt" read-lines))

(define (solve-part-1 lines)
  (let ((graph (parse-graph lines))
	(books (parse-books lines)))
    (apply + (map middle-page (filter (lambda (b) (is-ordered?  b graph)) books)))))

(define (solve-part-2 lines)
  (let ((graph (parse-graph lines))
	(books (parse-books lines)))
    (apply +
	   (map middle-page
		(map (lambda (b) (fix-book b graph))
		     (filter (lambda (b) (not (is-ordered? b graph)))
			     books))))))

(display (solve-part-1 my-input))
(newline)
(display (solve-part-2 my-input))
(newline)
