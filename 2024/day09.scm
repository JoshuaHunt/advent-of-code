(use-modules (utils))
(use-modules (srfi srfi-1))
(use-modules (ice-9 receive))

(define (parse-disk lines)
  (map (compose string->number string) (string->list (car lines))))

(define (fill! vec from to val)
  (define (iter i)
    (when (< i to)
      (vector-set! vec i val)
      (iter (+ 1 i))))
  (iter from))

(define (disk->vector disk)
  (define (iter-blank vec rest i id)
    (if (null? rest)
	vec
	(iter-block vec (cdr rest) (+ i (car rest)) (+ 1 id))))
  
  (define (iter-block vec rest i id)
    (if (null? rest)
	vec
	(begin
 	  (fill! vec i (+ i (car rest)) id)
	  (iter-blank vec (cdr rest) (+ i (car rest)) id))))
  
  (let* ((len (apply + disk))
	 (vec (make-vector len #f)))
    (iter-block vec disk 0 0)))

(define (rearrange! vec)
  (define (iter lo hi)
    (cond ((equal? lo hi)
	   vec) ; lo < hi
	  ((vector-ref vec lo)
	   (iter (+ 1 lo) hi)) ; lo points to an empty space
	  ((not (vector-ref vec hi))
	   (iter lo (- hi 1))) ; hi points to a number
	  (else
	   (vector-set! vec lo (vector-ref vec hi))
	   (vector-set! vec hi #f)
	   (iter (+ lo 1) (- hi 1)))))
  (iter 0 (- (vector-length vec) 1)))

(define (checksum vec)
  (define (iter i checksum)
    (cond ((<= (vector-length vec) i)
	   checksum)
	  ((not (vector-ref vec i))
	   (iter (+ i 1) checksum))
	  (else	
	   (iter (+ i 1)
		 (+ checksum (* i (vector-ref vec i)))))))
  (iter 0 0))

(define (file.new pos size id)
  (list pos size id))
(define (file.pos f) (car f))
(define (file.size f) (cadr f))
(define (file.id f) (caddr f))
(define (file.end f) (+ (file.pos f) (file.size f)))

(define (find-files disk)
  (define (iter disk pos id)
    (cond ((null? disk)
	   '())
	  ((null? (cdr disk))
	   (list (file.new pos (car disk) id)))
	  (else
	   (cons (file.new pos (car disk) id)
		 (iter (cddr disk)
		       (+ pos
			  (car disk)
			  (cadr disk))
		       (+ id 1))))))
  (iter disk 0 0))

(define (find-free-space disk)
  (define (iter disk pos)
    (if (or (null? disk) (null? (cdr disk)))
	'()
	(cons (file.new pos (car disk) #f)
	      (iter (cddr disk)
		    (+ pos
		       (car disk)
		       (cadr disk))))))
  (iter (cdr disk) (car disk)))

(define (rearrange-files files free-space)
  (define (reserve-space file spaces)
    ;; Returns (old-space new-spaces)
    (define (iter rest)
      ;; Returns (old-space new-rest)
      (cond ((null? rest)
	     (values #f rest))
	    ((<= (file.pos file) (file.pos (car rest)))
	     (values #f rest))
					; car rest starts strictly before file
	    ((equal? (file.size file) (file.size (car rest)))
	     (values (car rest)
		     (cdr rest)))
	     ((< (file.size file) (file.size (car rest)))
	      (let ((old-space (car rest)))
		(values old-space
			(cons (file.new (+ (file.pos old-space)
					   (file.size file))
					(- (file.size old-space)
					   (file.size file))
					#f)
			      (cdr rest)))))
	     
	     (else
	      (receive (old-space new-spaces)
		  (iter (cdr rest))
		(values old-space
			(cons (car rest) new-spaces))))))
    (iter spaces))
  
  (define (move-file file spaces)
    ;; Returns (new-file new-spaces)
    (receive (old-space new-spaces)
	(reserve-space file spaces)
      (if old-space
	  (values (file.new (file.pos old-space)
			    (file.size file)
			    (file.id file))
		  new-spaces)
	  (values file spaces))))

  (define (iter rest-files spaces result)
    (if (null? rest-files)
	result
	(begin
;	  (format #t "~a~%~a~%~a~%~%" rest-files spaces result)
	(receive (new-file new-spaces)
	    (move-file (car rest-files)
		       spaces)
	  (iter (cdr rest-files)
		new-spaces
		(cons new-file result))))))
  
  (iter (reverse files)
	free-space
	'()))
    

(define (files->vector size files)
  (let ((vec (make-vector size #f)))
    (for-each (lambda (f)
		(fill! vec
		       (file.pos f)
		       (file.end f)
		       (file.id f)))
	      files)
    vec))

(define my-input (call-with-input-file "2024/input09.txt" read-lines))

(define (solve-1 lines)
  (checksum (rearrange! (disk->vector (parse-disk lines)))))

(define (rearrange-2 lines)
  (let* ((disk (parse-disk lines))
	 (files (find-files disk))
	 (free-space (find-free-space disk)))
    (files->vector (apply + disk) (rearrange-files files free-space))))

(define (solve-2 lines)
  (checksum (rearrange-2 lines)))

(display (solve-1 my-input))
(newline)

(display (solve-2 my-input))
(newline)
