(use-modules (utils)
	     (srfi srfi-1)
	     (srfi srfi-26))

(define (grid.flood-fill g start)
  ;; Returns the hash-set of visited nodes
  (let ((result (hash-set.new))
	(start-val (grid.get g start)))
    (define (iter x)
      (cond ((hash-set.contains? result x)
	     #f)
	    ((equal? (grid.get g x) start-val)
	     (hash-set.add! result x)
	     (for-each iter (orthogonal-neighbours x)))
	    (else #f)))
    (iter start)
    result))
  

(define (region.new points)
  ;; Points is a hash-set
  (list points))

(define (region.contains? r point)
  (hash-set.contains? (car r) point))

(define (region.points r)
  (hash-set->list (car r)))	 

(define (region.area r)
  (hash-set.length (car r)))

(define (region.perimeter r)
  (->> (region.points r)
       (map (cut orthogonal-neighbours <>))
       (map (cut filter (cut region.contains? r <>) <>))
       (map (lambda (neighbours) (- 4 (length neighbours))))
       (apply +)))

(define (region.sides r)
  (define (get-edge-pairs point)
    (map (lambda (other) (list point other)) (orthogonal-neighbours point)))

  (define (edge-pair->edge pair)
    ;; Always orient edges in direction of increasing x / y
    (let* ((in (car pair))
	   (out (cadr pair))
	   (dir (dir-from in out)))
      (cond ((equal? dir 'north)
	     (list in (point.add in (dir->vector 'east))))
	    ((equal? dir 'east)
	     (list out (point.add out (dir->vector 'south))))
	    ((equal? dir 'south)
	     (list out (point.add out (dir->vector 'east))))
	    ((equal? dir 'west)
	     (list in (point.add in (dir->vector 'south))))
	    (else (error "Unknown dir" dir)))))

  (define (mergeable? edges side0 side1)
    (cond ((equal? (cadr side1) (car side0))
	   (mergeable? edges side1 side0))
	  ((not (equal? (cadr side0) (car side1)))
	   #f)
	  ;; the endpoint of side0 matches the start of side1. We
	  ;; don't have to check reversed edges due to the orientation
	  ;; convention on sides.
	  ((not (equal? (dir-from (car side0) (cadr side0))
			(dir-from (car side1) (cadr side1))))
	   #f)
	  ;; side0 and side1 are colinear. We don't have to check
	  ;; reversed edges due to the aforementioned convention.
	  (else
	   (let ((blocking-edges (filter
				  (lambda (e)
				    (or (equal? (car e) (car side1))
					(equal? (cadr e) (car side1))))
				  edges)))
	     ;; We know that side0 and side1 appear in the list, so we
	     ;; need to check whether any other orthogonal edges also
	     ;; appear in the list.
	     (equal? (length blocking-edges) 2)))))

  (define (merge-side sides)
    ;; Pre-condition: sides are actually mergeable!
    ;; Then the merged side goes from (min-x, min-y) to (max-x, max-y)
    ;; due to the orientation convention on sides.
    (let* ((endpoints (apply append sides))
	   (min-x (apply min (map car endpoints)))
	   (min-y (apply min (map cadr endpoints)))
	   (max-x (apply max (map car endpoints)))
	   (max-y (apply max (map cadr endpoints))))
      (list (list min-x min-y)
	    (list max-x max-y))))

  (define (merge-sides edges sides new-side)
    ;; Pre-condition: no two sides in sides are mergeable.
    ;; Post-condition: no two sides in the return value are mergeable
    ;; We only require `edges` for checking that there's no edge
    ;; orthogonal to the proposed merge point (see the figure-8
    ;; example from the puzzle description).
    
    (let* ((mergeable-sides (filter (cut mergeable? edges new-side <>) sides))
	   (unmergeable-sides (filter (compose not (cut member <> mergeable-sides)) sides)))
      (cons (merge-side (cons new-side mergeable-sides))
	    unmergeable-sides)))
		  
  (define (merge-edges edges)
    (fold-left (lambda (sides new-side) (merge-sides edges sides new-side))
	       '()
	       edges))
  
  (let* ((points (region.points r))
	 (edge-pairs (flat-map get-edge-pairs points))
	 (external-edge-pairs (filter
			       (compose not (cut region.contains? r <>) cadr)
			       edge-pairs))
	 (edges (map edge-pair->edge external-edge-pairs)))
    (length (merge-edges edges))))

(define (parse-grid lines)
  (grid.new (map string->list lines)))

(define (build-regions grid)
  (define (iter points regions)
    ;; Returns a list of regions
    (cond ((null? points)
	   regions)
	  ((find (cut region.contains? <> (car points)) regions)
	   (iter (cdr points)
		 regions))
	  (else
	   (iter (cdr points)
		 (cons (region.new (grid.flood-fill grid (car points)))
		       regions)))))
  (iter (grid.points grid) '()))

(define my-input (call-with-input-file "2024/input12.txt" read-lines))

(define (solve-1 lines)
  (let* ((grid (parse-grid lines))
	 (regions (build-regions grid)))
    (apply + (map * (map region.perimeter regions) (map region.area regions)))))

(define (solve-2 lines)
  (let* ((grid (parse-grid lines))
	 (regions (build-regions grid)))
    (apply + (map * (map region.sides regions) (map region.area regions)))))

(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
