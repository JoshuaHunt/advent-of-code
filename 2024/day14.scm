(use-modules (utils)
	     (srfi srfi-26))

(define (robot.new pos vel)
  (list pos vel))

(define robot.pos car)
(define robot.pos-x (compose car robot.pos))
(define robot.pos-y (compose cadr robot.pos))
(define robot.vel cadr)
(define (robot.with-pos r pos)
  (robot.new pos (robot.vel r)))

(define (robot.step r n grid-size)
  (let ((new-pos (map modulo
		      (vector.add (robot.pos r)
				  (vector.scale (robot.vel r) n))
		      grid-size)))
    (robot.with-pos r new-pos)))  

(define (strip-var-name s)
  (cadr (string-split s #\=)))

(define (parse-pos coords)
  (let ((x (string->number (car (string-split coords #\,))))
	(y (string->number (cadr (string-split coords #\,)))))
    (point.new x y)))

(define parse-vec parse-pos)

(define (parse-robot line)
  (let* ((pos (strip-var-name (car (string-split line #\space))))
	 (vel (strip-var-name (cadr (string-split line #\space)))))
    (robot.new (parse-pos pos) (parse-vec vel))))

(define (parse-robots lines)
  (map parse-robot lines))

(define (count-quadrants grid-size robots)
  (let* ((midpoint (vector.scale (point.add grid-size '(-1 -1))
				 1/2))
	 (mid-x (car midpoint))
	 (mid-y (cadr midpoint)))
    (map (lambda (pred?)
	   (length (filter pred? robots)))
	 (list
	  (lambda (r) (and (< (robot.pos-x r) mid-x)
			   (< (robot.pos-y r) mid-y)))
	  
	  (lambda (r) (and (> (robot.pos-x r) mid-x)
			   (< (robot.pos-y r) mid-y)))
	  
	  (lambda (r) (and (< (robot.pos-x r) mid-x)
			   (> (robot.pos-y r) mid-y)))
	  
	  (lambda (r) (and (> (robot.pos-x r) mid-x)
			   (> (robot.pos-y r) mid-y)))))))

(define my-input (call-with-input-file "2024/input14.txt" read-lines))

(define (solve-1 lines)
  (let ((robots (parse-robots lines))
	(grid-size (list 101 103)))
    (->> robots
	 (map (cut robot.step <> 100 grid-size))
	 (count-quadrants grid-size)
	 (apply *))))


(define (grid-with-size size fill)
  (define (list-with-size len)
    (if (<= len 0)
	'()
	(cons fill (list-with-size (- len 1)))))

  (grid.new (map (lambda (ignored) (list-with-size (car size)))
		 (list-with-size (cadr size)))))
  

(define (format-grid robots size)
  (grid->string (fold-left (lambda (g r) (grid.set! g (robot.pos r) #\@))
			   (grid-with-size size #\.)
			   robots)))

(define (has-neighbours? r robot-poses)
  (->> orthodiagonal-dirs
       (map dir->vector)
       (map (cut point.add (robot.pos r) <>))
       (filter (cut hash-set.contains? robot-poses <>))
       (length)
       (<= 2)))

(define (has-many-neighbours? robots)
  (let* ((poses (list->hash-set (map robot.pos robots)))
	 (ratio (/ (length (filter (cut has-neighbours? <> poses) robots))
		   (length robots))))
    (< 0.7 ratio)))

(define (solve-2 lines)
  (let ((grid-size (list 101 103)))
    (define (iter steps robots)
      (let ((new-robots (map (cut robot.step <> 1 grid-size) robots)))
	(if (has-many-neighbours? new-robots)
	    (begin
	      (display (format-grid new-robots grid-size))
	      (newline)
	      (display steps)
	      (newline)
	      (when (equal? (read-char) #\newline)
		(iter (+ 1 steps) new-robots)))
	    (iter (+ 1 steps) new-robots))))

    (iter 1 (parse-robots lines))))
	

(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
