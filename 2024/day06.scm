(use-modules (utils))
(use-modules (srfi srfi-26))
(use-modules (ice-9 futures))

(define (dir->vector dir)
  (cond
   ((equal? dir 'north) (list 0 -1))
   ((equal? dir 'south) (list 0 1))
   ((equal? dir 'east) (list 1 0))
   ((equal? dir 'west) (list -1 0))
   (else (error "Unknown dir " dir))))

(define (dir.clockwise dir)
  (cond ((equal? dir 'north) 'east)
	((equal? dir 'east) 'south)
	((equal? dir 'south) 'west)
	((equal? dir 'west) 'north)
	(else (error "Unknown dir " dir))))

(define (sentry.new pos dir)
  (list pos dir))

(define (sentry.pos sentry)
  (car sentry))

(define (sentry.dir sentry)
  (cadr sentry))

(define (sentry.next-pos sentry)
  (point.add (sentry.pos sentry) (dir->vector (sentry.dir sentry))))

(define (sentry.advance sentry)
  (sentry.new (sentry.next-pos sentry) (sentry.dir sentry)))

(define (sentry.turn-right sentry)
  (sentry.new (sentry.pos sentry) (dir.clockwise (sentry.dir sentry))))

(define (replace-char s old new)
  (map (lambda (c) (if (equal? c old) new c)) s))

(define (parse-grid lines)
  (grid.new (map (compose (cut replace-char <> #\^ #\.) string->list) lines)))

(define (grid.with-obstacle obstacle-pos grid)
  (let ((grid-copy (grid.copy grid)))
    (grid.set! grid-copy obstacle-pos #\#)
    grid-copy))

(define (find-sentry lines)
  (let* ((grid (grid.new (map string->list lines)))
	 (pos (only-element (grid.filter grid (cut equal? <> #\^)))))
    (sentry.new pos 'north)))

(define (mark-patrol-route! sentry grid old-sentries)
  ;; Returns true if the route is a loop.
  (if (grid.is-inside? grid (sentry.pos sentry))
      (begin
	(grid.set! grid (sentry.pos sentry) #\o)
	(cond ((hash-set.contains? old-sentries sentry)
	       #t)
	      ((equal? (grid.get grid (sentry.next-pos sentry)) #\#)
	       (mark-patrol-route! (sentry.turn-right sentry) grid (hash-set.add! old-sentries sentry)))
	      (else
	       (mark-patrol-route! (sentry.advance sentry) grid (hash-set.add! old-sentries sentry)))))
      #f))

(define (patrol-route sentry grid)
  (let ((grid-copy (grid.copy grid)))
    (mark-patrol-route! sentry grid-copy (hash-set.new))
    (grid.filter grid-copy (cut equal? <> #\o))))

(define my-input (call-with-input-file "2024/input06.txt" read-lines))

(define (solve-1 lines)
  (let ((grid (parse-grid lines))
	(sentry (find-sentry lines)))
    (length (patrol-route sentry grid))))

(define (causes-loop? obstacle-pos grid sentry)
  (mark-patrol-route!
   sentry
   (grid.with-obstacle obstacle-pos (grid.copy grid))
   (hash-set.new)))

(define (parallel-filter pred? xs)
  (define (future-pred? x)
    (future (pred? x)))
  (let ((futures (map future-pred? xs)))
    (filter touch futures)))

(define (solve-2 lines)
  (let* ((grid (parse-grid lines))
	 (sentry (find-sentry lines))
	 (base-route (filter (compose not (cut equal? <> (sentry.pos sentry)))
			     (patrol-route sentry grid))))
    (length (parallel-filter (cut causes-loop? <> grid sentry) base-route))))


(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
