(use-modules (utils)
	     (srfi srfi-9 gnu)
	     (srfi srfi-26))

(define (parse-register line)
  (string->number (list-ref (string-split line #\space) 2)))

(define (parse-program line)
  (list->vector
   (map string->number
	(string-split (cadr (string-split line #\space))
		      #\,))))

(define-immutable-record-type <prog>
  (prog._new a b c xs in-p outs)
  prog?
  (a prog.a prog.set-a)
  (b prog.b prog.set-b)
  (c prog.c prog.set-c)
  (xs prog.instructions)
  (in-p prog.instruction-pointer prog.set-instruction-pointer)
  (outs prog._output prog._set-output))

(define (prog.new a b c xs)
  (prog._new a b c xs 0 '()))
(define (prog.instruction p)
  (if (<= (vector-length (prog.instructions p))
	  (prog.instruction-pointer p))
      #f
      (vector-ref (prog.instructions p) (prog.instruction-pointer p))))
(define (prog.literal-op p)
  (vector-ref (prog.instructions p) (1+ (prog.instruction-pointer p))))
(define (prog.combo-op p)
  (case (prog.literal-op p)
   ((0 1 2 3) => identity)
   ((4) (prog.a p))
   ((5) (prog.b p))
   ((6) (prog.c p))
   (else (error "Unknown combo operand"))))
(define (prog.at-end? p)
  (equal? (vector-length (prog.instructions p))
	  (prog.instruction-pointer p)))
(define (prog.jmp-end p)
  (prog.set-instruction-pointer p (vector-length (prog.instructions p))))
(define (prog.step p)
  (prog.set-instruction-pointer p (+ 2 (prog.instruction-pointer p))))
(define (prog.unstep p)
  (if (<= 2 (prog.instruction-pointer p))
      (prog.set-instruction-pointer p (- (prog.instruction-pointer p) 2))
      (prog.unstep (prog.jmp-end p))))
(define (prog.append-output p o)
  (prog._set-output p (cons o (prog._output p))))
(define (prog.output p)
  (reverse (prog._output p)))
(define (prog.set-output p o)
  (prog._set-output p (reverse o)))

(define (adv prog)
  (prog.step (prog.set-a prog (floor (/ (prog.a prog)
					(expt 2 (prog.combo-op prog)))))))

(define (bxl prog)
  (prog.step (prog.set-b prog (logxor (prog.b prog)
				      (prog.literal-op prog)))))

(define (bst prog)
  (prog.step (prog.set-b prog (modulo (prog.combo-op prog)
				      8))))

(define (jnz prog)
  (if (equal? (prog.a prog) 0)
      (prog.step prog)
      (prog.set-instruction-pointer prog (prog.literal-op prog))))

(define (bxc prog)
  (prog.step (prog.set-b prog (logxor (prog.b prog)
				      (prog.c prog)))))

(define (out prog)
  (prog.step (prog.append-output prog (modulo (prog.combo-op prog)
					      8))))

(define (bdv prog)
  (prog.step (prog.set-b prog (floor (/ (prog.a prog)
					(expt 2 (prog.combo-op prog)))))))

(define (cdv prog)
  (prog.step (prog.set-c prog (floor (/ (prog.a prog)
					(expt 2 (prog.combo-op prog)))))))

(define (run prog)
  (case (prog.instruction prog)
    ((#f) (prog.output prog))
    ((0) (run (adv prog)))
    ((1) (run (bxl prog)))
    ((2) (run (bst prog)))
    ((3) (run (jnz prog)))
    ((4) (run (bxc prog)))
    ((5) (run (out prog)))
    ((6) (run (bdv prog)))
    ((7) (run (cdv prog)))))


(define (parse-input lines)
  (prog.new (parse-register (list-ref lines 0))
	    (parse-register (list-ref lines 1))
	    (parse-register (list-ref lines 2))
	    (parse-program (list-ref lines 4))))

(define (format-output xs)
  (string-join (map number->string xs) ","))

(define (solve-1 lines)
  (format-output (run (parse-input lines))))

;; 2,4,1,2,7,5,1,7,4,4,0,3,5,5,3,0
;; 2,4 <- B = (A % 8)
;; 1,2 <- B = B ^ 2
;; 7,5 <- C = floor(A / 2^B) = A >> B
;; 1,7 <- B = B ^ 7
;; 4,4 <- B = B ^ C
;; 0,3 <- A = A >> 3
;; 5,5 <- output (B % 8)
;; 3,0 <- loop if A > 0

(define (candidates prog output)
  ;; The program I have has a couple of key properties:
  ;;
  ;; - before looping, it always drops the last 3 bits of A
  ;;
  ;; - the loop has a single output per iteration
  ;;
  ;; - the output of the iteration only depends on the value of A at
  ;;   the start of the iteration.
  ;;
  ;; This allows us to search for A in 3-bit blocks.
  
  (define (undo-truncates a)
    (map (cut + (* 8 a) <>) (list 0 1 2 3 4 5 6 7)))

  (if (null? output)
      (list 0)
      (let* ((prev-candidates (candidates prog (cdr output)))
	     (all-candidates (flat-map undo-truncates prev-candidates)))
	(filter (lambda (a) (equal? output (run (prog.set-a prog a))))
		all-candidates))))
    

(define (solve-2 lines)
  (let* ((prog (parse-input lines))
	 (output (vector->list (prog.instructions prog))))
    (apply min (candidates prog output))))

(define my-input (call-with-input-file "2024/input17.txt" read-lines))

(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
