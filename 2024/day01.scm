(use-modules (utils))

(define (parse-line line)
  (map string->number
       (filter (compose not empty-string?)
	       (string-split line #\space))))

(define (parse-matrix lines)
  (map parse-line lines))

(define (transpose matrix)
  (apply map list matrix))

(define (diff x y)
  (abs (- x y)))

(define (solve-part-1 lines)
  (let ((matrix (parse-matrix lines)))
    (apply +
	   (apply map diff
		  (map (lambda (xs) (sort xs <))
		       (transpose matrix))))))

(define (compute-rhs-freqs lhs rhs)
  (define (count x)
    (length (filter (lambda (y) (equal? x y)) rhs)))
  
  (list
   lhs
   (map count lhs)))
    

(define (solve-part-2 lines)
  (let ((matrix (parse-matrix lines)))
    (apply +
	   (apply map *
		  (apply compute-rhs-freqs
		   (transpose matrix))))))

(define my-input (call-with-input-file "input01.txt" read-lines))

(format #t "~a~%~a~%"
	(solve-part-1 my-input)
	(solve-part-2 my-input))
