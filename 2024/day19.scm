(use-modules (utils)
	     (srfi srfi-9)
	     (srfi srfi-9 gnu)
	     (srfi srfi-26))

(define-record-type <node>
  (node.new char children terminal? parent)
  node?
  (char node.char)
  (children node.children node.set-children!)
  (terminal? node.terminal? node.set-terminal!)
  (parent node.parent))

(define (node.root)
  (node.new #f '() #f #f))

(define (node.get-or-add-child n c)
  (let ((old-child (assoc-ref (node.children n) c)))
    (or old-child
	(let ((new-child (node.new c '() #f n)))	  
	  (node.set-children! n
			      (assoc-set! (node.children n)
					  c
					  new-child))
	  new-child))))

(define (add-pattern-to-tree node pattern)
  (if (null? pattern)
      (node.set-terminal! node #t)
      (add-pattern-to-tree (node.get-or-add-child node (car pattern))
			   (cdr pattern)))
  node)

(define (node->string n)
  (define (foo n)
    (if (node.parent n)
        (cons (node.char n) (foo (node.parent n)))
	'()))
  (string-reverse (list->string (foo n))))

(define (split-commas s)
  (map string-trim-both (string-split s #\,)))

(define (parse-patterns lines)
  (let ((patterns (map string->list (split-commas (car lines)))))
    (fold-left add-pattern-to-tree (node.root) patterns)))

(define-immutable-record-type <walker>
  (walker.new node count)
  walker?
  (node walker.node walker.set-node)
  (count walker.count walker.set-count))

(define (walker->string w)
  (format #f "~ax ~a" (walker.count w) (node->string (walker.node w))))

(define (walker.at-end-of-pattern? w)
  (node.terminal? (walker.node w)))

(define (walker.go-to-child w c)
  (let ((child (assoc-ref (node.children (walker.node w)) c)))
    (if child
	(walker.set-node w child)
	#f)))

(define-immutable-record-type <traversal>
  (traversal._new walkers root)
  traversal?
  (walkers traversal.walkers traversal.set-walkers)
  (root traversal.root))

(define (traversal.new patterns-tree)
  (traversal._new (list (walker.new patterns-tree 1)) patterns-tree))

(define (traversal.add-walker t count)
  (if (equal? count 0)
      t
      (traversal.set-walkers
       t
       (cons (walker.new (traversal.root t) count)
	     (traversal.walkers t)))))

(define (at-end-of-pattern? t) ; traversal
  (apply or-fn (map walker.at-end-of-pattern? (traversal.walkers t)))) ; optimisable: there's no early return

(define (count-at-end-of-pattern t)
  (apply + (map walker.count (filter walker.at-end-of-pattern? (traversal.walkers t)))))

(define (match-char t c)
  ;; Advances all walkers by the indicated char, dropping those
  ;; that don't match.
  (traversal.set-walkers
   t
   (->> (traversal.walkers t)
	(map (lambda (n) (walker.go-to-child n c)))
	(filter identity))))

(define (accept-char t c) ; traversal, char
  ;(format #t "~a~%" (map walker->string (traversal.walkers t)))
  (match-char (traversal.add-walker t (count-at-end-of-pattern t))
	      c))

(define (is-possible? g ps) ; goal, patterns
 ; (format #t " == ~a ==~%" g)
  (at-end-of-pattern? (fold-left accept-char
				 (traversal.new ps)
				 (string->list g))))

(define (count-possibilities g ps)
					;  (format #t " == ~a ==~%" g)
  (apply +
	 (map walker.count
	      (filter walker.at-end-of-pattern?
		      (traversal.walkers (fold-left accept-char
						    (traversal.new ps)
						    (string->list g)))))))
  
(define my-input (call-with-input-file "2024/input19.txt" read-lines))


(define (parse-goals lines)
  (cddr lines))

(define (solve-1 lines)
  (let ((patterns (parse-patterns lines))
	(goals (parse-goals lines)))
    (length (filter (cut is-possible? <> patterns) goals))))

(define (solve-2 lines)
  (let ((patterns (parse-patterns lines))
	(goals (parse-goals lines)))
    (apply + (map (cut count-possibilities <> patterns) goals))))

(display (solve-1 my-input))
(newline)

;; 750786028787682 is too high
(display (solve-2 my-input))
(newline)
