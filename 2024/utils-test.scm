(use-modules (srfi srfi-64)
	     (srfi srfi-1)
	     (utils))

(test-begin "parallel-filter")
(test-equal "evens to 1003"
 (filter even? (iota 1003))
 (parallel-filter even? (iota 1003)))
(test-end)

(test-begin "parallel-every")
(test-equal "true"
  #t
  (parallel-every even? '(2 4 6 8 10 12 14 16 18)))
(test-equal "false"
  #f
  (parallel-every even? (iota 1003)))
(test-end "parallel-every")

(test-begin "nested-parallel-filter-every")
(let ((xs (map iota (iota 100))))
  (test-equal "foo"
    (filter (lambda (ys) (every even? ys)) xs)
    (parallel-filter (lambda (ys) (parallel-every even? ys)) xs)))
(test-end "nested-parallel-filter-every")
  
