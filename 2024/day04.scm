(use-modules (utils))

(define (ray.new x y dx dy)
  (list x y dx dy))
(define ray.x car)
(define ray.y cadr)
(define ray.dx caddr)
(define ray.dy cadddr)

(define (vec.new dx dy)
  (list dx dy))
(define vec.x car)
(define vec.y cadr)
 

(define (ray.get r len grid)
  (define (iter x y count)
    (if (<= count 0)
	'()
	(cons (grid.get grid (point.new x y))
	      (iter (+ x (ray.dx r))
		    (+ y (ray.dy r))
		    (- count 1)))))
  (iter (ray.x r) (ray.y r) len))

(define (ray.match? r grid text)
  (let ((expected (string->list text))
        (actual (ray.get r (string-length text) grid)))
    (equal? actual expected)))

(define (cross.new x y dl dr)
  (list x y dl dr))
(define cross.x car)
(define cross.y cadr)
(define cross.dl caddr)
(define cross.dr cadddr)

(define (cross.match? c grid)
  (define (make-ray x y dpos)
    (ray.new (- x (vec.x dpos))
	     (- y (vec.y dpos))
	     (vec.x dpos)
	     (vec.y dpos)))
  
  (let ((ray-l (make-ray (cross.x c) (cross.y c) (cross.dl c)))
	(ray-r (make-ray (cross.x c) (cross.y c) (cross.dr c))))
    (and (ray.match? ray-l grid "MAS")
	 (ray.match? ray-r grid "MAS"))))
    

(define (generate-rays grid)
  (define (iter x y)
    (cond ((> y (grid.height grid))
	   '())
	  ((> x (grid.width grid))
	   (iter 0 (+ 1 y)))
	  (else
	   (append (list
		    (ray.new x y 1 0)
		    (ray.new x y 1 -1)
		    (ray.new x y 0 -1)
		    (ray.new x y -1 -1)
		    (ray.new x y -1 0)
		    (ray.new x y -1 1)
		    (ray.new x y 0 1)
		    (ray.new x y 1 1))
		   (iter (+ 1 x) y)))))
  (iter 0 0))

(define (generate-crosses grid)
  (define (iter x y)
    (cond ((> y (grid.height grid))
	   '())
	  ((> x (grid.width grid))
	   (iter 0 (+ 1 y)))
	  (else
	   (append (list
		    (cross.new x y (vec.new 1 -1) (vec.new -1 -1))
		    (cross.new x y (vec.new 1 -1) (vec.new 1 1))
		    (cross.new x y (vec.new -1 1) (vec.new -1 -1))
		    (cross.new x y (vec.new -1 1) (vec.new 1 1)))
		   (iter (+ 1 x) y)))))
  (iter 0 0))

(define (parse-grid lines)
  (map string->list lines))

(define (solve-part-1 lines)
  (let* ((grid (parse-grid lines))
	 (rays (generate-rays grid)))
    (length (filter (lambda (r) (ray.match? r grid "XMAS"))
		    rays))))

(define (solve-part-2 lines)
  (let* ((grid (parse-grid lines))
	 (crosses (generate-crosses grid)))
    (length (filter (lambda (c) (cross.match? c grid)) crosses))))
	 

(define my-input (call-with-input-file "2024/input04.txt" read-lines))

(display (solve-part-1 my-input))
(newline)
(display (solve-part-2 my-input))
(newline)
