(use-modules (utils)
	     (srfi srfi-26))

(define my-input (call-with-input-file "2024/input11.txt" read-lines))

(define (parse-stones line)
  (map string->number (string-split (car line) #\space)))

(define (replace stone)
  (cond ((equal? 0 stone)
	 (list 1))
	((even? (string-length (number->string stone)))
	 (let* ((name (number->string stone))
		(len (string-length name)))
	   (map string->number
		(list (substring name 0 (/ len 2))
		      (substring name (/ len 2))))))
	(else (list (* 2024 stone)))))

(define (step stones)
  (flat-map replace stones))

(define (repeat n f xs)
  (if (equal? n 0)
      xs
      (repeat (- n 1) f (f xs))))

(define (solve-1 lines)
  (length (repeat 25 step (parse-stones lines))))

(define (hash-increment! table key value)
  (hash-set! table key (+ value (hash-ref table key 0))))

(define (step-buckets buckets)
  (hash-fold (lambda (stone count new-buckets)
	       (for-each (cut hash-increment! new-buckets <> count)
			 (replace stone))
	       new-buckets)
	     (make-hash-table)
	     buckets))

(define (stones->buckets stones)
  (fold-left (lambda (table stone)
	       (hash-increment! table stone 1)
	       table)
	     (make-hash-table)
	     stones))

(define (sum-buckets table)
  (hash-fold (lambda (key val acc) (+ val acc))
	     0
	     table))

(define (solve-2 lines)
  (sum-buckets (repeat 75 step-buckets (stones->buckets (parse-stones lines)))))

(display (solve-1 my-input))
(newline)
(display (solve-2 my-input))
(newline)
  
