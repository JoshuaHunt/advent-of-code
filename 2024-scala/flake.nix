{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
  };

  outputs = { self, nixpkgs, ...}: let
    system = "x86_64-linux";
  in {
    devShells."${system}".default =
      with import nixpkgs { inherit system; };
      mkShell {
        packages = [
          scala
          scalafmt
          scalafix
          jdk17
          (vscode-with-extensions.override {
            vscode = vscodium;
            vscodeExtensions = vscode-utils.extensionsFromVscodeMarketplace [
               {
                 name = "scala";
                 publisher = "scala-lang";
                 version = "0.5.9";
                 sha256 = "sha256-zgCqKwnP7Fm655FPUkD5GL+/goaplST8507X890Tnhc=";
               }
               {
                name = "Nix";
                publisher = "bbenoist";
                version = "1.0.1";
                sha256 = "sha256-qwxqOGublQeVP2qrLF94ndX/Be9oZOn+ZMCFX1yyoH0=";
               }
               {
                name = "metals";
                publisher = "scalameta";
                version = "1.47.0";
                sha256 = "sha256-NHlNaXjNp4Qu2mn4N3lcmIR/4m9DWXqVDHC/ZBPA4nU=";
               }
             ];
           })
        ];
        shellHook = ''
          exec fish
        '';
      };
  };
}
      
