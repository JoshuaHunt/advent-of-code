import scala.util.Using
import scala.io.Source

@main
def main(): Unit = {
  val input = "input02.txt"
  val contents = Using(Source.fromFile(input))(_.mkString)
  contents.foreach { lines =>
    println(solve1(lines))
    println(solve2(lines))
  }
}

def solve1(input: String): Int = {
  val reports = parseReports(input)
  val safeReports = reports.filter(_.isSafe)
  safeReports.size
}

def solve2(input: String): Int = {
  val reports = parseReports(input)
  val safeReports = reports.filter(_.isDampedSafe)
  safeReports.size
}

def parseReports(input: String): List[Report] = {
  val lines = input.trim.split("\n")
  lines.map(parseReport).toList
}

def parseReport(input: String): Report = {
  val levels = input.trim.split("\\s+").map(_.toInt).toList
  Report(levels)
}

case class Report(levels: List[Int]) {
  def isSafe: Boolean = {
    val pairs = levels.zip(levels.tail)
    val isMonoto = pairs.forall(_ < _) || pairs.forall(_ > _)
    val smallSteps = pairs.forall((x, y) => {
      val diff = (x - y).abs
      1 <= diff && diff <= 3
    })
    isMonoto && smallSteps
  }

  def isDampedSafe: Boolean = {
    val dampedLevels =
      levels.indices.map(i => levels.patch(i, Nil, 1)).toList
    val dampedReports = dampedLevels.map(Report.apply).toList
    isSafe || dampedReports.exists(_.isSafe)
  }
}
