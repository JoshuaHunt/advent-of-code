import scala.util.Using
import scala.io.Source
import scala.util.{Try, Success, Failure}
import scala.collection.MapView

@main
def main(): Unit = {
  val input = "input01.txt"
  val contents = Using(Source.fromFile(input))(_.mkString)
  contents.foreach { lines =>
    println(solve1(lines))
    println(solve2(lines))
  }
}

def solve1(input: String): Int = {
  val (column1, column2) = parseColumns(input)
  column1.zip(column2).map((x, y) => (x - y).abs).sum
}

def solve2(input: String): Int = {
  val (column1, column2) = parseColumns(input)
  val frequencies: Map[Int, Int] =
    column2.groupBy(identity).view.mapValues(_.size).toMap
  column1.map(x => x * frequencies.getOrElse(x, 0)).sum
}

def parseColumns(input: String): (List[Int], List[Int]) = {
  val lines = input.trim.split("\n")
  val pairs = lines.map(_.trim.split("\\s+").map(_.toInt))
  val column1 = pairs.map(_(0)).toList.sorted
  val column2 = pairs.map(_(1)).toList.sorted
  (column1, column2)
}
